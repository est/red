<?php

if(!class_exists('red_item_mysql_db')) {
  class red_item_mysql_db extends red_item {
    static public $track_disk_usage = TRUE;
    var $_mysql_db_name;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_illegal_database_name_patterns = [ '^information_schema$', '^performance_schema$', '^mysql$', '^lost\+found$', '^aria_log', '^ibdata', '^ib_logfile', '^ibtmp', '^phpmyadmin$', '^tc\.log$' ];
    var $_item_quota = '1gb';

    function __construct($construction_options) {
      $this->_track_disk_usage = self::$track_disk_usage;
      parent::__construct($construction_options);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge( $this->_datafields,
        array (
          'mysql_db_name' => array (
             'req' => true,
             'pcre'   => RED_SQL_DB_MATCHER,
             'pcre_explanation'   => RED_SQL_DB_EXPLANATION,
             'type'  => 'varchar',
             'fname'  => red_t('Database name'),
             'user_insert' => TRUE,
             'user_update' => FALSE,
             'user_visible' => TRUE,
             'input_type' => 'text',
             'text_length' => 20,
             'text_max_length' => 64,
             'tblname'   => 'red_item_mysql_db',
             'filter' => true
          ),
        )
      );
      $this->_datafields['item_host']['user_visible'] = TRUE;
    }

    function set_mysql_db_name($value) {
      $this->_mysql_db_name = $value;
    }
    function get_mysql_db_name() {
      return $this->_mysql_db_name;
    }
    function additional_validation() {
      if ($this->_delete) {
        // Ensure no mysql users are using this database.
        // The database matcher matches things like:
        // mydb
        // mydb:myotherdb:myyetanotherdb
        $sql = "SELECT mysql_user_name FROM red_item JOIN red_item_mysql_user USING (item_id)
         WHERE item_status != 'deleted' and item_status != 'pending-delete' AND mysql_user_db REGEXP @user_regexp";
        $regexp = '(^|:)' . $this->get_mysql_db_name() . '(:|$)';
        $result = red_sql_query($sql, ['@user_regexp' => $regexp]);
        $users = [];
        while ($row = red_sql_fetch_row($result)) {
          $users[] = $row[0];
        }
        if (count($users) > 0) {
          $users_display = implode(',', $users);
          $this->set_error(red_t("This database is assigned permission to one or more MySQL users (@users_display). Please edit the user and remove permission before continuing.", [ '@users_display' => $users_display ]), 'validation');
        }
        // No more validation when deleting.
        return;
      }
      if ($this->get_item_quota() == 0) {
        $this->set_error(red_t('The database quota cannot be set to 0.'),'validation');
      }
      foreach($this->_illegal_database_name_patterns as $pattern) {
        if (preg_match("/$pattern/", $this->get_mysql_db_name())) {
          $this->set_error(red_t('The database name you chose is not allowed.'),'validation');
        }
      }

      if(!$this->_delete && !$this->is_field_value_unique('mysql_db_name','red_item_mysql_db',$this->get_mysql_db_name())) {
        $this->set_error(red_t('The database name you chose is already taken.'),'validation');
      }
    }
  }
}

?>
