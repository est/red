<?php

// emailbox - this class is for mainipulating email boxes 
if(!class_exists('red_item_mailbox')) {
  class red_item_mailbox extends red_item {
    static public $track_disk_usage = TRUE;
    var $_mailbox;
    var $_mailbox_login;
    var $_mailbox_auto_response;
    var $_mailbox_auto_response_reply_from;
    var $_mailbox_auto_response_action;
    var $_mailbox_auto_response_action_options;
    var $_mailbox_abandoned;
    var $_mailbox_mountpoint;
    var $_human_readable_description;
    var $_human_readable_name;
    var $_item_quota = '1gb';

    function __construct($co) {
      $this->_track_disk_usage = self::$track_disk_usage;

      $this->_mailbox_auto_response_action_options = array(
        '' => red_t('Do not send auto response'),
        'respond_and_deliver' => red_t('Respond and deliver messages'),
        'respond_only' => red_t('Respond but do not deliver')
      );
      parent::__construct($co);
      $this->_datafields['item_quota']['user_visible'] = TRUE;
      $this->_datafields['item_disk_usage']['user_visible'] = TRUE;

      $this->_set_child_datafields();
      // If it's a new item, we have to autoset the mountopint if one is
      // specified in red_server for this item's host.
      if (!$this->exists_in_db()) {
        $this->set_mailbox_mountpoint($this->get_mountpoint_for_host());
      }
      
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge($this->_datafields,
        array (
          'mailbox_login' => array (
            'req' => true,
            'pcre'   => RED_LOGIN_MATCHER,
            'pcre_explanation'   => RED_LOGIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('User account login'),
            'user_insert' => TRUE,
            'user_update' => FALSE,
            'user_visible' => TRUE,
            'input_type' => 'select',
            'options' => $this->get_user_account_login_options(),
            'text_length' => 20,
            'text_max_length' => 128,
            'tblname'   => 'red_item_mailbox',
            'filter' => true
          ),
          'mailbox_auto_response_action' => array (
            'req' => false,  
            'pcre'   => RED_AUTO_RESPONSE_ACTION_MATCHER,
            'pcre_explanation'   => RED_AUTO_RESPONSE_ACTION_EXPLANATION,
            'type'  => 'varchar',
            'input_type' => 'select',
            'options' => $this->_mailbox_auto_response_action_options,
            'fname'  => red_t('Auto Responder Action'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'tblname'   => 'red_item_mailbox'
          ),
          'mailbox_auto_response' => array (
            'req' => false,  
            'pcre'   => RED_ANYTHING_MATCHER,
            'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
            'type'  => 'text',
            'fname'  => red_t('Auto Responder Message'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'textarea',
            'textarea_cols' => 50,
            'textarea_rows' => 5,
            'tblname'   => 'red_item_mailbox'
          ),
          'mailbox_auto_response_reply_from' => array (
            'req' => false,  
            'pcre'   => RED_EMAIL_MATCHER,
            'pcre_explanation'   => RED_EMAIL_EXPLANATION,
            'type'  => 'text',
            'fname'  => red_t('Auto Responder Reply From Address'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'tblname'   => 'red_item_mailbox'
          ),
          'mailbox_mountpoint' => array (
            'req' => false,  
            'pcre'   => RED_ANYTHING_MATCHER,
            'pcre_explanation'   => RED_ANYTHING_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Mount Point'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'text_length' => 8,
            'text_max_length' => 8,
            'tblname'   => 'red_item_mailbox',
            'filter' => false
          ),
          'mailbox_abandoned' => array (
            'req' => false,  
            'pcre'   => RED_TINYINT_MATCHER,
            'pcre_explanation'   => RED_TINYINT_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Abandoned mailbox'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'text_length' => 1,
            'text_max_length' => 1,
            'tblname'   => 'red_item_mailbox',
            'filter' => false
         ),
        )
      );
    }

    function is_abandoned() {
      if ($this->get_mailbox_abandoned() == 1) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }

    // We have to Override. If we are not on a mailstore server, we don't set
    // the quota and we don't track usage.
    function set_item_host($value) {
      parent::set_item_host($value);
      if (!preg_match('/^mailstore/', $this->get_item_host())) {
        $this->set_item_quota(0);
        $this->_track_disk_usage = FALSE;
      }
    }
    function set_mailbox_login($value) {
      $this->_mailbox_login = strtolower($value);
    }
    function get_mailbox_login() {
      return $this->_mailbox_login;
    }

    function set_mailbox_mountpoint($value) {
      $this->_mailbox_mountpoint = $value;
    }
    function get_mailbox_mountpoint() {
      return $this->_mailbox_mountpoint;
    }
    function set_mailbox_auto_response($value) {
      $this->_mailbox_auto_response = $value;
    }
    function get_mailbox_auto_response() {
      return $this->_mailbox_auto_response;
    }
    function set_mailbox_auto_response_action($value) {
      $this->_mailbox_auto_response_action = $value;
    }
    function get_mailbox_auto_response_action() {
      return $this->_mailbox_auto_response_action;
    }
    function set_mailbox_auto_response_reply_from($value) {
      $this->_mailbox_auto_response_reply_from = $value;
    }
    function get_mailbox_auto_response_reply_from() {
      return $this->_mailbox_auto_response_reply_from;
    }
    function set_mailbox_abandoned($value) {
      $this->_mailbox_abandoned = $value;
    }
    function get_mailbox_abandoned() {
      return $this->_mailbox_abandoned;
    }

    function additional_validation() {
      if ($this->_delete) {
        // Ensure this mailbox is not being used in an email address. We allow the deletion of
        // the mailbox is owned by another member to ease the transition into this new validation
        // rule.
        $login = $this->get_mailbox_login();
        $member_id = $this->get_member_id();
        $sql = "SELECT COUNT(*) AS count FROM red_item JOIN red_item_email_address USING (item_id)
          JOIN red_hosting_order USING(hosting_order_id)
          WHERE item_status != 'deleted' AND item_status != 'pending-delete' AND email_address_recipient
          REGEXP @login_regexp AND red_hosting_order.member_id = #member_id";
        $result = red_sql_query($sql, [
            '@login_regexp' => "(^|,){$login}@mail.mayfirst.org", 
            '#member_id' => $member_id
          ]
        );
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t("This mailbox is in use in one of your email addresses. Please remove it from the email address before deleting."),'validation');
        }
        return true;
      } 
      if ($this->_disable) {
        $login = $this->get_mailbox_login();
        $member_id = $this->get_member_id();
        $sql = "SELECT COUNT(*) AS count FROM red_item JOIN red_item_email_address USING (item_id)
          JOIN red_hosting_order USING(hosting_order_id)
          WHERE item_status = 'active' AND email_address_recipient
          REGEXP @login_regexp AND red_hosting_order.member_id = #member_id";
        $result = red_sql_query($sql, [
            '@login_regexp' => "(^|,){$login}@mail.mayfirst.org", 
            '#member_id' => $member_id
          ]
        );
        $row = red_sql_fetch_row($result);
        if ($row[0] > 0) {
          $this->set_error(red_t("This mailbox is in use in one of your email addresses. Please remove it from the email address before disabling."),'validation');
        }
        return true;
      }

      $related_user_accounts = $this->get_available_user_account_logins();
      if(!in_array($this->get_mailbox_login(),$related_user_accounts)) {
        $this->set_error(red_t("You can only specify a user account that exists."),'validation');
      }

      // Make sure login is unique
      if(!$this->is_field_value_unique('mailbox_login','red_item_mailbox',$this->get_mailbox_login())) {
        $this->set_error(red_t('The login has already been assigned a mailbox.'),'validation');
      }
      // If auto response is set, make sure auto_response_from_email
      // is also set
      $action = $this->get_mailbox_auto_response_action();
      $from = $this->get_mailbox_auto_response_reply_from();
      $message = $this->get_mailbox_auto_response();
      if(!empty($action)) {
        if(empty($from)) {
          $this->set_error(red_t('You must specify the email address you want your auto response to be sent from.'),'validation');
        }
        if(empty($message)) {
          $this->set_error(red_t('You must specify the auto response message that you want sent.'),'validation');
        }
      }
    }

    function get_user_account_login_options() {
      static $all_logins = [];
      if ($all_logins) return $all_logins;
      $all_logins = $this->get_available_user_account_logins();
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT mailbox_login 
        FROM red_item_mailbox JOIN red_item USING (item_id)
        WHERE
          hosting_order_id = #hosting_order_id AND
          item_status != 'deleted'";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      while ($row = red_sql_fetch_row($result)) {
        $login = $row[0];
        unset($all_logins[$login]);
      }
      return $all_logins;
    }
  }
}



?>
