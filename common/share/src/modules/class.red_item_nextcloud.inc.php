<?php

if(!class_exists('red_item_nextcloud')) {
  class red_item_nextcloud extends red_item {
    static public $track_disk_usage = TRUE;
    var $_item_quota = '1gb';
    var $_nextcloud_login;
    var $_nextcloud_abandoned;
    var $_human_readable_description;
    var $_human_readable_name;

    function __construct($co) {
      $this->_track_disk_usage = self::$track_disk_usage;
      parent::__construct($co);
      $this->_set_child_datafields();
      return true;
    }

    function _set_child_datafields() {
      $this->_datafields = array_merge($this->_datafields, [
          'nextcloud_login' => [
            'req' => true,
            'pcre'   => RED_LOGIN_MATCHER,
            'pcre_explanation'   => RED_LOGIN_EXPLANATION,
            'type'  => 'varchar',
            'fname'  => red_t('Login name'),
            'user_insert' => TRUE,
            'user_update' => FALSE,
            'user_visible' => TRUE,
            'input_type' => 'select',
            'options' => $this->get_user_account_login_options(),
            'tblname'   => 'red_item_nextcloud',
            'filter' => true,
          ],
          'nextcloud_abandoned' => [
            'req' => false,  
            'pcre'   => RED_TINYINT_MATCHER,
            'pcre_explanation'   => RED_TINYINT_EXPLANATION,
            'type'  => 'int',
            'fname'  => red_t('Abandoned'),
            'user_insert' => TRUE,
            'user_update' => TRUE,
            'user_visible' => FALSE,
            'input_type' => 'text',
            'text_length' => 1,
            'text_max_length' => 1,
            'tblname'   => 'red_item_nextcloud',
            'filter' => false
          ],
        ]
      );
    }

    function is_abandoned() {
      if ($this->get_nextcloud_abandoned() == 0) {
        return FALSE;
      }
      elseif ($this->get_nextcloud_abandoned() == 1) {
        return TRUE;
      }
      elseif ($this->get_nextcloud_abandoned() == 2) {
        // This means they have never logged in.
        $last_modified = strtotime($this->get_item_modified());
        $six_months_ago = strtotime("6 months ago");
        if ($last_modified < $six_months_ago) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }
    }

    function set_nextcloud_login($value) {
      $this->_nextcloud_login = $value;
    }
    function get_nextcloud_login() {
      return $this->_nextcloud_login;
    }
    function set_nextcloud_abandoned($value) {
      $this->_nextcloud_abandoned = $value;
    }
    function get_nextcloud_abandoned() {
      return $this->_nextcloud_abandoned;
    }

    function additional_validation() {
      if ($this->get_item_quota() == 0) {
        $this->set_error(red_t('The Nextcloud quota cannot be set to 0.'),'validation');
      }
      $related_user_accounts = $this->get_available_user_account_logins();
      if(!in_array($this->get_nextcloud_login(),$related_user_accounts) && !$this->_delete) {
        $this->set_error(red_t("You can only specify a user account that exists."),'validation');
      }
      if(!$this->is_field_value_unique('nextcloud_login','red_item_nextcloud',$this->get_nextcloud_login())) {
        $this->set_error(red_t("A nextcloud record with the same login exists."),'validation');
      }
    }

    function get_user_account_login_options() {
      static $all_logins = [];
      if ($all_logins) return $all_logins;
      $all_logins = $this->get_available_user_account_logins();
      $hosting_order_id = $this->get_hosting_order_id();
      $sql = "SELECT nextcloud_login 
        FROM red_item_nextcloud JOIN red_item USING (item_id)
        WHERE
          hosting_order_id = #hosting_order_id AND
          item_status != 'deleted'";
      $result = red_sql_query($sql, ['#hosting_order_id' => $hosting_order_id]);
      while ($row = red_sql_fetch_row($result)) {
        $login = $row[0];
        unset($all_logins[$login]);
      }
      return $all_logins;
    }
  }  
}

?>
