<?php
if(!class_exists('red_item_web_conf_node_apache2')) {
  class red_item_web_conf_node_apache2 extends red_item_web_conf {
    var $_sites_enabled_dir = '/etc/apache2/sites-enabled';
    var $_sites_available_dir = '/etc/apache2/sites-available';
    var $_path_to_default_index_template = '/usr/local/share/red/node/etc/red/default_index_template.ihtml';
    var $_apache_cmd = '. /etc/apache2/envvars; /usr/sbin/apache2';
    var $_reload_apache_cmd = '/usr/bin/systemctl reload apache2';
    var $_apache_server_wide_conf_file = '/etc/apache2/apache2.conf';
    var $_passwd_file = '/etc/passwd';

    /*
     * MAIN FUNCTIONS
     */

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // Find systemctl...
      $systemctl_candidates = ['/usr/bin/systemctl', '/bin/systemctl' ];
      foreach ($systemctl_candidates as $candidate) {
        if (file_exists($candidate)) {
          $this->_reload_apache_cmd = "{$candidate} reload apache2";
          break;
        }
      }

      // red_item will reset this to false on error
      if(!$this) return;
    }

    function node_sanity_check() {
      if(!is_writable($this->_sites_enabled_dir)) {
        $message = 'Apache sites enabled directory not writable. '.
          'Trying: ' . $this->_sites_enabled_dir;
        $this->set_error($message,'system');
        return false;
      }
      if(!is_writable($this->_sites_available_dir)) {
        $message = 'Apache sites available directory not writable. '.
          'Trying: ' . $this->_sites_available_dir();
        $this->set_error($message,'system');
        return false;
      }
      if(!is_writable($this->_sites_enabled_dir)) {
        $message = 'Apache sites enabled directory not writable. '.
          'Trying: ' . $this->_sites_enabled_dir();
        $this->set_error($message,'system');
        return false;
      }

      // Probably has arguments, only check the cmd itself
      $cmd_parts = explode(' ',$this->_reload_apache_cmd);
      $cmd = $cmd_parts[0];
      if(!file_exists($cmd)) {
        $message = 'Apache reload_apache command does not exist. '.
          'Trying: ' . $cmd;
        $this->set_error($message,'system');
        return false;
      }
      // Probably has arguments, only check the cmd itself
      $cmd_parts = explode(' ',$this->_apache_cmd);
      $cmd = $cmd_parts[0];
      if(!file_exists($cmd)) {
        $message = 'Apache check syntax command does not exist. '.
          'Trying: ' . $cmd;
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    // either delete, insert, restore, or update is always called
    function delete() {
      // set delete flag - so we run less rigorous sanity checking
      if(!$this->delete_apache_files()) return false;
      if(!$this->delete_web_directories()) return false;
      if(!$this->reload_apache()) return false;
      if(!$this->purge_x509_files()) return false;
      if(!$this->delete_site_owner_user()) return false;
      return true;

    }

    function disable() {
      if(!$this->disable_site()) return false;
      if(!$this->reload_apache()) return false;
      if(!$this->purge_x509_files()) return false;
      return true;
    }

    function insert() {
      // Make sure any included ssl key files and crts exist and are not
      // password protected. This is only relevant for sites providing
      // their own cert and keys.
      if (!$this->on_new_infrastructure()) {
        if(!$this->ssl_key_and_crt_file_exists()) return false;
        if(!$this->ssl_key_file_not_password_protected()) return false;
      }

      // Ensure the user account that will own the web files exists.
      if (!$this->create_site_owner_user()) return FALSE;

      // Create web directories (if they don't exist) and, only if they
      // don't exist, chowns them.
      if(!$this->create_web_directories()) return false;
      
      // If they have specified a sub folder for the web root
      // make sure it exists (letsencrypt will fail if it doesn't)
      if (!$this->doc_root_exists()) return false;

      if(!$this->create_and_enable_apache_conf_files()) return FALSE;

      if (!$this->on_new_infrastructure()) {
        $tls_key = $this->get_web_conf_tls_key();
        if ($this->get_web_conf_tls() == 1 && empty($tls_key)) {
          // If this is a new site or a site with expired certificates,
          // we will present a site on port 443 that will give tls errors.
          // In our tests, letsencrypt responds to those errors by attempting
          // to validate via port 80.
          // Reload apache so our newly generated config files are active.
          if(!$this->reload_apache()) return false;

          // Now generate files.
          if(!$this->generate_letsencrypt_files()) return false;

          // Regenerate the web configuration - now that we have a letsencrypt
          // certificate, we have to regenerate to ensure they are included.
          if (!$this->create_and_enable_apache_conf_files('https')) return FALSE;
        }
      }
      if ($this->get_web_conf_tls() == 0) {
        // Ensure any left over ssl files or letsencrypt files are cleaned up.
        if(!$this->purge_x509_files()) return false;
      }

      if (!$this->set_user_quota()) return FALSE;
      if(!$this->reload_apache()) return false;
      return true;
    }

    /**
     * Create an enable apache configuration files.
     *
     * But don't reload apache (yet).
     */
    function create_and_enable_apache_conf_files($port = NULL) {
      if (is_null($port)) {
        $ports = $this->get_ports();
      }
      else {
        $ports = array($port);
      }
      foreach($ports as $port) {
        // Create the http configuration.
        $tmp = tempnam(sys_get_temp_dir(), 'red-apache-conf');
        if(!$this->create_apache_conf_file($port, $tmp)) {
          unlink($tmp);
          return false;
        }
        if(!$this->check_apache_syntax($tmp)) {
          unlink($tmp);
          return false;
        }
        if (!$this->move_apache_conf_file_into_place($tmp, $port)) {
          unlink($tmp);
          return false;
        }
        if(!$this->enable_site($port)) return false;
      }
      if ($this->get_web_conf_tls() == 0) {
        // Ensure we don't leave behind a .ssl.conf file.
        $available = $this->get_apache_sites_path('available', 'https');
        $enabled = $this->get_apache_sites_path('enabled', 'https');
        if (file_exists($available)) {
          if (!unlink($available)) {
            $message = 'Failed to unlink un-needed apache ssl configuration file from available directory.';
            $this->set_error($message,'system', 'soft');
            return FALSE;
          }
        }
        // file_exists fails on symlinks if they point to a file that doesn't exist. We want
        // to kill this symlink no matter what.
        if (is_link($enabled)) {
          if (!unlink($enabled)) {
            $message = 'Failed to unlink un-needed apache ssl configuration file from enabled directory.';
            $this->set_error($message,'system', 'soft');
            return FALSE;
          }
        }
      }
      return true;
    }

    // Return an array with http or http and https in it.
    function get_ports() {
      $tls = $this->get_web_conf_tls();
      // There is always an http version.
      $ports = array('http');
      if ($tls == 1) {
        // Add https version as well.
        $ports[] = 'https';
      }
      return $ports;

    }
    /**
     * Regnerate web configuration.
     *
     * Sometimes (when we change the backend) we have to
     * regenerate all web configurations so they include the
     * right directives for the given backend. However, if we
     * reload the web server before all of them are regenerated
     * we may get apache errors. So - this function will just
     * regenerate the web configuration without testing them.
     */
    function regenerate() {
      if(!$this->node_sanity_check()) return false;
      if(!$this->remove_old_sites_paths()) return false;
      if(!$this->create_web_directories()) return false;
      if(!$this->ssl_key_and_crt_file_exists()) return false;
      if(!$this->ssl_key_file_not_password_protected()) return false;
      if (!$this->create_and_enable_apache_conf_files()) return false;
      return true;
    }

    function update() {
      // We do exactly the same thing as insert, however, we first
      // disable the apache configuration files, otherwise the apache
      // syntax check will always fail because it will detect duplicate
      // directives.
      if (!$this->disable_site()) return false;
      if (!$this->insert()) return false;
      return true;
    }

    function restore() {
      // if sites available file exists, create it, otherwise, update it
      if(!file_exists($this->get_apache_sites_path('available', 'http'))) {
        return $this->insert();
      }
      else {
        return $this->update();
      }
    }

    /*
     * HELPER FUNCTIONS
     */

    /**
     * Refactoring function.
     *
     * We are switching from naming apache conf files after the hosting order
     * identifier to naming them after the item id. This function ensures
     * that the old style files are deleted.
     */
    function remove_old_sites_paths() {
      $types = array('available', 'enabled');
      $ports = array('http', 'https');
      foreach($types as $type) {
        foreach($ports as $port) {
          $path = $this->get_old_apache_sites_path($type, $port);
          if (is_link($path)) {
            if (!unlink($path)) {
              $msg = "Failed to remove old style path ($path)";
              $this->set_error($msg, 'system', 'soft');
              return FALSE;
            }
          }
        }
      }
      return TRUE;
    }

    /**
     * Delete the site owner user
     */
    function delete_site_owner_user() {
      $user = $group = $this->get_web_conf_login();
      if ($user == 'site' . $this->get_item_id() . 'writer') {
        if ($this->unix_account_exists($user)) {
          $cmd = "/usr/sbin/userdel";
          $args = [ "--force", $user ];
          $resp = red_fork_exec_wait($cmd, $args);
          if ($resp === FALSE || $resp != 0) {
            $error = "Failed to delete the web site owner user: '$resp'.";
            $this->set_error($error,'system','soft');
            return FALSE;
          }
          $cmd = "/usr/sbin/delgroup";
          $args = [ $user ];
          $resp = red_fork_exec_wait($cmd, $args);
          if ($resp === FALSE || $resp != 0) {
            $error = "Failed to delete the web site owner group: '$resp'.";
            $this->set_error($error,'system','soft');
            return FALSE;
          }
        }
      }
      return TRUE;
    }
    /**
     * Create site writer user
     *
     */
    function create_site_owner_user() {
      $user = $group = $this->get_web_conf_login();
      if ($user == 'site' . $this->get_item_id() . 'writer') {
        // We are using the new infrastructure. We are responsible for
        // creating this user.
        $uid = $gid = $this->get_web_conf_writer_uid();
        $home_dir = $this->get_site_dir();

        if (!$this->unix_account_exists($user)) {
          // First create a group so we can put the read user in this group.
          $cmd = "/usr/sbin/groupadd";
          // Create a group with the same name as the user
          $args = [
            "--gid",
            $gid,
            $group,
          ];
          $resp = red_fork_exec_wait($cmd, $args);
          if ($resp === FALSE || $resp != 0) {
            $error = "Failed to create the web site owner group: '$resp'.";
            $this->set_error($error,'system','soft');
            return FALSE;
          }
          $cmd = "/usr/sbin/useradd";
          // Create a user with a disabled password, and a home directory (but don't
          // try to create the home dir, that will happen later.)
          $args = [
            '--home-dir',
            $home_dir,
            '--no-create-home',
            "--uid",
            $uid,
            "--gid",
            $gid,
            "--shell",
            "/usr/bin/bash",
            $user,
          ];
          $resp = red_fork_exec_wait($cmd, $args);
          if ($resp === FALSE || $resp != 0) {
            $error = "Failed to create the web site owner user.";
            $this->set_error($error,'system','soft');
            return FALSE;
          }
          // Lastly, add the www-data user to this user's group so apache can traverse
          // their web directory.
          $cmd = '/usr/sbin/adduser';
          $args = [ 'www-data', $group ];
          $resp = red_fork_exec_wait($cmd, $args);
          if ($resp === FALSE || $resp != 0) {
            $error = "Failed to add www-data to the user's group.";
            $this->set_error($error,'system','soft');
            return FALSE;
          }
        }
      }


      return TRUE;
    }

    function unix_account_exists($user) {
      return red_key_exists_in_file($user,':',$this->_passwd_file);
    }

    /**
     *
     * Ensure the doc root exists
     *
     * Without a docroot, letsencrypt will fail with a cryptic message.
     *
     */
    function doc_root_exists() {
      $base = $this->get_site_dir();
      $root = ltrim($this->get_web_conf_document_root(), '/');
      if (!is_dir("{$base}/web/{$root}")) {
        $msg = "You specified a document root that does not exist. Please create it before updating your web configuration.";
        $this->set_error($msg, 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    /**
     * When deleting or removing https remove letsencrypt.
     */
    function purge_x509_files() {
      $cert_name = 'site' . $this->get_item_id();
      if ($this->on_new_infrastructure()) {
        $base = '/etc/ssl/mayfirst';
        $renewal = NULL;
        $live = "{$base}/{$cert_name}";
        $archive = NULL;
      }
      else {
        $base = '/etc/letsencrypt';
        $renewal = "{$base}/renewal/{$cert_name}.conf";
        $live = "{$base}/live/{$cert_name}";
        $archive = "{$base}/archive/{$cert_name}";
      }

      if ($renewal && file_exists($renewal)) {
        if (!unlink($renewal)) {
          $message = 'Cannot delete renewal x509 configuration.';
          $this->set_error($message,'system', 'soft');
          return FALSE;
        }
      }
      if (file_exists($live)) {
        if (!red_delete_directory_recursively($live)) {
          $message = 'Cannot delete live x509 dir.';
          $this->set_error($message,'system', 'soft');
          return FALSE;
        }
      }
      if (file_exists($archive)) {
        if (!red_delete_directory_recursively($archive)) {
          $message = 'Cannot delete archive x509 dir.';
          $this->set_error($message,'system', 'soft');
          return FALSE;
        }
      }
      return TRUE;
    }

    function generate_letsencrypt_files() {
      // Use http - if we are inserting, we will only have an
      // http config. In any event, it only provides the server names
      // and path to web config.
      $file = $this->get_apache_sites_path('enabled', 'http');
      if(!file_exists($file)) {
        $message = 'Cannot find apache configuration file when running mf-certbot.';
        $this->set_error($message,'system');
        return FALSE;
      }
      $cert_name = 'site' . $this->get_item_id();
      $env = array('MFCB_CERT_NAME' => $cert_name);
      // First run with --dry-run to see if we get any errors.
      $env['MFCB_DRY_RUN'] = 1;
      $exit_status = red_fork_exec_wait('/usr/local/sbin/mf-certbot', array($file), $env);
      if($exit_status !== 0) {
        $msg = "Generating your https certificate failed. Please ensure that ".
          "all the domains listed for this configuration are properly assigned ".
          "the IP address for the server. You can always add more domains at a ".
          "later date. Or you can change your site to http only for now.";
        $this->set_error($msg, 'system', 'soft');

        // Ensure we don't leave behind a broken configuration
        if (!$this->cleanup_after_failed_letsencrypt()) return FALSE;
        return FALSE;
      }
      // Now run without --dry-run.
      unset($env['MFCB_DRY_RUN']);
      $exit_status = red_fork_exec_wait('/usr/local/sbin/mf-certbot', array($file), $env);
      if($exit_status !== 0) {
        // We still return a soft error - we want the user to be able to adjust/recover.
        $this->set_error("Running mf-certbot returned an error (exit status: $exit_status). You can always change your site to http only for now.", 'system', 'soft');
        if (!$this->cleanup_after_failed_letsencrypt()) return FALSE;
        return FALSE;
      }
      return TRUE;
    }

    // If letsencrypt fails, clean up the apache configurations that aren't
    // working.
    function cleanup_after_failed_letsencrypt() {
      // Ensure we remove the https apache configuration files.
      $types = array('available','enabled');
      foreach($types as $type) {
        $file = $this->get_apache_sites_path($type, 'https');
        if ($type == 'enabled') {
          $exists = is_link($file);
        }
        else {
          $exists = file_exists($file);
        }
        if($exists) {
          if(!unlink($file)) {
            $message = "Unable to delete sites $type file. Trying: " . $file;
            $this->set_error($message,'system');
            return false;
          }
        }
      }
      if(!$this->reload_apache()) return false;
      return TRUE;
    }

    // Getters
    function get_web_related_directory_names() {
      // These are the names of the directories created in the
      // site directory when web is enabled
      $dirs = ['web','logs','cgi-bin','include'];
      if ($this->on_new_infrastructure()) {
        $dirs[] = 'bin';
      }
      return $dirs;
    }

    function get_apache_sites_path($type, $port) {
      $path = '';
      if($type == 'enabled') {
        $path = $this->_sites_enabled_dir . '/';
      }
      elseif($type == 'available') {
        $path = $this->_sites_available_dir . '/';
      }

      $path .= 'site' . $this->get_item_id();
      if($port == 'https') $path .= '.ssl';

      $path .= '.conf';
      return $path;
    }

    function get_old_apache_sites_path($type, $port) {
      $path = '';
      if($type == 'enabled') {
        $path = $this->_sites_enabled_dir . '/';
      }
      elseif($type == 'available') {
        $path = $this->_sites_available_dir . '/';
      }

      $path .= $this->get_hosting_order_identifier();
      if($port == 'https') $path .= '.ssl';

      $path .= '.conf';
      return $path;
    }

    function get_php_ini_dir() {
      return $this->get_site_dir() . "/include/php5";
    }

    function get_bin_dir() {
      return $this->get_site_dir() . '/bin';
    }

    // not used by core mod_php class, but used by both
    // suexec/fcgid and suphp
    function create_php_ini() {
      if ($this->on_new_infrastructure()) {
        // We no longer create the old php5 dir
        $dir = NULL;
        $file = $this->get_site_dir() . '/web';
        $root = $this->get_web_conf_document_root();
        if ($root) {
          $root = trim($root, '/');
          $file .= "/{$root}";
        }
        $file .= "/.user.ini";
      }
      else {
        $dir = $this->get_php_ini_dir();
        $file = $dir . '/php.ini';
      }

      // The file might exist. Don't mess even if it's a symlink to a non-existant
      // file.
      if(file_exists($file) || is_link($file)) return true;

      $contents = "; place your own custom php.ini settings here\n" .
        "; only add settings you wish to change from the server defaults\n";
      if(!file_put_contents($file,$contents)) {
        $message = "Unable to create php ini file. Trying $file";
        $this->set_error($message,'system');
        return false;
      }

      $unix_user_name = $this->get_web_conf_login();
      // the enclosing directory won't by default be properly
      // chown'ed, so do it here
      if ($dir) {
        if(!red_chown($dir,$unix_user_name)) {
          $this->set_error("Failed to chown $file.",'system');
          return false;
        }
      }
      if(!red_chown($file,$unix_user_name)) {
        $this->set_error("Failed to chown $file.",'system');
        return false;
      }
      if(!$this->set_group_permission($file)) {
        $this->set_error("Failed to chgrp $file.",'system');
        return false;
      }
      return true;
    }

    // Action functions
    function delete_apache_files() {
      // Delete both sites available file and sites enabled file
      $types = array('available','enabled');
      $ports = $this->get_ports();
      foreach($types as $type) {
        foreach($ports as $port) {
          $file = $this->get_apache_sites_path($type, $port);
          if ($type == 'enabled') {
            $exists = is_link($file);
          }
          else {
            $exists = file_exists($file);
          }
          if($exists) {
            if(!unlink($file)) {
              $message = "Unable to delete sites $type file. Trying: " . $file;
              $this->set_error($message,'system');
              return false;
            }
          }
        }
      }
      return true;
    }

    function check_apache_syntax($file = false) {
      // If $file is not passed, test the configuration files
      // currently in place. If $file is passed, also test
      // the file passed.
      $cmd = $this->_apache_cmd;
      // for testing
      $args = "-t";
      if($file) {
        // Only check the relevant file. However - we need
        // to include the server wide config file so that
        // we can read in any module conf file that might be
        // referenced in this particular file (and would
        // otherwise fail the syntax check).
        $args .= " -f $file -C 'Include " .
          $this->_apache_server_wide_conf_file . "'";

      }

      // redirect standard error to standard output
      $args .= " 2>&1";
      exec("$cmd $args",$output,$return_value);
      if($return_value != 0) {
        // Failed syntax check - this is a soft error - the user should
        // be allowed to fix it.
        $output = $this->get_htmlentities(implode(' ',$output));
        $error = "Failed apache check syntax command! There is probably ".
          "a typo or other mistake in your apache settings. Output: ".
          "$output.";
        $this->set_error($error,'system','soft');
        return false;
      }
      return true;
    }

    function move_apache_conf_file_into_place($source, $port) {
      if(!file_exists($source)) {
        $message = "Unable to move apache conf file: $source. It doesn't exist.";
        $this->set_error($message);
        return false;
      }
      $target = $this->get_apache_sites_path('available', $port);
      if(!rename($source,$target)) {
        $message = "Failed to move apache conf file into place. Trying: ".
          "$source -> $target";
        $this->set_error($message,'system');
        return false;
      }
      return true;
    }

    function delete_web_directories() {
      // delete all the web related site directories
      $site_dir = $this->get_site_dir();
      $site_dir_symlink = NULL;
      $mountpoint = $this->get_web_conf_mountpoint();

      $names = $this->get_web_related_directory_names();
      if ($mountpoint) {
        $site_dir_symlink = rtrim($site_dir, "/");
        $site_dir = "/media/{$mountpoint}/sites/" . $this->get_item_id();
        array_unshift($names, '.ssh/authorized_keys');
        array_unshift($names, '.ssh');
      }
      foreach($names as $name) {
        $full_path = $site_dir . '/' . $name;
        if(!red_delete_directory_recursively($full_path)) {
          $message = "Unable to delete $full_path";
          $this->set_error($message,'system');
          return false;
        }
      }
      // Attempt to rmdir it. The directory might not be empty if we are using old style
      // path which would include the users directory, in which case this will fail
      // and we ignore the error.
      @rmdir($site_dir);

      if ($site_dir_symlink) {
        // Also, clean up the symlink.
        if (is_link($site_dir_symlink)) {
          if (!unlink($site_dir_symlink)) {
            $this->set_error("Failed to remove site dir symlink.", 'system');
            return FALSE;
          }
        }
      }
      return TRUE;
    }

    function create_web_directories() {
      // Create all the web related site directories
      $site_dir = $this->get_site_dir();
      $site_dir_symlink = NULL;
      $mountpoint = $this->get_web_conf_mountpoint();

      if ($mountpoint) {
        // A mountpoint means we actually store the site in /media/<mountpoint> and
        // create a symlink in /home/.
        $site_dir_symlink = $site_dir;
        // Ensure the enclosing directory for the symlink exists.
        $site_dir_symlink_parts = explode('/', rtrim($site_dir_symlink, '/'));
        array_pop($site_dir_symlink_parts);
        $site_dir_symlink_parent = implode('/', $site_dir_symlink_parts);
        if(!red_create_directory_recursively($site_dir_symlink_parent))  {
          $this->set_error("Failed to create symlink parent.", 'system');
          return FALSE;
        }
        $site_dir = "/media/{$mountpoint}/sites/" . $this->get_item_id();
      }

      // Create/make sure site dir is there
      $group_writable = false;
      if(!red_create_directory_recursively($site_dir, $group_writable))  {
        $message = "Failed to create the site directory. Trying: $site_dir";
        $this->set_error($message,'system');
        return false;
      }

      if ($mountpoint) {
        // Also make sure symlink is there if we are using a mountpoint.
        if (!file_exists($site_dir_symlink)) {
          if (!symlink($site_dir, $site_dir_symlink)) {
            $this->set_error("Failed to create site dir symlink.", 'system');
            return FALSE;
          }
        }
        // Make sure permissions are set properly.
        $group = $this->get_web_conf_login();
        chgrp($site_dir, $group);
        chmod($site_dir, 0750);
        // Drop in a .ssh directory and empty authorized_keys file.
        if (!file_exists("{$site_dir}/.ssh")) {
          mkdir("{$site_dir}/.ssh");
        }
        touch("{$site_dir}/.ssh/authorized_keys");
      }
     
      $names = $this->get_web_related_directory_names();
      foreach($names as $name) {
        $full_path = $site_dir . '/' . $name;
        
        if(!file_exists($full_path)) {
          if(!red_create_directory_recursively($full_path)) {
            $message = "Unable to create ". $full_path;
            $this->set_error($message,'system');
            return false;
          }
          // only create default index file if the web directory did
          // not exist
          if($name == 'web') {
            if(!$this->create_default_index_file()) return false;
          }
        }    
        // Default permissions.
        $chmod = 0755;
        // Exception for logs.
        if($name == 'logs') {
          $chmod = 0750;
        }
        elseif (!$this->on_new_infrastructure()) {
          // On old infrastructure we have to do back flips around permissions.
          // make include, and web writable by the group, they will remain owned by root.
          if($name == 'include' || $name == 'web' || $name == 'cgi-bin' || $name == 'bin') {
            $chmod = 0775;
          }
        }
        if(!chmod($full_path, $chmod)) {
          $message = "Unable to chmod a web directory. Trying ".
            $full_path;
          $this->set_error($message,'system');
          return false;
        }
        if ($this->on_new_infrastructure()) {
          // On new infra, set both user and group.
          $uid = $gid = $this->get_web_conf_writer_uid();
          $owner = "$uid:$gid";
          // Unless it's logs - don't let users mess with these.
          if ($name == 'logs') {
            $owner = "0:$gid";
          }
          $cmd = '/usr/bin/chown';
          $args = [ "$owner", $full_path ];
          $resp = red_fork_exec_wait($cmd, $args);
          if ($resp === FALSE || $resp != 0) {
            $this->set_error("Failed to chown $full_path", 'system', 'soft');
            return FALSE;
          }
        }
        else {
          // on old infra, leave ownership a root, and chgrp the folder
          if(!$this->set_group_permission($full_path)) {
            $message = "Unable to chown/chgrp the web directory. Trying ".
              $full_path;
            $this->set_error($message,'system');
            return false;
          }
        }
        
        // Always ensure web symlink is present in user directory.
        if ($this->on_new_infrastructure()) {
          if($name == 'web') {
            if(!$this->create_user_symlink()) return false;
          }
        }
      }
      if ($this->on_new_infrastructure()) {
        // Pre-create the files and directories they are allowed to write to.
        $paths = [
          '.cache' => [ 'type' => 'dir', ],
          '.config' => [ 'type' => 'dir', ],
          '.drush' => [ 'type' => 'dir', ],
          '.cv' => [ 'type' => 'dir', ],
          '.emacs.d' => [ 'type' => 'dir', ],
          '.local' => [ 'type' => 'dir', ],
          '.npm' => [ 'type' => 'dir', ],
          '.cpan' => ['type' => 'dir', ],
          'perl5' => ['type' => 'dir', ],
          '.poetry' => [ 'type' => 'dir', ],
          '.tmp' => [ 'type' => 'dir' ],
          '.vim' => [ 'type' => 'dir', ],
          '.bash_history' => [ 'type' => 'file', ],
          '.bash_logout' => [ 'type' => 'file', ],
          '.bashrc' => [ 'type' => 'file', ],
          '.drush/drushrc.php' => [ 'type' => 'file', 'content' => "<?php\n\$options['backup-dir'] = getenv('HOME') . '/.drush/backup-dir';\n" ],
          '.emacs' => [ 'type' => 'file', ],
          '.emacs.el' => [ 'type' => 'file', ],
          '.gitconfig' => [ 'type' => 'file', ],
          '.profile' => [ 
            'type' => 'file', 
            'content' => "export PATH=\"~/bin:\$PATH\"\nexport TMPDIR=~/.tmp\nexport TEMPDIR=~/.tmp\nexport TEMP=~.tmp\nexport TMP=~.tmp\n"
          ],
          '.vimrc' => [ 'type' => 'file', 'content' => 'set backupdir=~/.vim' ],
          
        ];
        $uid = $gid = $this->get_web_conf_writer_uid();
        foreach ($paths as $name => $attr) {
          $type = $attr['type'];
          $abs_path = "{$site_dir}/{$name}";
          $content = $attr['content'] ?? NULL;
          if (!file_exists($abs_path)) {
            if ($content) {
              file_put_contents($abs_path, $content);
            }
            else {
              $cmd = '/usr/bin/touch';
              if ($type == 'dir') {
                $cmd = '/usr/bin/mkdir';
              }
              $args = [ $abs_path ];
              $resp = red_fork_exec_wait($cmd, $args);
              if ($resp === FALSE || $resp != 0) {
                $this->set_error("Failed to create $abs_path", 'system', 'soft');
                return FALSE;
              }
            }
            $cmd = '/usr/bin/chown';
            $args = [ "$uid:$gid", $abs_path ];
            $resp = red_fork_exec_wait($cmd, $args);
            if ($resp === FALSE || $resp != 0) {
              $this->set_error("Failed to chown $abs_path", 'system', 'soft');
              return FALSE;
            }
          }
        }
      }
      return true;
    }

    function set_group_permission($path) {
      // Use group_id to avoid problems if the user
      // has not yet been created on the server
      $unix_group_id = $this->get_hosting_order_unix_group_id();

     
      if(!red_chgrp($path,$unix_group_id)) {
        $this->set_error("Failed to chgrp $path.",'system');
        return false;
      }
      return true;
    }
    function create_user_symlink() {
      // Get target path.
      $site_dir = $this->get_site_dir();
      $target = $site_dir . '/web';

      // Get link path.
      $user = $this->get_web_conf_login();
      $home_dir = $this->get_user_home_dir($user);
      if (!$home_dir) {
        $message = "Failed to get user home dir when symlinking web folder.";
        $this->set_error($message,'system', 'soft');
        return FALSE;
      }

      $link = "{$home_dir}/web";

      // The file might exist.
      if(file_exists($link)) {
        return TRUE;
      }

      if (!symlink($target, $link)) {
        $message = "Failed to symlink from user home directory.";
        $this->set_error($message,'system', 'soft');
        return FALSE;
      }

      // Update permissions.
      $unix_group_name = $this->get_hosting_order_unix_group_name();
      if(!red_chown_symlink($link, $user)) {
        $this->set_error("Failed to chown $link.",'system');
        return false;
      }
      if(!red_chgrp_symlink($link, $unix_group_name)) {
        $this->set_error("Failed to chgrp $link.",'system');
        return false;
      }
      return true;
    }

    function get_user_home_dir($user) {
      // Hackey, but there doesn't seem to be a getent in php.
      $file = file("/etc/passwd");
      foreach($file as $line) {
        if (preg_match('/^' . $user . ':x:[0-9]+:[0-9]+:[^:]*:([^:]+):/', $line, $matches)) {
          $homedir = $matches[1];
          if (is_dir($homedir)) {
            return $homedir;
          }
        }
      }
      return NULL;
    }

    function create_default_index_file() {
      $content = file_get_contents($this->_path_to_default_index_template);
      $hosting_order_identifier = $this->get_hosting_order_identifier();
      $content = str_replace('{hosting_order_identifier}',$hosting_order_identifier,$content);

      $site_dir = $this->get_site_dir();
      $file = $site_dir . '/web/index.html';

      // The file might exist.
      if(file_exists($file)) return true;

      if(!red_append_to_file($file,$content)) {
        $message = "Unable to create default index page. Trying ".
          $file;
        $this->set_error($message,'system');
        return false;
      }

      // fix permissions
      if(!chmod($file,0664)) {
        $message = "Unable to chmod the default index file. Trying ".
          $file;
        $this->set_error($message,'system');
        return false;
      }

      $unix_user_name = $this->get_web_conf_login();
      $unix_group_name = $this->get_hosting_order_unix_group_name();
      if(!red_chown($file,$unix_user_name)) {
        $this->set_error("Failed to chown $file.",'system');
        return false;
      }
      if(!red_chgrp($file,$unix_group_name)) {
        $this->set_error("Failed to chgrp $file.",'system');
        return false;
      }
      return true;
    }

    
    function disable_site($port = NULL) {
      if (is_null($port)) {
        $ports = $this->get_ports();
      }
      else {
        $ports = array($port);
      }
      foreach($ports as $port) {
        // delete the sites enabled file
        $target = $this->get_apache_sites_path('enabled', $port);
        // We should only need is_link, but for legacy resaons we seem
        // to have some real files in some places and we want to be sure
        // we get rid of any file with this name here. Also file_exists
        // won't work because it returns FALSE if the target is missing
        // and we want to blow away any file whether or not the target
        // exists.
        if(is_link($target) || is_file($target)) {
          if(!unlink($target)) {
            $message = "Failed to disabled the site.";
            $this->set_error($message,'system');
            return false;
          }
        }
      }
      return true;
    }

    function enable_site($port = NULL) {
      if (is_null($port)) {
        $ports = $this->get_ports();
      }
      else {
        $ports = array($port);
      }
      foreach($ports as $port) {
        $target = $this->get_apache_sites_path('enabled', $port);
        $source = $this->get_apache_sites_path('available', $port);
        if(is_link($target) && readlink($target) == $source) {
          // This is a noop - our link exists and is pointing to the right place.
          continue;
        }
        // If a file exists, then delete it. We have some legacy cruft with old enabled
        // files that are real files, not symlinks. NOTE: file_exists returns FALSE if the
        // file is a symlink and the symlink target does not exist. So, we check for is_link
        // to ensure we delete whatever is here.
        if (file_exists($target) || is_link($target)) {
          if (!unlink($target)) {
            $this->set_error("Unable to remove pre-existing enabled file: '$target'.",'system');
            return false;
          }
        }
        // Now create the symlink.
        if(!symlink($source,$target)) {
          $this->set_error("Unable to create symlink source '$source' to target '$target'.",'system');
          return false;
        }
      }
      return true;
    }

    function reload_apache() {
      // First check syntax and sanity
      if(!$this->check_apache_syntax() || !$this->check_apache_sanity())  {
        return false;
      }

      // attempt reload
      $args = explode(' ', $this->_reload_apache_cmd);
      $cmd = array_shift($args);

      $return_value = red_fork_exec_wait($cmd, $args);
      if($return_value != 0) {
        // Failure! This is potentially very bad. Apache might be shutdown
        $this->set_error("Failed apache reload command despite passing syntax check!",'system');
        return false;
      }
      return true;
    }

    // Place any additional checks we should run that can only
    // be run after the other steps have completed and just before
    // reloading apache.
    function check_apache_sanity() {
      if(!$this->_delete && !file_exists($this->get_site_dir() . '/logs') && $this->get_web_conf_logging() > 0) {
        $this->set_error("Your logs directory does not exist. Please create a directory in " . 
          $this->get_site_dir() . " called 'logs'. Your web site is being disabled until this change is made.",
          'system','soft');
        $this->disable_site();
        return false;
      }
      return true;
    }
    
    function ssl_key_and_crt_file_exists() {
      $tls_key_file = trim($this->get_web_conf_tls_key());
      $tls_cert_file = trim($this->get_web_conf_tls_cert());
      foreach(array($tls_key_file, $tls_cert_file) as $file) {
        if(!empty($file) && !file_exists($file)) {
          $this->set_error("The SSL Certificate file you specified does ".
            "not exist. Trying: '" . $parts[1] ."'",'system','soft');
          return false;
        }
      }
      return true;
    }

    function ssl_key_file_not_password_protected() {
      $file = trim($this->get_web_conf_tls_key());
      if (empty($file)) {
        return TRUE;
      }
      $command = "/usr/bin/openssl";
      $args = array('rsa', '-passin', 'pass:', '-noout', '-in', $file);
      $return = red_fork_exec_wait($command, $args);
      // should return 0 on success, 1 on error
      if($return == 1) {
        $this->set_error("The SSL Key file you specified appears ".
          "to be password protected.",'system','soft');
        return false;
      }
      return true;
    }

    // Override to make exception depending on whether or not you
    // are on the new infrastructure.
    function get_hosting_order_unix_group_name() {
      if ($this->on_new_infrastructure()) {
        // Group name is same as user.
        return $this->get_web_conf_login();
      }
      return parent::get_hosting_order_unix_group_name();
    }
    function get_hosting_order_unix_group_id() {
      if ($this->on_new_infrastructure()) {
        return intval($this->get_web_conf_writer_uid());
      }
      return parent::get_hosting_order_unix_group_id();
    }
  }
}


?>
