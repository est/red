<?php
if (!class_exists('red_item_mailbox_node_postfix')) {
  class red_item_mailbox_node_postfix extends red_item_mailbox {

    var $_maildirmake_cmd = "/usr/bin/maildirmake.dovecot";
    var $_usermod_cmd = "/usr/sbin/usermod";
    var $_useradd_cmd = "/usr/sbin/useradd";
    var $_userdel_cmd = "/usr/sbin/userdel";
    var $_groupadd_cmd = "/usr/sbin/groupadd";
    var $_postmap_cmd = "/usr/sbin/postmap";
    var $_passwd_file = '/etc/passwd';
    var $_auto_response_db_name = '.red.auto_response.db';
    var $_auto_responder_cmd = "/usr/local/share/red/node/sbin/red-auto-responder";
    var $_virtual_alias_maps_file = "/etc/postfix/virtual_alias_maps.red";
    var $_auto_response_db; // auto response sqlite db.

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to FALSE on error
      if (!$this) return;
    }

    function delete() {
      if (!$this->rebuild_virtual_alias_maps()) return FALSE;
      if (!$this->delete_maildir()) return FALSE;
      if (!$this->delete_user()) return FALSE;
      return TRUE;
    }

    function disable() {
      if (!$this->rebuild_virtual_alias_maps()) return FALSE;
      if (!$this->disable_user()) return FALSE;
      return TRUE;
    }

    function insert() {
      if (!$this->rebuild_virtual_alias_maps()) return FALSE;
      if (!$this->create_user()) return FALSE;
      if (!$this->create_maildir()) return FALSE;
      if (!$this->update_auto_response_settings()) return FALSE;
      if (!$this->set_user_quota()) return false;

      return TRUE;
    }

    function update() {
      // we have to be able to restore from being disabled.
      return $this->restore();
    }

    function restore() {
      if (!$this->rebuild_virtual_alias_maps()) return FALSE;
      if (!$this->unix_account_exists()) {
        if (!$this->create_user()) return FALSE;
      }
      else {
        if (!$this->update_user_password()) return FALSE;
        if (!$this->update_home_dir_symlink()) return FALSE;
      }
      if (!$this->enable_user()) return FALSE;
      if (!$this->maildir_exists()) {
        if (!$this->create_maildir()) return FALSE;
      }
      if (!$this->update_auto_response_settings()) return FALSE;
      if (!$this->set_user_quota()) return false;
      return TRUE;
    }

    function unix_account_exists() {
      return red_key_exists_in_file($this->get_mailbox_login(),':',$this->_passwd_file);
    }

    function get_maildir() {
      return $this->get_home_dir() . "/Maildir";
    }

    function get_home_dir() {
      if ($this->on_new_infrastructure()) {
        // New style.
        return "/home/users/" . $this->get_mailbox_login();
      }
      // Old style.
      $homedir_template = '/home/members/{member_name}/sites/{identifier}/users/{login}'; 
      $find = array('{member_name}',
              '{identifier}',
              '{login}');
      $replace = [
        $this->get_member_unix_group_name(),
        $this->get_hosting_order_identifier(),
        $this->get_mailbox_login(),
      ]; 
      return str_replace($find, $replace, $homedir_template);
    }

    function maildir_exists() {
      return is_dir($this->get_maildir());
    }

    function node_sanity_check() {
      if (!file_exists($this->_maildirmake_cmd)) {
        $message = 'Maildirmake command does not exist. Trying: ' . 
          $this->_maildirmake_cmd;
        $this->set_error($message,'system');
        return FALSE;
      }
        
      if (!file_exists($this->_usermod_cmd)) {
        $message = 'usermod command command does not exist. Trying: ' . 
          $this->_usermod_cmd;
        $this->set_error($message,'system');
        return FALSE;
      }
      return TRUE;
    }

    function create_maildir() {
      if (!$this->maildir_exists()) {
        $cmd = $this->_maildirmake_cmd;
        $args = [ $this->get_maildir() ];
        if (red_fork_exec_wait($cmd, $args) != 0) {
          $this->set_error("Failed to create maildir: " . $this->get_maildir(), 'system');
          return FALSE;
        }
        if (!$this->maildir_exists()) {
          $this->set_error("Failed to create maildir.",'system');
          return FALSE;
        }
        if (!$this->chown_maildir()) return FALSE;
      }
      return TRUE;
    }

    function chown_maildir() {
      $maildir = $this->get_maildir();
      $login = $this->get_mailbox_login();
      $dirs = array($maildir, "$maildir/new", "$maildir/cur", "$maildir/tmp");
      foreach($dirs as $dir) {
        if (!red_chown($dir, $login)) {
          $this->set_error("Failed to chown $dir .",'system');
          return FALSE;
        }
        if (!red_chgrp($dir, 'users')) {
          $this->set_error("Failed to chgrp $dir.",'system');
          return FALSE;
        }
      }
      return TRUE;
    }

    function get_user_account_field($field) {
      $login = $this->get_mailbox_login();
      // The user account info lives in the user account table.
      $field = 'user_account_' . $field;
      $sql = "SELECT !field FROM red_item JOIN red_item_user_account USING(item_id) WHERE 
        item_status != 'deleted' AND user_account_login = @login";
      $result = red_sql_query($sql, ['!field' => $field, '@login' => $login]);
      $row = red_sql_fetch_row($result);
      return $row[0];
    }

    function update_user_password() {
      if (!$this->on_new_infrastructure()) {
        // If this is a mosh, the password is updated when the user account is
        // updated.
        return TRUE;
      }
      $pass = $this->get_user_account_field('password');
      $cmd = $this->_usermod_cmd;

      $args = [
        "--password",
        $pass,
        $this->get_mailbox_login()
      ];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $error = "Failed to update the user password.";
        $this->set_error($error, 'system', 'soft');
        return FALSE;
      }
      return TRUE;

    }

    function update_home_dir_symlink() {
      if (!$this->on_new_infrastructure()) {
        // No symlinks on moshes.
        return TRUE;
      }
      $mountpoint = $this->get_mailbox_mountpoint();
      if (!$mountpoint) {
        $message = "Mountpoint is not set.";
        $this->set_error($message, 'system');
        return FALSE;
      }
      $login = $this->get_mailbox_login();
      // We have to put the home directory in the mount point and
      // create a symlink to the user facing home dir.
      $target_want = "/media/{$mountpoint}/users/{$login}";

      if (!is_dir($target_want)) {
        $message = "Mountpoint path doesn't exist. Please create it first.";
        $this->set_error($message, 'system');
        return FALSE;
      }
      $homedir = $this->get_home_dir();
      $target_actual = rtrim(readlink($homedir), '/');
      if (!$target_actual) {
        $message = "Home dir is not a symlink?";
        $this->set_error($message, 'system');
        return FALSE;
      }
      if ($target_want == $target_actual) {
        // Nothing to do, already set properly.
        return TRUE;
      }
      if (!unlink($homedir)) {
        $message = "Failed to unlink home directory when re-creating symlink.";
        $this->set_error($message, 'system');
        return FALSE;
      }
      if (!symlink($target_want, $homedir)) {
        $message = "Failed to create updated symlink.";
        $this->set_error($message, 'system');
        return FALSE;
      }
      return TRUE;
    }

    function create_user() {
      if(!$this->unix_account_exists()) {
        // We should only be creating users on new style servers, not on old
        // moshes.
        if (!$this->on_new_infrastructure()) {
          $message = "Missing user account on mosh.";
          $this->set_error($message, 'system');
          return FALSE;
        }

        $user_dir = '/home/users';
        if (!red_create_directory_recursively($user_dir)) {
          $message = "Failed to create /home/users directory.";
          $this->set_error($message,'system');
          return FALSE;
        }
        $uid = $this->get_user_account_field('uid');
        $login = $this->get_mailbox_login();
        $mountpoint = $this->get_mailbox_mountpoint();
        if (empty($mountpoint)) {
          $this->set_error("Mount point is not set.", "system", "soft");
          return FALSE;
        }

        // We have to put the home directory in the mount point and
        // create a symlink to the user facing home dir.
        $mountpath = "/media/{$mountpoint}";
        if (!is_dir($mountpath)) {
          // Something is wrong, mountpoint is not mounted.
          $this->set_error("Mountpoint: /media/{$mountpoint} is not mounted.", "system", "soft");
          return FALSE;
        }
        $mount_user_dir = "{$mountpath}/users";
        if (!red_create_directory_recursively($mount_user_dir)) {
          $message = "Failed to create mount path site user directory.";
          $this->set_error($message, 'system', 'soft');
          return FALSE;
        }

        // We first create the user with the home dir on the mount
        // point. Then we create a symlink in /home/users/<login> and
        // update the user.
        $real_home = "{$mount_user_dir}/{$login}";
        $symlink_home = $this->get_home_dir();
        $pass = $this->get_user_account_field('password');
        $cmd = $this->_useradd_cmd;  
        $args = array(
          '-u', $uid,
          '--no-user-group', // They will all be in the users group.
          '-d', $real_home,
          '-s', '/bin/bash',
          '-p', $pass,
          '-m', // create home directory
          $login
        );

        if (red_fork_exec_wait($cmd, $args) != 0) {
          $error = "Failed to create the new user.";
          $this->set_error($error,'system','hard');
          return FALSE;
        }
        
        // We want the user to think their home directory is in /home/users/<login>
        // not in /media/<mountoint/users/login, so we give them a symlink and update their
        // user account to point to the symlink.
        if (!symlink($real_home, $symlink_home)) {
          $this->set_error("Failed to symlink real home ($real_home) to mount home ($symlink_home).", 'system', 'soft');
          return FALSE;
        }
        $cmd = $this->_usermod_cmd;  
        $args = array(
          "-d", $symlink_home,
          $login
        );
        if (red_fork_exec_wait($cmd, $args) != 0) {
          $error = "Failed to update home directory.";
          $this->set_error($error,'system','soft');
          return FALSE;
        }
      }
      return TRUE;
    }

    function delete_maildir() {
      $home_dir = $this->get_home_dir();
      $maildir = "{$home_dir}/Maildir";
      $ret = red_delete_directory_recursively($maildir);
      if (!$ret) {
        $this->set_error("Failed to delete the Maildir: $maildir", 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    function delete_user() {
      if (!$this->unix_account_exists()) {
        // Already deleted.
        return TRUE;
      }

      if (!$this->on_new_infrastructure()) {
        // Old style moshes don't delete users.
        return TRUE;
      }

      $login = $this->get_mailbox_login();
      // First stop and kill any running processes in use by the user.
      $cmd = "/usr/bin/killall";
      $args = [
        '-STOP',
        '--user',
        $login
      ];
      red_fork_exec_wait($cmd, $args);
      $cmd = "/usr/bin/killall";
      $args = [
        '-9',
        '--user',
        $login
      ];
      red_fork_exec_wait($cmd, $args);

      $cmd = $this->_userdel_cmd;  
      $mountpoint = $this->get_mailbox_mountpoint();
      $args = [
        $login,
      ];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $error = "Failed to delete the user.";
        $this->set_error($error,'system','hard');
        return false;
      }

      // The command above doesn't clean up the directories. 
      $mountpoint_dir = "/media/{$mountpoint}/users/{$login}";
      if (is_dir($mountpoint_dir)) {
        if (!red_delete_directory_recursively($mountpoint_dir)) {
          $this->set_error("Failed to delete mountpoint directory.", 'system', 'hard');
          return FALSE;
        }
      }
      $home_dir = $this->get_home_dir();
      if (is_link($home_dir)) {
        if (!unlink($home_dir)) {
          $message = 'Failed to unlink home directory symlink.';
          $this->set_error($message,'system', 'soft');
          return FALSE;
        }
      }
      return TRUE;
    }

    function enable_user() {
      if (!$this->on_new_infrastructure()) {
        return TRUE;
      }
      // unset expiration date
      $login = $this->get_mailbox_login();
      $cmd = $this->_usermod_cmd;  
      $args = [
        '-e',
        '',
        $login,
      ];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $error = "Failed to enable the user $login.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return TRUE;
    }

    function disable_user() {
      if (!$this->on_new_infrastructure()) {
        // We can't disable Maildirs on moshes. We have to rely on
        // them falling out of the postfix map files.
        return TRUE;
      }
      // set expiration date to 1  
      $login = $this->get_mailbox_login();
      $cmd = $this->_usermod_cmd;  
      $args = [
        '-e',
        '1',
        $login,
      ];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $error = "Failed to disable the user $login.";
        $this->set_error($error,'system','hard');
        return false;
      }
      return TRUE;
    }

    function update_auto_response_settings() {
      // If the autoresponder_action is empty, remove the auto responder
      // reference in the .forward file and delete the auto_response_db.
      // If it is not empty, add to the forward file and initialize db
      $action = $this->get_mailbox_auto_response_action();
      if (empty($action)) {
        if (!$this->unset_auto_response()) return false;
      } else {
        if (!$this->set_auto_response()) return false;
      }
      return true;
    }

    function set_auto_response() {
      if (!$this->initialize_auto_response_db()) return false;
      if (!$this->update_auto_response_db()) return false;
      // first delete from forward file to avoid adding the same
      // line twice
      if (!$this->delete_from_forward_file()) return false;
      if (!$this->update_forward_file()) return false;
      return true;
    }

    function unset_auto_response() {
      if (!$this->unlink_auto_response_db()) return false;
      if (!$this->delete_from_forward_file()) return false;
      return true;
    }

    function unlink_auto_response_db() {
      $home = $this->get_home_dir() . '/';
      $db = $home . $this->_auto_response_db_name;
      if (file_exists($db)) {
        if (!unlink($db)) {
          $message = "Unable to delete auto response db. Trying '$db'.";
          $this->set_error($message,'system');
          return false;
        }
      }
      return true;
    }

    function delete_from_forward_file() {
      $home = $this->get_home_dir() . '/';
      $forward = $home . '.forward';
      if (file_exists($forward)) {
        $lines = file($forward);
        foreach ($lines as $k => $v) {
          if(preg_match("#$this->_auto_responder_cmd#",$v)) {
            unset($lines[$k]);
          }
        }
        if (count($lines) == 0)  {
          if(!unlink($forward)) {
            $this->set_error("Failed to unlink '$forward'",'system');
            return false;
          }
        } else {
          if(!file_put_contents($forward,implode("\n",$lines))) {
            $this->set_error("failed to resave '$forward'",'system');
            return false;
          }
        }
      }
      return true;
    }

    function update_auto_response_db() {
      $message = $this->get_mailbox_auto_response();
      $reply_from = $this->get_mailbox_auto_response_reply_from();
      // Delete existing settings
      $sql = "DELETE FROM settings";
      if (!$this->_auto_respose_db->query($sql)) {
        $this->set_error("Failed to delete existing settings.",'system');
        return false;
      }
      // Insert new settings
      $message = $this->_auto_respose_db->quote($message);
      $sql = "INSERT INTO settings (name,value) VALUES('message',$message)";
      if (!$this->_auto_respose_db->query($sql)) {
        $this->set_error("Failed to insert message.",'system');
        return false;
      }
      $reply_from = $this->_auto_respose_db->quote($reply_from);
      $sql = "INSERT INTO settings (name,value) VALUES('reply_from',$reply_from)";
      if (!$this->_auto_respose_db->query($sql)) {
        $this->set_error("Failed to insert message.",'system');
        return false;
      }
      return true;
    }

    function update_forward_file() {
      $action = $this->get_mailbox_auto_response_action();
      $parts = array();
      if($action == 'respond_and_deliver') {
        $parts[] = $this->get_mailbox_login();
      }
      $parts[] = '"|' . $this->_auto_responder_cmd . '"';
      $line = implode(', ',$parts);
      $file = $this->get_home_dir() . '/.forward';
      if(!red_append_to_file($file,$line)) {
        $this->set_error("Failed to append to '$file'",'system');
        return false;
      }
      return true;
    }

    function initialize_auto_response_db() {
      $home = $this->get_home_dir() . '/';
      $db_path = $home . $this->_auto_response_db_name;

      // see if the db already exists
      $exists = false;

      // if it does exists, we will still initialize...
      if(file_exists($db_path)) {
        $exists = true;
      }
      if(!$this->_auto_respose_db = new PDO("sqlite:$db_path")) {
        $this->set_error("Failed to initialize sqlite from '$db_path'",'system');
        return false;
      }
      // ... but we won't re-create the tables.
      if($exists) return true;

      $sql = 'CREATE TABLE settings (name varchar(20), value text)';
      $this->_auto_respose_db->query($sql);
      $sql = 'CREATE TABLE log (email varchar(128), ts timestamp)';
      $this->_auto_respose_db->query($sql);

      // chown to the current user
      $username = $this->get_mailbox_login();
      if(!red_chown($db_path, $username)) {
        $this->set_error("Failed to chown '$db_path' to '$username'",'system');
        return false;
      }
      if(!red_chgrp($db_path, "users")) {
        $this->set_error("Failed to chgrp '$db_path' to 'users'",'system');
        return false;
      }
      if(!chmod($db_path,0600)) {
        $this->set_error("Failed to chmod '$db_path' to 0600",'system');
        return false;
      }
      return true;
    }

    

    // The virtual alias maps file contains info on how to deliver email sent to
    // @mail.mayfirst.org, so it should be updated when the mailbox record is changed.
    function rebuild_virtual_alias_maps() {
      if (!$this->on_new_infrastructure()) {
        if (!red_write_virtual_alias_maps_red_file($this->get_item_host(), $this->_virtual_alias_maps_file)) {
          $this->set_error("Failed to write the virtual alias maps file.", "system", "soft");
          return FALSE;
        }
      }
      return TRUE;

    }
  }  
}


?>
