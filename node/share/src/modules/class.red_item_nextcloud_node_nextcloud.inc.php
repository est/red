<?php
if(!class_exists('red_item_nextcloud_node_nextcloud')) {
  class red_item_nextcloud_node_nextcloud extends red_item_nextcloud {
    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

      // red_item will reset this to false on error
      if(!$this) return;
    }

    function delete() {
      if(!$this->delete_user_from_nextcloud()) return false;
      return true;
    }

    function disable() {
      return $this->disable_user_in_nextcloud();
    }

    function insert() {
      if(!$this->add_user_to_nextcloud()) return false;
      // When inserting, the command to retrieve the current disk usage often
      // fails (because the user is not yet fully create?). So, set the disk
      // usage to 0 to circumvent trying to retrieve it.
      $disk_usage = 0;
      if(!$this->set_user_quota($disk_usage)) return false;
      return true;
    }

    function update() {
      if (!$this->enable_user_in_nextcloud()) return FALSE;
      if (!$this->set_user_quota()) return false;
      $login = $this->get_nextcloud_login();
      return TRUE;
    }

    function restore() {
      $this->insert(); 
      return true;
    }

    function node_sanity_check() {
      return true;
    }
    
    // Override set user quota - we do it via nextcloud 
    // command not user fileystem.
    function set_user_quota($disk_usage = NULL) {
      $args = [ 
        'user:setting',
        $this->get_nextcloud_login(),
        'files',
        'quota',
        $this->get_item_quota()
      ];
      if (!$this->run_occ_command($args)) return FALSE; 
      return $this->update_disk_usage($disk_usage);
    }

    function disable_user_in_nextcloud() {
      return $this->run_occ_command(['user:disable', $this->get_nextcloud_login()]);
    }

    function enable_user_in_nextcloud() {
      return $this->run_occ_command(['user:enable', $this->get_nextcloud_login()]);
    }

    function delete_user_from_nextcloud() {
      return $this->run_occ_command(['user:delete', $this->get_nextcloud_login()]);
    }

    function add_user_to_nextcloud() {
      return $this->run_occ_command(['mayfirstauth:presaveuser', $this->get_nextcloud_login()]);
    }
    
    function run_occ_command($args) {
      $cmd = '/usr/local/sbin/mf-nextcloud-occ-wrapper';
      // Insert 'mayfirst' as the first argument to the command.
      array_unshift($args, 'mayfirst');
      if(FALSE === red_fork_exec_wait($cmd, $args)) {
        $this->set_error("Failed to execute occ wraper.", 'system', 'soft');
        return FALSE;
      }
      return TRUE;

    }
  }
}


?>
