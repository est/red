<?php
require_once('class.red_item_web_conf_node_apache2.inc.php');
if(!class_exists('red_item_web_conf_node_apache2_suexec_fcgid')) {
  class red_item_web_conf_node_apache2_suexec_fcgid extends red_item_web_conf_node_apache2 {
    // overridden functions
    function get_apache_conf_file_children_additions() {
      $group_name = $this->get_hosting_order_unix_group_name();
      $user = $this->get_web_conf_execute_as_user();
      $ret = "\tFCGIWrapper " . $this->get_fcgi_wrapper_path() . " .php\n";
      $ret .= "\tSuexecUserGroup $user $group_name\n";
      return $ret;
    }

    function get_web_related_directory_names() {
      $core = parent::get_web_related_directory_names();
      // add bin and include/php5
      $core[] = 'bin';
      $core[] = 'include/php5';
      return $core;
    }

    function create_web_directories() {
      if(!parent::create_web_directories()) return false;

      if(!$this->create_fcgi_wrapper()) return false;
      if(!$this->chown_fcgi_wrapper()) return false;
      if(!$this->create_php_ini()) return false;

      return true;
    }
      
    // helper functions
    function get_fcgi_wrapper_path() {
      return $this->get_bin_dir() . "/php-cgi";
    }

    function get_fcgi_wrapper_contents() {
      return "#!/bin/bash\n" .
        "exec /usr/bin/php5-cgi -c /etc/php5/cgi -c " . $this->get_php_ini_dir() . "\n";
    }

    function create_fcgi_wrapper() {
      $file = $this->get_fcgi_wrapper_path();
      $dir = $this->get_bin_dir();

      // The file might exist - if we are creating an ssl version of an
      // existing site.
      if(file_exists($file)) return true;

      $contents = $this->get_fcgi_wrapper_contents(); 
      if(!file_put_contents($file,$contents)) {
        $message = "Unable to create fcgi wrapper. Trying $file";
        $this->set_error($message,'system');
        return false;
      }

      // fix permissions
      if(!chmod($file,0755)) {
        $message = "Unable to chmod the default index file. Trying $file";
        $this->set_error($message,'system');
        return false;
      }
      if(!$this->set_group_permission($file)) {
        $this->set_error("Failed to chgrp $file.",'system');
        return false;
      }


      return true;
    }

    function chown_fcgi_wrapper() {
      $file = $this->get_fcgi_wrapper_path();
      $dir = $this->get_bin_dir();

      $unix_user_name = $this->get_web_conf_execute_as_user();
      if(!red_chown($dir,$unix_user_name)) {
        $this->set_error("Failed to chown $dir.",'system');
        return false;
      }
      if(!red_chown($file,$unix_user_name)) {
        $this->set_error("Failed to chown $file.",'system');
        return false;
      }
      return true;
    }
  }
}
