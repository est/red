<?php
if(!class_exists('red_item_cron_node_systemd')) {
  class red_item_cron_node_systemd extends red_item_cron {

    /*
     * MAIN FUNCTIONS
     */

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
    }

    function delete() {
      if (!$this->systemd_stop_and_disable_services()) return FALSE;
      if (!$this->delete_files()) return FALSE;
      return $this->systemd_daemon_reload();
    }
    function disable() {
      return $this->delete();
    }
    function insert() {
      if (!$this->enable_linger()) return FALSE;
      if (!$this->generate_files()) return FALSE;
      if (!$this->systemd_daemon_reload()) return FALSE;
      return $this->systemd_enable_and_restart_services();
    }

    function update() {
      return $this->insert();
    }

    function restore() {
      return $this->insert();
    }
      
    function enable_linger() {
      // This I don't understand. We can't seem to "enable" dbus but if it's not running, we can't enable
      // linger.
      $cmd = "/usr/bin/systemctl";
      $args = [ 'start', 'dbus'];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $this->set_error("Failed to start dbus. Please contact support.", 'system', 'soft');
        return FALSE;
      }
      // This command is indempotent.
      $user = $this->get_user();
      $cmd = '/usr/bin/loginctl';
      $args = [ 'enable-linger', $user];
      if (red_fork_exec_wait($cmd, $args) != 0) {
        $this->set_error("Failed to enable linger for user.", 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    function get_service_name() {
      return 'red-item-' . $this->get_item_id();
    }
    function systemd_stop_and_disable_services() {
      if ($this->get_cron_schedule() == $this::$FOREVER) {
        // If the service file is already gone, just return.
        if (file_exists($this->get_service_dir() . $this->get_service_name() . '.service')) {
          $args = [ 'disable', $this->get_service_name() . '.service' ];
          if (!$this->execute_systemctl_command($args)) {
            $this->set_error("Failed to disable the service", 'system', 'soft');
            return FALSE;
          }
          $args = [  'stop', $this->get_service_name() . '.service' ];
          if (!$this->execute_systemctl_command($args)) {
            $this->set_error("Failed to stop the service", 'system', 'soft');
            return FALSE;
          }
        }
      }
      else {
        if (file_exists($this->get_service_dir() . $this->get_service_name() . '.timer')) {
          $args = [ 'disable', $this->get_service_name() . '.timer' ];
          if (!$this->execute_systemctl_command($args)) {
            $this->set_error("Failed to disable the timer", 'system', 'soft');
            return FALSE;
          }
          $args = [ 'stop', $this->get_service_name() . '.timer' ];
          if (!$this->execute_systemctl_command($args)) {
            $this->set_error("Failed to stop the timer", 'system', 'soft');
            return FALSE;
          }
        }
      }
      return TRUE;
    }

    function get_uid() {
      $user = $this->get_user();
      $output = [];
      $result_code = NULL;
      exec("id -u " . escapeshellarg($user), $output, $result_code);
      if ($result_code != 0) {
        $this->set_error("Failed to run id to get user id, please contact support.", 'system', 'soft');
        return FALSE;
      }
      return $output[0] ?? NULL;
    }

    // Fixme. We should be able to simply run:
    // systemctl --user --machine <user>@ <command> 
    // But I am getting "Cannot access user instance remotely."
    // So we are using this sudo abomonition.
    function execute_systemctl_command($args, $attempt = 1) {
      $uid = $this->get_uid();
      if (!$uid) {
        return FALSE;
      }
      $xdg_runtime_dir = "/run/user/$uid";
      if (!file_exists($xdg_runtime_dir)) {
        if ($attempt == 1) {
          // Sometimes it takes a few seconds to show up after we enable linger
          sleep(5);
          $attempt++;
          return $this->execute_systemctl_command($args, $attempt);
        }
        $this->set_error("XDG_RUNTIME_DIR does not exist ($xdg_runtime_dir), please contact support.", 'system', 'soft');
        return FALSE;
      }
      $user = $this->get_user();
      $environment = "XDG_RUNTIME_DIR=$xdg_runtime_dir";
      $cmd = "/usr/bin/sudo";
      $args = array_merge([
        '-u',
        $user,
        '--login',
        $environment,
        "--",
        "systemctl",
        "--user"
      ], $args);
      $result_code = red_fork_exec_wait($cmd, $args);
      if ($result_code != 0) {
        red_log("systemctl failure, result code: $result_code, cmd: $cmd and args: " . implode(" ", $args));
        return FALSE;
      }
      return TRUE;
    }

    function systemd_enable_and_restart_services() {
      if ($this->get_cron_schedule() == $this::$FOREVER) {
        // Before we can enable the service, we have to create a multi-user.target.wants
        // directory owned by the user.
        $multi_user_target_wants = $this->get_service_dir() . 'multi-user.target.wants';
        if (!file_exists($multi_user_target_wants)) {
          if (!mkdir($multi_user_target_wants)) {
            $this->set_error("Failed to create multi-user.target.wants. Please contact support.", 'system', 'soft');
          }
          if (!chown($multi_user_target_wants, $this->get_user())) {
            $this->set_error("Failed to chown multi_user.target.wants. Please contact support.", 'system', 'soft');
          }
        }
        $args = [ 'enable', $this->get_service_name() . '.service' ];
        if (!$this->execute_systemctl_command($args)) {
          $this->set_error("Failed to enable the service", 'system', 'soft');
          return FALSE;
        }
        $args = [ 'restart', $this->get_service_name() . '.service' ];
        if (!$this->execute_systemctl_command($args)) {
          $this->set_error("Failed to restart the service", 'system', 'soft');
          return FALSE;
        }
      }
      else {
        // Before we can enable the timer, we have to create a default.target.wants
        // directory owned by the user.
        $default_target_wants = $this->get_service_dir() . 'default.target.wants';
        if (!file_exists($default_target_wants)) {
          if (!mkdir($default_target_wants)) {
            $this->set_error("Failed to create default.target.wants. Please contact support.", 'system', 'soft');
          }
          if (!chown($default_target_wants, $this->get_user())) {
            $this->set_error("Failed to chown default.target.wants. Please contact support.", 'system', 'soft');
          }
        }
        $args = [ 'enable', $this->get_service_name() . '.timer' ];
        if (!$this->execute_systemctl_command($args)) {
          $this->set_error("Failed to enable the timer", 'system', 'soft');
          return FALSE;
        }
        $args = [ 'restart', $this->get_service_name() . '.timer' ];
        if (!$this->execute_systemctl_command($args)) {
          $this->set_error("Failed to restart the timer", 'system', 'soft');
          return FALSE;
        }
      }
      return TRUE;
    }
    function systemd_daemon_reload() {
      $uid = $this->get_uid();
      if (!$uid) {
        return TRUE;
      }
      $xdg_runtime_dir = "/run/user/$uid";
      if (!file_exists($xdg_runtime_dir)) {
        // If we are deleting a job and there is no run user directory, daemon-reload will
        // fail, and no need to run it.
	return TRUE;
      }
      $args = [ 'daemon-reload' ];
      if (!$this->execute_systemctl_command($args)) {
        $this->set_error("Failed to run systemctl --user daemon-reload", 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    function delete_files() {
      $file = $this->get_service_dir() . $this->get_service_name() . '.service';
      if (file_exists($file) && !unlink($file)) {
        $this->set_error("Failed to delete file: $file", 'system', 'soft');
        return FALSE;
      }
      if ($this->get_cron_schedule() != $this::$FOREVER) {
        $file = $this->get_service_dir() . $this->get_service_name() . '.timer';
        if (file_exists($file) && !unlink($file)) {
          $this->set_error("Failed to delete file: $file", 'system', 'soft');
          return FALSE;
        } 
      }
      return TRUE;
    }

    function get_service_dir() {
      return $this->get_home_dir() . '/.config/systemd/user/';
    }

    function generate_files() {
      $service_content = $this->get_service_content();
      $service_dir = $this->get_service_dir();
      if(!red_create_directory_recursively($service_dir)) {
        $this->set_error("Failed to create systemd user directory.", 'system', 'soft');
        return FALSE;
      }
      $file = $service_dir . $this->get_service_name() . '.service';
      if (!file_put_contents($file, $service_content)) {
        $this->set_error("Failed to save file: $file", 'system', 'soft');
        return FALSE;
      }
      if ($this->get_cron_schedule() != $this::$FOREVER) {
        $timer_content = $this->get_timer_content();
        $file = $service_dir . $this->get_service_name() . '.timer';
        if (!file_put_contents($file, $timer_content)) {
          $this->set_error("Failed to save file: $file", 'system', 'soft');
          return FALSE;
        } 
      }
      return TRUE;
    }

    function get_timer_content() {
      $schedule = $this->get_cron_schedule();
      $timer_content = '';
      $timer_content .= "[Unit]\n";
      $timer_content .= "Description=Timer Job with item ID: " . $this->get_item_id() . "\n\n";
      $timer_content .= "[Timer]\n";
      $timer_content .= "OnCalendar=$schedule\n";

      if ($schedule == 'minutely') {
        $timer_content .= "RandomizedDelaySec=10\n";
      }
      elseif ($schedule == 'hourly') {
        $timer_content .= "RandomizedDelaySec=600\n";
      }
      elseif ($schedule == 'daily') {
        $timer_content .= "RandomizedDelaySec=3600\n";
      }
      $timer_content .= "[Install]\n";
      $timer_content .= "WantedBy=default.target\n";
      return $timer_content;
    }


    function get_service_content() {
      $user = $this->get_user();
      if (!$user) {
        return FALSE;
      }
      $service_content = '';
      $service_content .= "[Unit]\n";
      $service_content .= "Description=Job with item ID: " . $this->get_item_id() . "\n\n";
      $service_content .= "[Service]\n";
      $service_content .= 'ExecStart=' . $this->get_cron_cmd() . "\n"; 
      $workingDirectory = $this->get_cron_working_directory();
      if (!$workingDirectory) {
        // Default to the web directory.
        $workingDirectory = $this->get_home_dir() . '/web';
      }
      $service_content .= "WorkingDirectory=$workingDirectory\n";
      $environment = $this->get_cron_environment();
      # Prevent users from filling /tmp directory.
      $tmp = $this->get_home_dir() . '/.tmp';
      $default_environment="TEMP=$tmp TMP=$tmp TEMPDIR=$tmp TMPDIR=$tmp";
      $service_content .= "Environment={$default_environment} $environment\n";
     
      if ($this->get_cron_schedule() == $this::$FOREVER) {
        // This is the default, specify for clarity.
        $service_content .= "Type=simple\n";
        $service_content .= "Restart=always\n\n";
        $service_content .= "[Install]\n";
        $service_content .= "WantedBy=multi-user.target\n";
      }
      else {
        $service_content .= "Type=oneshot\n";
      }
      
      return $service_content;
    }
  }  
}


?>
