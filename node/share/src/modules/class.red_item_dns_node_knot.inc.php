<?php
if(!class_exists('red_item_dns_node_knot')) {
  class red_item_dns_node_knot extends red_item_dns {
    var $knot_zone_dir = '/var/lib/knot';
    var $knot_etc_dir = '/etc/knot';
    var $red_lib_dir = '/var/lib/red';
    var $sqlite_path = '/var/lib/red/alias-lookup-cache.db';
    var $pdo;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
    }

    // Ensure our conf and zone file directories exist.
    function node_sanity_check() {
      if (!$this->sqlite_init()) {
        return FALSE;
      }

      if (!red_create_directory_recursively($this->red_lib_dir))  {
        $this->set_error("Failed to created red_lib_dir", 'system');
        return FALSE;
      }
      if(!is_dir($this->knot_zone_dir))  {
        $message = 'Knot root directory does not exist. Trying: ' . $this->knot_zone_dir;
        $this->set_error($message,'system');
        return FALSE;
      }
      if(!is_dir($this->knot_etc_dir))  {
        $message = 'Knot root directory does not exist. Trying: ' . $this->knot_zone_dir;
        $this->set_error($message,'system');
        return FALSE;
      }
      return TRUE;
    }

    /**
     * Standard red operations.
     */
    function delete() {
      if ($this->get_dns_type() == 'dkim') {
        if (!$this->delete_dkim_keys()) return FALSE;
      }
      elseif ($this->get_dns_type() == 'alias') {
        if (!$this->sqlite_unlink_fqdn_from_target()) {
          return FALSE;
        }
      }

      if (!$this->update_zone()) return FALSE;

      // If there are no more records for this zone, ensure all
      // zones files and conf are removed.
      if (!$this->zone_has_records()) {
        if (!$this->delete_zone()) return FALSE;
      }
      if (!$this->reload_knot()) return FALSE;
      if ($this->zone_has_records()) {
        // If zone has no records it will fail the check.
        if (!$this->check_zone()) return FALSE;
      }
      if (!$this->mark_zone_dirty()) return FALSE;
      return TRUE;
    }

    function disable() {
      return $this->delete();  
    }

    function insert() {
      if ($this->get_dns_type() == 'alias') {
        if (!$this->sqlite_link_fqdn_to_target()) {
          return FALSE;
        }
      }
      if (!$this->create_zone_conf()) return FALSE;
      if (!$this->update_zone()) return FALSE;
      if (!$this->reload_knot()) return FALSE;
      if (!$this->check_zone()) return FALSE;
      if (!$this->mark_zone_dirty()) return FALSE;
      return TRUE;
    }

    function update() {
      return $this->insert();
    }

    function restore() {
      return $this->insert();
    }

    /**
     * Indicate that the zone has been changed and needs to be
     * transferred to our auth servers..
     */
    function mark_zone_dirty() {
      if (!touch($this->red_lib_dir . '/zone-changed.txt')) {
        $this->set_error("Failed to touch zone-changed file.", 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    /**
     * check zone validity
     */
    private function check_zone() {
      if (!$this->knotc('zone-check')) {
        $zone = $this->get_dns_zone();
        $this->set_error("Failed to validate your zone. Please re-check your DNS settings and try again or contact support (run knotc zone-check $zone).", 'system', 'soft');
        return FALSE;
      }
      return TRUE;

    }
    private function knotc($subcmd, $extra_args = []) {
      $cmd = '/usr/sbin/knotc';
      $initial_args = [ $subcmd ];
      if ($subcmd != 'reload') {
        $initial_args[] = $this->get_dns_zone();
      }
      $args = array_merge($initial_args, $extra_args);
      if (red_fork_exec_wait($cmd, $args) != 0) {
        return FALSE;
      }
      return TRUE;
    }
    /**
     * reload knot
     */
    function reload_knot() {
      $cmd = '/usr/sbin/knotc';
      if (!$this->knotc('reload')) {
        $this->set_error("Failed to reload knotc. Please re-check your DNS settings and try again or contact support.", 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }
    /**
     * Delete zone file and config. 
     */
    private function delete_zone() {
       $zone = strtolower($this->get_dns_zone());
       // Ensure /etc/knot/knot.conf.d/knot.zones.conf.d/$zone is gone. 
      $dests = [ 
        $this->get_zone_conf_file_path(),
        $this->get_zone_file_path(),
      ];
      foreach ($dests as $dest) {
        if (file_exists($dest)) {
          if (!unlink($dest)) {
            $this->set_error("Failed to delete $dest. Please contact support.", 'system', 'soft');
            return FALSE;
          }
        }
      }
      return TRUE;
    }
    
    function get_zone_file_path() {
      return $this->knot_zone_dir . '/' . $this->get_dns_zone() . '.zone';
    }


    function get_zone_conf_file_path() {
      return $this->knot_etc_dir . '/' . 'knot.conf.d/knot.zones.conf.d/' .
        strtolower($this->get_dns_zone()) . '.conf'; 
    }

    private function create_zone_conf() {
      $zone = strtolower($this->get_dns_zone());
      // Ensure /etc/knot/knot.conf.d/knot.zones.conf.d/$zone exists.
      $dest = $this->get_zone_conf_file_path();
      if (!file_exists($dest)) {
        $content = "zone:\n  - domain: {$zone}.\n";
        if (!file_put_contents($dest, $content)) {
          $this->set_error("Failed to move zone config file into place.",'system', 'soft');
          return FALSE;
        }
        if (!$this->knotc('reload')) {
          $this->set_error("Failed to reload knot after config file creation. Please conact support.",'system', 'soft');
          return FALSE;
        }
        // If config file does not exist, it means no SOA or NS records, so add those here.
        if (!$this->update_soa()) {
          return FALSE;
        }
        if (!$this->update_ns()) {
          return FALSE;
        }
      }
      return TRUE;
    }

    private function update_soa() {
      $name_servers = $this->get_nameservers();
      $ttl = 86400;
      // Get the first one as the authority.
      $ns = array_shift($name_servers) . '.';
      $email = 'hostmaster.' . $this->get_dns_zone() . '.';
      // For serial, uses a timestamp in ISO format for readability.
      // Include microtime so we can have multiple updates in less than one
      // second.
      $serial = time();
      // Time between refresh from the secondary server (not really used
      // since the primary server notifies the secondary when there is an
      // update).
      $refresh = '2h';
      // How often for the secondary to retry if first attempt fails.
      $retry = '3m';
      // How long secondary values are valid if we can't reach the primary
      $expire = '4w';
      // How long the secondary should cache a "no response for this domain"
      $negative_response_ttl = '1h';

      $records = [ "@ {$ttl} SOA {$ns} {$email} {$serial} {$refresh} {$retry} {$expire} {$negative_response_ttl}" ];
      return $this->update_knot_type('@', 'SOA', $records);
    }

    private function update_ns() {
      $nameservers = $this->get_nameservers();
      $records = [];
      $fqdn = $this->append_dot($this->get_dns_zone());
      foreach ($nameservers as $nameserver) {
        $nameserver = $this->append_dot($nameserver);
        $records[] = "$fqdn 86400 NS {$nameserver}";
      }
      return $this->update_knot_type($fqdn, 'NS', $records);
    }

    private function update_zone() {
      $type = $this->get_dns_type();
      switch ($type) {
        case 'ptr':
          $ip = $this->get_dns_ip();
          if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
            return $this->update_ipv6_ptr();
          }
          else {
            return $this->update_ptr();
          }
        case 'mx':
          return $this->update_mx();
        case 'a':
          return $this->update_a();
        case 'alias':
          return $this->update_alias();
        case 'aaaa':
          return $this->update_aaaa();
        case 'dkim':
          return $this->update_dkim();
        case 'txt':
          return $this->update_txt();
        case 'cname':
          return $this->update_cname();
        case 'srv':
          return $this->update_srv();
        case 'sshfp':
          return $this->update_sshfp();

      }      
      $this->set_error("Failed to process unkown type: $type. Please contact support.", 'system', 'soft');
      return FALSE;
    }

    private function update_knot_type($key, $type, $new) {
      $args = [
        '/usr/sbin/knotc',
        'zone-read',
        escapeshellarg($this->get_dns_zone()),
        escapeshellarg($key),
        escapeshellarg($type),
      ];
      $cmd = implode(' ', $args);
      $current = [];
      $return_code;
      exec($cmd, $current, $return_code);
      // knotc returns 1 if there is no data.
      if ($return_code == 1) {
        // Overwrite the output with an empty array
        $current = [];
      }
      elseif ($return_code != 0) {
        $this->set_error("Failed to read zone file ($cmd). Please contact support.", 'system', 'soft');
        return FALSE;
      }
      $needs_update = FALSE;
      foreach ($current as $current_line) {
        if (!in_array($current_line, $new)) {
          $needs_update = TRUE;
          break;
        }
      }
      foreach($new as $new_line) {
        if (!in_array($new_line, $current)) {
          $needs_update = TRUE;
          break;
        }
      }
      if ($needs_update) {
        if (!$this->knotc('zone-begin')) {
          $this->set_error("Failed to run zone-begin. Please contact support.", 'system', 'soft');
          return FALSE;
        }
        $unset_args = [$key, $type];
        if (count($current) > 0) {
          if (!$this->knotc('zone-unset', $unset_args)) {
            $this->set_error("Failed to unset zone. Please contact support.", 'system', 'soft');
            $this->knotc('zone-abort');
            return FALSE;
          }
        }
        foreach ($new as $line) {
          $set_args = [];
          $line_parts = explode(' ', $line);
          // The key is the first argument.
          $set_args[] = array_shift($line_parts);
          // The ttl is the second argument.
          $set_args[] = array_shift($line_parts);
          // The type is the third argument
          $set_args[] = array_shift($line_parts);
          // The rest is the last argument.
          $set_args[] = implode(' ', $line_parts);
          if (!$this->knotc('zone-set', $set_args)) {
            $line = htmlentities($line);
            $this->set_error("Failed to set '$line'. Please contact support.", 'system', 'soft');
            $this->knotc('zone-abort');
            return FALSE;
          }
        }
        if (!$this->knotc('zone-commit')) {
          $this->set_error("Failed to commit zone. Please contact support.", 'system', 'soft');
          $this->knotc('zone-abort');
          return FALSE;
        }
      }
      return TRUE;
    }

    /**
     * Get control panel records
     *
     * Return all control panel records matching the key field
     * and dns type. 
     */
    private function get_control_panel_records($fields) {
      $params = [];
      $escaped_fiels = [];
      foreach ($fields as $k => $field) {
        $token = '!field' . $k;
        $params[$token] = $field;
        $escaped_fields[] = $token;
      } 
      $fields_list = implode(',', $escaped_fields);

      $sql = "SELECT $fields_list 
        FROM 
          red_item JOIN red_item_dns USING (item_id)
        WHERE 
          item_status IN (
              'active', 
              'pending-update', 
              'pending-restore', 
              'pending-insert'
            ) AND
          dns_zone = @dns_zone AND
          dns_fqdn = @dns_fqdn AND
          dns_type = @dns_type AND
      ";
      $params['@dns_zone'] = $this->get_dns_zone();
      $params['@dns_fqdn'] = $this->get_dns_fqdn();
      $params['@dns_type'] = $this->get_dns_type();
      if ($this->get_dns_type() == 'ptr') {
        $sql .= " dns_ip = @dns_ip";
        $params["@dns_ip"] = $this->get_dns_ip();
      }
      else {
        $sql .= " dns_fqdn = @dns_fqdn";
        $params['@dns_fqdn'] = $this->get_dns_fqdn();
      }
      if ($this->get_dns_type() == 'dkim') {
        $sql .= " AND dns_dkim_selector = @dns_dkim_selector";
        $params['@dns_dkim_selector'] = $this->get_dns_dkim_selector();
      }
      $result = red_sql_query($sql, $params);
      $ret = [];
      while ($row = red_sql_fetch_assoc($result)) {
        $ret[] = $row;
      }
      return $ret;
    }

    function update_ptr() {
      // We only want last part of IP.
      $ip = $this->get_dns_ip();
      $ip_parts = explode('.', $ip);
      $ip_last_part = array_pop($ip_parts);
      $fields = [ 'dns_ip', 'dns_fqdn', 'dns_ttl' ];
      $records = $this->get_control_panel_records($fields);
      $lines = [];
      foreach ($records as $key => $record) {
        $ttl = $record['dns_ttl'];
        $fqdn = $this->append_dot($record['dns_fqdn']);
        $lines[] = "{$ip_last_part} {$ttl} PTR {$fqdn}";
      }
      return $this->update_knot_type($ip_last_part, 'PTR', $lines);

    }

    function update_ipv6_ptr() {
      $ip = $this->get_dns_ip();
      $unpacked = unpack("H*hex", inet_pton($ip));
      // Chop off the first 64 bits - that's the zone.
      $significant_part = substr($unpacked['hex'], 16);
      // Reverse it (zone files want it backwards, what can I say?).
      $reversed = strrev($significant_part);
      // Now separate with periods.
      $ip_last_part = substr(preg_replace("/([A-f0-9]{1})/", "$1.", $reversed), 0, -1); 

      $fields = [ 'dns_ip', 'dns_fqdn', 'dns_ttl' ];
      $key_field = 'dns_ip';
      $key_value = $ip;
      $records = $this->get_control_panel_records($fields);
      $lines = [];
      foreach ($records as $key => $record) {
        $ttl = $record['dns_ttl'];
        $fqdn = $this->append_dot($record['dns_fqdn']);
        $lines[] = "{$ip_last_part} {$ttl} PTR {$fqdn}";
      }
      return $this->update_knot_type($ip_last_part, 'PTR', $lines);
    }

    function update_alias() {
      $fields = [ 'dns_server_name'];
      $records = $this->get_control_panel_records($fields);
      $fqdn = $this->append_dot($this->get_dns_fqdn());
      $ttl = $this->get_dns_ttl();
      $ips = [];
      // We should only have one or zero records.
      if (count($records) > 0) {
        $record = array_pop($records);
        $server_name = $record['dns_server_name'];
        $ips = $this->sqlite_get_target_ips($server_name);
        if (!$ips && !$this->get_item_status() != 'pending-delete' && $this->get_item_status() != 'pending-disable')  {
          $this->set_error("Failed to lookup IP address for $server_name. Please try again or remove that alias record.", 'system', 'soft');
          return FALSE;
        }
      }
      // Now generate our lines
      $lines = [];
      foreach($ips as $ip) {
        $lines[] = "{$fqdn} {$ttl} A {$ip}";
      }
      return $this->update_knot_type($fqdn, 'A', $lines);
    }

    function update_a() {
      $fields = [ 'dns_ip'];
      $records = $this->get_control_panel_records($fields);
      $fqdn = $this->append_dot($this->get_dns_fqdn());
      $ttl = $this->get_dns_ttl();
      $lines = [];
      foreach ($records as $record) {
        $ip = $record['dns_ip'];
        $lines[] = "{$fqdn} {$ttl} A {$ip}";
      }
      return $this->update_knot_type($fqdn, 'A', $lines);
    }

    function update_aaaa() {
      $fields = [ 'dns_ip', 'dns_ttl' ];
      $records = $this->get_control_panel_records($fields);
      $fqdn = $this->append_dot($this->get_dns_fqdn());
      $lines = [];
      foreach ($records as $record) {
        $ttl = $record['dns_ttl'];
        $ip = $record['dns_ip'];
        $lines[] = "{$fqdn} {$ttl} AAAA {$ip}";
      }
      return $this->update_knot_type($fqdn, 'AAAA', $lines);
    }

    function update_mx() {
      $fields = [ 'dns_server_name', 'dns_ttl', 'dns_dist' ];
      $records = $this->get_control_panel_records($fields);
      $fqdn = $this->append_dot($this->get_dns_fqdn());
      $lines = [];
      foreach ($records as $record) {
        $ttl = $record['dns_ttl'];
        $dist = $record['dns_dist'];
        $server_name = $this->append_dot($record['dns_server_name']);
        $lines[] = "{$fqdn} {$ttl} MX {$dist} {$server_name}";
      }
      return $this->update_knot_type($fqdn, 'MX', $lines);
    }


    function update_cname() {
      $fields = [ 'dns_server_name', 'dns_ttl' ];
      $records = $this->get_control_panel_records($fields);
      $fqdn = $this->append_dot($this->get_dns_fqdn());
      $lines = [];
      foreach ($records as $record) {
        $ttl = $record['dns_ttl'];
        $server_name = $this->append_dot($record['dns_server_name']);
        $lines[] = "{$fqdn} {$ttl} CNAME {$server_name}";
      }
      return $this->update_knot_type($fqdn, 'CNAME', $lines);
    }

    function update_srv() {
      $fields = [ 'dns_server_name', 'dns_fqdn', 'dns_ttl', 'dns_port', 'dns_dist', 'dns_weight' ];
      $records = $this->get_control_panel_records($fields);
      $fqdn = $this->append_dot($this->get_dns_fqdn());
      $lines = [];
      foreach ($records as $record) {
        $ttl = $record['dns_ttl'];
        $server_name = $this->append_dot($record['dns_server_name']);
        $port = $record['dns_port'];
        $dist = $record['dns_dist'];
        $weight = $record['dns_weight'];
        $lines[] = "{$fqdn} {$ttl} SRV {$dist} {$weight} {$port} {$server_name}";
      }
      return $this->update_knot_type($fqdn, 'SRV', $lines);
    }

    function update_sshfp() {
      $fields = [ 'dns_sshfp_algorithm', 'dns_sshfp_type', 'dns_sshfp_fpr', 'dns_ttl' ];
      $records = $this->get_control_panel_records($fields);
      $fqdn = $this->append_dot($this->get_dns_fqdn());
      $lines = [];
      foreach ($records as $record) {
        $ttl = $record['dns_ttl'];
        $sshfp_algorithm = $record['dns_sshfp_algorithm'];
        $sshfp_type = $record['dns_sshfp_type'];
        $sshfp_fpr = $record['dns_sshfp_fpr'];
        $lines[] = "{$fqdn} {$ttl} SSHFP {$sshfp_algorithm} {$sshfp_type} {$sshfp_fpr}";
      }
      return $this->update_knot_type($fqdn, 'SSHFP', $lines);
    }

    function update_txt() {
      $fields = [ 'dns_text', 'dns_ttl' ];
      $records = $this->get_control_panel_records($fields);
      $fqdn = $this->append_dot($this->get_dns_fqdn());
      $lines = [];
      foreach ($records as $record) {
        $ttl = $record['dns_ttl'];
        $text = $record['dns_text'];
        $lines[] = "{$fqdn} {$ttl} TXT \"{$text}\"";
      }
      return $this->update_knot_type($fqdn, 'TXT', $lines);

    }
    function update_dkim() {
      $fields = [ 'dns_dkim_signing_domain', 'dns_ttl' ];
      $records = $this->get_control_panel_records($fields);
      $lines = [];
      $fqdn = $this->get_dns_fqdn();
      $selector = $this->get_dns_dkim_selector();
      $final_fqdn = $this->append_dot($selector . '._domainkey.' . $fqdn);
      foreach ($records as $record) {
        $ttl = $record['dns_ttl'];
        $signing_domain = $record['dns_dkim_signing_domain'];
        $text = $this->get_dkim_sig($fqdn, $selector, $signing_domain);
        $lines[] = "{$final_fqdn} {$ttl} TXT \"{$text}\"";
      }
      return $this->update_knot_type($final_fqdn, 'TXT', $lines);
    }

    function get_dkim_sig($fqdn, $dkim_selector, $dkim_signing_domain) {
      // Dkim is way more complicated because we have to call a command and parse the output.
      $domain = $fqdn;
      if (!empty($dkim_signing_domain)) {
        $domain = $dkim_signing_domain;
      }
      $domain = escapeshellarg($domain);
      $selector = escapeshellarg($dkim_selector);
      $cmd = "/usr/local/bin/dkim-generate $domain $selector";
      $output = [];
      $result_code = NULL;
      exec($cmd, $output, $result_code);
      if ($result_code != 0) {
        $this->set_error("Failed to run dkim-generate on $domain and $selector.", 'system', 'soft');
        return FALSE;
      }
      // Now we have to parse the output. We get something like (note multi line):
      //
      // pb1._domainkey  IN  TXT  ( "v=DKIM1; h=rsa-sha256; k=rsa; s=email; "
      // "p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2jMXn/y2bIAe2xZOeDCts2UNz7YJvh5xy6+0aZMKn/Rt1swIB1UCWt60//VR6Ra4UUW7FcBpW3BX+qdQ0+kdZ6w9ekHYfhAk6pUZlBGs6+GPEFtBXcVLMiafHU2A0sK/Gvwzt9myPg+yF2mq055B/4hsaEdOD7pZ2dwUHy7OTjKCamaB2lwQA+HNDY1frgPQUBzGJZfc3JVy4i"
     // "Y2cpF6Hw5TNG0ohCfY3Paf2YzBtGE+RcpsPwfgGFiRDO1NmTowxkCPZMY3Gi8OlFlp2lPdrg4UgD+6jc9o71DnBPbnV7u/U56WRicMHMNCZqVDkQbcSe8Yi9khJy3W4Xsade3SzQIDAQAB" )  ; ----- DKIM key pb1 for myorg.mayfirst.dev
      // We just want the part in quotes, and rsa-sha256 must be converted to just sha256.
      $dkim_sig = "";
      foreach($output as $line) {
        if (preg_match('/"(.*)"/', $line, $matches)) {
          if (substr($dkim_sig, -1, 1) == ';') {
            // If this is the line ending with s=email; then we want a space. Not required, but makes
            // it look better.
            $dkim_sig .= ' ';
          }
          $dkim_sig .= trim($matches[1]);
        }
        $dkim_sig = str_replace('h=rsa-sha256', 'h=sha256', $dkim_sig);
      }
      return $dkim_sig;
    }

    function delete_dkim_keys() {
      // When deleting a dkim record, be sure to delete the
      // corresponding keys.
      $domain = $this->get_dns_fqdn();
      $signing_domain = $this->get_dns_dkim_signing_domain();
      if (!empty($signing_domain)) {
        $domain = $signing_domain;
      }
      $selector = $this->get_dns_dkim_selector();
      $dir = "/etc/opendkim/keys/{$domain}";
      $private = "{$dir}/{$selector}.private";
      $public = "{$dir}/{$selector}.txt";
      foreach ([$public, $private] as $delete_file) {
        if (file_exists($delete_file)) {
          if (!unlink($delete_file)) {
            $this->set_error("Failed to delete {$delete_file}.", 'system', 'soft');
            return FALSE;
          }
        }
      }
      if (is_dir($dir)) {
        if (!rmdir($dir)) {
          $this->set_error("Failed to delete {$dir}.", 'system', 'soft');
          return FALSE;
        }
      }

      // Now delete from the KeyTable
      $file = '/etc/opendkim/KeyTable';
      $delimiter = ' ';
      if (!red_delete_from_file($file, $domain, $delimiter)) {
        $this->set_error("Failed to remove key from KeyTable.", 'system', 'soft');
        return FALSE;
      }
      $file = '/etc/opendkim/SigningTable';
      if (!red_delete_from_file($file, $domain, $delimiter)) {
        $this->set_error("Failed to remove key from SigningTable.", 'system', 'soft');
        return FALSE;
      }
      // Now sync the directory to remove the files from
      // the relay servers.
      $cmd = "/usr/local/bin/dkim-rsync";
      $output = [];
      $result_code = NULL;
      exec($cmd, $output, $result_code);
      if ($result_code != 0) {
        $this->set_error("Failed to run dkim-rsync on $domain and $selector.", 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    function append_dot($value) {
      // Ensure server_name end in a dot so we don't append $ORIGIN.
      if(!preg_match('/\.$/', $value)) {
        $value .= '.';
      }
      return $value;
    }

    /*
     * For a given zone, determine if all records have been deleted.
     */
    private function zone_has_records() {
      $sql = "SELECT COUNT(*) 
        FROM red_item_dns JOIN red_item USING(item_id) 
        WHERE 
          item_status != 'pending-delete' AND
          item_status != 'deleted' AND 
          item_status != 'transfer-limbo'
          AND item_status != 'disabled' AND 
          item_status != 'pending-disable' AND 
          dns_zone = @zone";
      $result = red_sql_query($sql, ['@zone' => $this->get_dns_zone()]);
      $row = red_sql_fetch_row($result);
      if ($row[0] > 0) {
        return TRUE;
      }
      return FALSE;
    }
    function sqlite_unlink_fqdn_from_target() {
      $target = $this->get_dns_server_name();
      try {
        $sql = "DELETE FROM target_fqdn WHERE fqdn = :fqdn AND target = :target";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([
          ':fqdn' => $this->get_dns_fqdn(),
          ':target' => $this->get_dns_server_name(),
        ]);
        // If this is the last target, we should also clear the target_ip table.
        $sql = "SELECT COUNT(*) AS count FROM target_fqdn WHERE target = :target";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([':target' => $this->get_dns_server_name()]);
        $row = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($row['count'] == 0) {
          $sql = "DELETE FROM target_ip WHERE target = :target";
          $stmt = $this->pdo->prepare($sql);
          $stmt->execute([
            ':target' => $this->get_dns_server_name(),
          ]);
        }

      }
      catch (\PDOException $e) {
        $this->set_error("Failed to unlink fqdn to target. Please contact support. Error: " . $e->getMessage(), 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    function sqlite_link_fqdn_to_target() {
      $target = $this->get_dns_server_name();
      try {
        $sql = "SELECT target FROM target_fqdn WHERE fqdn = :fqdn";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([':fqdn' => $this->get_dns_fqdn()]);
        $exists = FALSE;
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
          if ($row['target'] == $target) {
            $exists = TRUE;
          }
        }
        if (!$exists) {
          $sql = "INSERT INTO target_fqdn VALUES(:target, :zone, :ttl, :fqdn)";
          $stmt = $this->pdo->prepare($sql);
          $stmt->execute([
            ':fqdn' => $this->get_dns_fqdn(),
            ':zone' => $this->get_dns_zone(),
            ':ttl' => $this->get_dns_ttl(),
            ':target' => $target
          ]);
        }
      }
      catch (\PDOException $e) {
        $this->set_error("Failed to link fqdn to target. Please contact support. Error: " . $e->getMessage(), 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    function sqlite_get_target_ips($target) {
      $target = $target;
      $sql = "SELECT ip FROM target_ip WHERE target = :target";
      try {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([':target' => $target]);
        $ips = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
          $ips[] = $row['ip'];
        }
      }
      catch (\PDOException $e) {
        $this->set_error("Failed to query sqlite table. Please contact support. Error: " . $e->getMessage(), 'system', 'soft');
        return FALSE;
      }
      if (!$ips) {
        $ips = $this->dns_lookup($target);
        if ($ips) {
          if (!$this->sqlite_insert_alias_ips($target, $ips)) {
            return FALSE;
          }
        }
      }
      return $ips;
    }

    function sqlite_insert_alias_ips($target, $ips) {
      try {
        $sql = "INSERT INTO target_ip VALUES(:target, :ip)";
        $stmt = $this->pdo->prepare($sql);
        foreach($ips as $ip) {
          $stmt->execute([
            ':target' => $target,
            ':ip' => $ip
          ]);
        }
      }
      catch (\PDOException $e) {
        $this->set_error("Failed to insert IPs. Please contact support. Error: " . $e->getMessage(), 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }

    function dns_lookup($domain) {
      $ips = [];
      $results = dns_get_record($domain, DNS_A);
      if (!$results) {
        red_log("Failed to lookup A record for $domain.");
      }
      else {
        foreach($results as $result) {
          $ip = $result['ip'] ?? NULL;
          if ($ip) {
            $ips[] = $result['ip'];
          }
        }
      }
      return $ips;
    }

    /**
     * We use a sqlite database to cache alias dns lookups.
     * 
     * target_fqdn:
     *  - target 
     *  - fqdn
     *
     * target_ip:
     *  - target 
     *  -ip
     */
    function sqlite_init() {
      $exists = FALSE;
      if (file_exists($this->sqlite_path)) {
        $exists = TRUE;
      }
      $this->pdo = new \PDO("sqlite:" . $this->sqlite_path);
      if (!$this->pdo) {
        $this->set_error("Failed to initialize sqlite database. Please contact support.", 'system', 'soft');
        return FALSE;
      }
      // We want to throw an exception on errors.
      $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      // If we already exists, don't initiliaze the database tables.
      if ($exists) {
        return TRUE;
      }

      $sqls[] = "CREATE TABLE IF NOT EXISTS 
        target_fqdn (
          target TEXT NOT NULL,
          zone TEXT NOT NULL,
          ttl INT NOT NULL,
          fqdn TEXT NOT NULL
        )
      ";
      $sqls[] = "CREATE INDEX target_fqdn_target_idx ON target_fqdn (target)";
      $sqls[] = "CREATE INDEX target_fqdn_fqdn_idx ON target_fqdn (fqdn)";
      $sqls[] = "CREATE TABLE IF NOT EXISTS 
        target_ip (
          target TEXT NOT NULL,
          ip TEXT NOT NULL
        )
      ";
      $sqls[] = "CREATE INDEX target_ip_target_idx ON target_fqdn (target)";
      try {
        foreach ($sqls as $sql) {
          $this->pdo->exec($sql);
        }
      }
      catch (\PDOException $e) {
        $this->set_error("Failed to create sqlite table. Please contact support. Error: " . $e->getMessage(), 'system', 'soft');
        return FALSE;
      }
      return TRUE;
    }
  }  
}

?>
