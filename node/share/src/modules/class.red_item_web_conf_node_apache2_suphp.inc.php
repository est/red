<?php
require_once('class.red_item_web_conf_node_apache2.inc.php');
if(!class_exists('red_item_web_conf_node_apache2_suphp')) {
  class red_item_web_conf_node_apache2_suphp extends red_item_web_conf_node_apache2 {
    function get_apache_conf_file_children_additions() {
      return "\tsuPHP_ConfigPath " . $this->get_php_ini_dir() . "\n";
    }

    function get_web_related_directory_names() {
      $core = parent::get_web_related_directory_names();
      // add include/php5
      $core[] = 'include/php5';
      return $core;
    }

    function create_web_directories() {
      if(!parent::create_web_directories()) return false;

      // add default php.ini file
      if(!$this->create_php_ini()) return false;

      return true;
    }
  }
}
