<?php
// This file is included by all mediawiki web applications by default.
// If you don't like what you see here, don't try to edit this file.
// Instead, remove the include line from your own LocalSettings.php file.
