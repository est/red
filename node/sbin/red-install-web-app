#!/bin/bash

# Install the web app specified.

function fail() {
  msg="$1"
  exit="$2"
  printf "$msg\n"
  exit "$exit"
}

[[ $(whoami) = "root" ]] && fail "Please run as the user who owns the web directory." 10

webapp="$1"
web_path="$2"

[ "$webapp" != "drupal9" -a "$webapp" != "wordpress" -a "$webapp" != "backdrop" ] && \
  fail "Please specify drupal9, wordpress or backdrop as the first argument." 10

[ ! -d "$web_path" ] && fail "The web_path specified ($web_path) doesn't exist." 11 

# There should only be two files (at most) - .user.ini and index.html.
count=$(ls -A "$web_path" | wc -l)
[ "$count" -gt 3 ] && fail \
  "Please delete the contents of the web directory before installing a web app. I found $count files. Should not be more then 2." 1

ready=y
for file in $(ls "$web_path"); do
  if [ "$file" != ".user.ini" -a "$file" != "index.html" -a "$file" != ".well-known" ]; then
    printf "I found %s in the web directory. Please delete it and try again.\n" "$file"
    ready=n
  fi
done

if [ "$ready" != "y" ]; then
  exit 1
fi

# Make sure we can write to the specified directory.
[ ! -w "$web_path" ] && fail "I can't seem to write to the specified directory." 13

# The URL to download the most recent version of the web app.
url=

# The path, relative to the web root, containing the sensitive db
# configuration settings. It will have the permissions updated at
# the end of the install process.
if [ "$webapp" = "drupal9" ]; then
  url="https://www.drupal.org/download-latest/zip"
elif [ "$webapp" = "wordpress" ]; then
  url="https://wordpress.org/latest.zip"
elif [ "$webapp" = "backdrop" ]; then
  # As far as I can tell, there is no "latest" URL I can use. Boo. However, this URL redirects to
  # the right tagged release, and the tag is in the URL we get redirected to.
  tag=$(curl -q https://github.com/backdrop/backdrop/releases 2>/dev/null| grep -E -o "tag/[0-9.]+" | sed "s#tag/##" | head -n1)
  if [ -z "$tag" ]; then
    printf "Failed to fetch the backdrop tag.\n"
    exit 14
  fi
  url="https://github.com/backdrop/backdrop/releases/download/$tag/backdrop.zip"
fi

[ -z "$url" ] && fail "Failed to discover the correct download URL." 15

# Download the app to the temp file.
file=$(mktemp --suffix .zip)
wget --quiet "$url" -O "$file"

if [ "$?" != 0 ]; then
  rm "$file"
  fail "Something went wrong downloading the url: $url."  16
fi

# Unzip the cms to a temporary directory.
dir=$(mktemp -d)
unzip -qq -d "$dir" "$file" 
if [ "$?" != 0 ]; then
  rm "$file"
  rm -rf "$dir"
  fail "Something went wrong unzipping the downloaded file." 17
fi

# Clean up the zip file
rm "$file"

# Unzipping the file always results in a directory (e.g. wordpress, backdrop or drupal-9.2.1)
# Since the name will vary (thanks drupal) we just pick off the one expected directory.
cms_dir=$(ls "$dir")
cms_path="${dir}/$cms_dir"

if [ ! -d "$cms_path" ]; then
  rm -rf "$dir"
  fail "I can't locate the cms directory. Tried $cms_path." 18
fi

# We don't have permission to change the time or permissions of the
# enclosing web directory, so skip times for all directories and 
# skip perms for all dirs and files to avoid an error.
rsync -a --omit-dir-times --no-perms "${cms_path}/" "${web_path}/"

if [ "$?" != "0" ]; then
  rm -rf "$dir"
  fail "Failed to rsync from $cms_path to $web_path." 19
fi

# If an index.html file exists, we have to move it.
index_html_path="${web_path}/index.html"
if [ -f "$index_html_path" ]; then
  suffix=$RANDOM
  mv "$index_html_path" "${index_html_path}.${suffix}" 
fi
exit 0;
