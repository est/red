CREATE TABLE `red_vps` (
  `vps_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `vps_status` enum('active','deleted') DEFAULT 'active',
  `vps_server` varchar(128), 
  `vps_cpu` tinyint unsigned NOT NULL DEFAULT 1,
  `vps_ram` tinyint unsigned NOT NULL DEFAULT 1,
  `vps_hd` bigint(20) unsigned NOT NULL DEFAULT 0,
  `vps_ssd` bigint(20) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`vps_id`),
  KEY (`vps_server`),
  KEY (`vps_status`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_vps_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
);

INSERT INTO red_service SET service_table = 'red_vps', service_area = 'member', service_name = 'VPS', service_description = 'A VPS, or Virtual Private Server, provides an isolated server for your membership to run custom software. Please request a VPS by opening a support ticket.', service_delete_order = 250, service_order_by = 'vps_server', service_status = 'active', service_item = 0;
