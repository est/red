ALTER TABLE red_item_dns CHANGE COLUMN `dns_type` `dns_type` enum('mx','a','text','cname','srv','aaaa','ptr', 'mailstore', 'sshfp') DEFAULT NULL;
ALTER TABLE red_item_dns ADD COLUMN dns_sshfp_algorithm tinyint;
ALTER TABLE red_item_dns ADD COLUMN dns_sshfp_type tinyint;
ALTER TABLE red_item_dns ADD COLUMN dns_sshfp_fpr text;

