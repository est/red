-- add txt type and alias type
ALTER TABLE red_item_dns CHANGE COLUMN `dns_type` `dns_type` enum('mx','a','text','cname','srv','aaaa','ptr','mailstore','sshfp','dkim','txt','alias') DEFAULT NULL;
-- copy text values to txt.
UPDATE red_item_dns SET dns_type = 'txt' WHERE dns_type = 'text';
-- drop text type.
ALTER TABLE red_item_dns CHANGE COLUMN `dns_type` `dns_type` enum('mx','a','cname','srv','aaaa','ptr','mailstore','sshfp','dkim','txt','alias') DEFAULT NULL;
