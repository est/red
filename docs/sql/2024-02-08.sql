CREATE TABLE `red_item_psql` (
  `item_id` mediumint(11) unsigned NOT NULL DEFAULT 0,
  `psql_name` varchar(16) DEFAULT NULL,
  `psql_password` varchar(256) DEFAULT NULL,
  `psql_max_connections` smallint(6) DEFAULT 25,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_psql_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `red_service` VALUES (39,'red_item_psql','hosting_order','PostgreSQL database and user','A PostgreSQL database is an alternative to MySQL.','','psql002.mayfirst.org',150,'psql_name','active',1,30);

