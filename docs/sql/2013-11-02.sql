ALTER TABLE red_server ADD COLUMN closed TINYINT(1) NOT NULL DEFAULT 0;
ALTER TABLE red_hosting_order MODIFY hosting_order_host VARCHAR(128);
ALTER TABLE red_hosting_order ADD INDEX (hosting_order_host);
ALTER TABLE red_server ADD INDEX (server);

