ALTER TABLE red_item_dns ADD COLUMN dns_dkim_selector varchar(32);
ALTER TABLE red_item_dns ADD COLUMN dns_dkim_signing_domain varchar(255);
ALTER TABLE red_item_dns CHANGE COLUMN `dns_type` `dns_type` enum('mx','a','text','cname','srv','aaaa','ptr', 'mailstore', 'sshfp', 'dkim') DEFAULT NULL;

