ALTER TABLE red_item_user_account ADD COLUMN user_account_abandoned_mailbox tinyint(1) DEFAULT 0;
CREATE INDEX user_account_abandoned_mailbox ON red_item_user_account (user_account_abandoned_mailbox);

