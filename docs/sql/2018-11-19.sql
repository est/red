ALTER TABLE red_item_web_conf ADD COLUMN web_conf_php_version decimal(3,1) DEFAULT 0;
UPDATE red_item_web_conf SET web_conf_php_version = '7.0' WHERE item_id in (SELECT item_id FROM red_item WHERE item_status != 'deleted');
UPDATE red_item_web_conf SET web_conf_php_version = '5.6' WHERE item_id in (SELECT item_id FROM red_item WHERE item_status != 'deleted') AND web_conf_settings LIKE '%# mfplphpversion: 5%';
