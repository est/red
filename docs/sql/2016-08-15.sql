ALTER TABLE red_member ADD COLUMN member_benefits_price smallint(6);
ALTER TABLE red_member ADD COLUMN member_benefits_level enum('basic', 'standard', 'extra');
ALTER TABLE red_invoice ADD COLUMN invoice_type enum('membership', 'benefits') DEFAULT NULL;
