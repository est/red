DROP TABLE IF EXISTS `red_item_mysql_db`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `red_item_mysql_db` (
  `item_id` mediumint(11) unsigned NOT NULL default '0',
  `mysql_db_name` varchar(128),
  PRIMARY KEY  (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

INSERT INTO red_service SET service_table = 'red_item_mysql_db',service_area = 'hosting_order', service_name = 'MySQL database', service_description = "MySQL database", service_delete_order = 150, service_order_by = 'mysql_db_name', service_default_host = 'default', service_id = 20, service_status = 'inactive';

DROP TABLE IF EXISTS `red_item_mysql_user`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `red_item_mysql_user` (
  `item_id` mediumint(11) unsigned NOT NULL default '0',
  `mysql_user_name` varchar(16),
  `mysql_user_password` varchar(41),
  `mysql_user_db` text,
  `mysql_user_priv` enum('full','read'),
  PRIMARY KEY  (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

INSERT INTO red_service SET service_table = 'red_item_mysql_user',service_area = 'hosting_order', service_name = 'MySQL database user', service_description = "MySQL database user", service_delete_order = 150, service_order_by = 'mysql_user_name', service_default_host = 'default', service_id = 21, service_status = 'inactive';

DROP TABLE IF EXISTS `red_item_web_app`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `red_item_web_app` (
  `item_id` mediumint(11) unsigned NOT NULL default '0',
  `web_app_name` varchar(128),
  `web_app_version` varchar(16),
  `web_app_dir` varchar(128),
  `web_app_mysql_db_id` mediumint(11) unsigned NOT NULL default '0',
  `web_app_mysql_user_id` mediumint(11) unsigned NOT NULL default '0',
  `web_app_cron_id` mediumint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

INSERT INTO red_service SET service_table = 'red_item_web_app',service_area = 'hosting_order', service_name = 'Web application', service_description = "Web application", service_delete_order = 100, service_order_by = 'web_app_name,web_app_version', service_default_host = 'default', service_id = 23, service_status = 'inactive';

