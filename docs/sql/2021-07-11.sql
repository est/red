UPDATE red_item_web_app SET web_app_name = CONCAT(web_app_name, web_app_version) WHERE web_app_name = 'drupal';
ALTER TABLE red_item_web_app DROP COLUMN web_app_version;
ALTER TABLE red_item_web_app DROP COLUMN web_app_dir;
ALTER TABLE red_item_web_app DROP COLUMN web_app_mysql_db_id;
ALTER TABLE red_item_web_app DROP COLUMN web_app_mysql_user_id;
ALTER TABLE red_item_web_app DROP COLUMN web_app_cron_id;
ALTER TABLE red_item_web_app DROP COLUMN web_app_initial_admin_email;
ALTER TABLE red_item_web_app ADD COLUMN web_app_update varchar(32) DEFAULT 'none';
UPDATE red_service SET service_order_by = 'web_app_name' WHERE service_id = 23;
UPDATE red_item SET item_status = 'deleted' WHERE item_status = 'active' AND item_id IN (SELECT item_id FROM red_item_web_app WHERE web_app_name = 'mediawiki')
