CREATE TABLE `red_member_dns_status` (
  `member_dns_status_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `member_dns_status` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_dns_status_id`),
  KEY `fk_member_id` (`member_id`),
  CONSTRAINT `red_member_dns_status_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `red_member` (`member_id`) ON DELETE CASCADE
);
