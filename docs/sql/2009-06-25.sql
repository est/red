CREATE TABLE `red_map_user_member` (
  `map_user_member_id` mediumint(11) unsigned NOT NULL auto_increment,
	`login` varchar(255) default NULL,
  `member_id` mediumint(11) unsigned NOT NULL default '0',
	`status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY  (`map_user_member_id`)
);

CREATE TABLE `red_map_user_server` (
  `map_user_server_id` mediumint(11) unsigned NOT NULL auto_increment,
  `member_id` mediumint(11) unsigned NOT NULL,
	`login` varchar(255) default NULL,
  `server` varchar(128) NOT NULL default '0',
	`status` enum('active','deleted') DEFAULT 'active',
  PRIMARY KEY  (`map_user_server_id`)
);

CREATE TABLE `red_server` (
  `server_id` mediumint(11) unsigned NOT NULL auto_increment,
	`accepting` tinyint(1) default 0,
  `server` varchar(128) NOT NULL default '0',
  PRIMARY KEY  (`server_id`)
);

ALTER TABLE red_member ADD COLUMN member_parent_id mediumint(11) unsigned DEFAULT NULL;
ALTER TABLE red_member ADD COLUMN member_status enum('active','inactive','suspended') default 'active';
ALTER TABLE red_member ADD COLUMN member_term enum('month','year') default 'year';
ALTER TABLE red_member ADD COLUMN member_price smallint(6) NOT NULL default '0';
ALTER TABLE red_member ADD COLUMN member_type enum('individual','organization') NOT NULL default 'organization';
ALTER TABLE red_member ADD COLUMN member_start_date datetime NOT NULL default '0000-00-00 00:00:00';
ALTER TABLE red_member DROP INDEX member_friendly_name;
ALTER TABLE red_member DROP INDEX member_group_name;
ALTER TABLE red_unique_unix_group ADD COLUMN unique_unix_group_status enum('active','deleted') default 'active';
ALTER TABLE red_unique_unix_group DROP INDEX  unique_group_name;

RENAME TABLE invoice to red_invoice;
RENAME TABLE payment to red_payment;
RENAME TABLE contact to red_contact;
RENAME TABLE password_reset to red_item_pass_reset;
ALTER TABLE red_item_pass_reset CHANGE COLUMN password_reset_id item_id MEDIUMINT(11) UNSIGNED DEFAULT 0;
ALTER TABLE red_item_pass_reset CHANGE COLUMN login pass_reset_login VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE red_item_pass_reset CHANGE COLUMN hash pass_reset_hash VARCHAR(32) NULL DEFAULT NULL;
ALTER TABLE red_item_pass_reset CHANGE COLUMN expires pass_reset_expires timestamp NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE red_item_pass_reset CHANGE COLUMN reset pass_reset timestamp NOT NULL DEFAULT '0000-00-00 00:00:00';


RENAME TABLE correspondence to red_correspondence;
ALTER TABLE red_correspondence ADD COLUMN member_id mediumint(11) unsigned DEFAULT 0 AFTER correspondence_id;
UPDATE red_correspondence SET member_id = 1 WHERE correspondence_body LIKE '%tirelessly%';
UPDATE red_correspondence SET member_id = 1 WHERE correspondence_body LIKE '%undergone a transformation%';
UPDATE red_correspondence SET member_id = 1 WHERE correspondence_body LIKE '%I\'m the Collector%';

ALTER TABLE red_contact ADD COLUMN contact_status enum('active','deleted') DEFAULT 'active';
ALTER TABLE red_invoice ADD COLUMN member_id mediumint(11) unsigned DEFAULT NULL AFTER invoice_id;

ALTER TABLE red_service ADD COLUMN service_area VARCHAR(32) AFTER service_table;
ALTER TABLE red_service ADD COLUMN service_order_by VARCHAR(64);

ALTER TABLE purchase DROP COLUMN staff_id;

UPDATE red_service SET service_area = 'hosting_order';
INSERT INTO red_service SET service_table = 'red_hosting_order', service_name = 'Hosting Order', service_area = 'member', service_description = 'Hosting orders', service_delete_order = 230, service_order_by = 'hosting_order_identifier';
INSERT INTO red_service SET service_table = 'red_map_user_member', service_name = 'Member Access', service_area = 'member', service_description = 'Allow other users to add/remove hosting orders', service_delete_order = 50, service_order_by = 'login';
INSERT INTO red_service SET service_table = 'red_member', service_name = 'Member', service_area = 'top', service_description = 'Members', service_delete_order = 240, service_order_by = 'member_friendly_name';
INSERT INTO red_service SET service_table = 'red_map_user_server', service_name = 'Server Access', service_area = 'top', service_description = 'Allow other users to add/remove members', service_delete_order = 50, service_order_by = 'login';
INSERT INTO red_service SET service_table = 'red_contact', service_name = 'Contacts', service_area = 'member', service_description = 'Contacts', service_delete_order = 240, service_order_by = 'contact_last_name';
INSERT INTO red_service SET service_table = 'red_invoice', service_name = 'Invoices', service_area = 'member', service_description = 'Invoices', service_delete_order = 240, service_order_by = 'invoice_date DESC';
INSERT INTO red_service SET service_table = 'red_correspondence', service_name = 'Correspondence', service_area = 'member', service_description = 'Correspondence', service_delete_order = 240, service_order_by = 'correspondence_date';
INSERT INTO red_service SET service_table = 'red_item_pass_reset', service_name = 'Password reset', service_area = 'hosting_order', service_description = 'Password Reset', service_delete_order = 50, service_order_by = 'pass_reset_expires';
INSERT INTO red_service SET service_table = '', service_name = 'Search', service_area = 'top', service_description = 'Search', service_delete_order = 0, service_order_by = '';

ALTER TABLE red_hosting_order CHANGE COLUMN `hosting_order_status` `hosting_order_status` enum('active','deleted') DEFAULT 'active';
UPDATE red_hosting_order SET hosting_order_status = 'deleted' WHERE hosting_order_status != 'active';


INSERT INTO red_server SET server = 'wiwa.mayfirst.org';
INSERT INTO red_server SET server = 'evo.mayfirst.org';
INSERT INTO red_server SET server = 'menchu.mayfirst.org';
INSERT INTO red_server SET server = 'viewsic.mayfirst.org';
INSERT INTO red_server SET server = 'zapata.mayfirst.org';
INSERT INTO red_server SET server = 'chavez.mayfirst.org';
INSERT INTO red_server SET server = 'malcolm.mayfirst.org';
INSERT INTO red_server SET server = 'mandela.mayfirst.org';
INSERT INTO red_server SET server = 'albizu.mayfirst.org';
INSERT INTO red_server SET server = 'jones.mayfirst.org';
INSERT INTO red_server SET server = 'roe.mayfirst.org';
INSERT INTO red_server SET server = 'june.mayfirst.org';
INSERT INTO red_server SET server = 'lucy.mayfirst.org';
INSERT INTO red_server SET server = 'julia.mayfirst.org', accepting = 1;
INSERT INTO red_server SET server = 'eagle.mayfirst.org';


DROP TABLE red_map_user_hosting_order;

UPDATE red_member SET member_parent_id = member_id;
UPDATE red_member SET member_parent_id = 319 WHERE member_id IN (SELECT DISTINCT(member_id) FROM red_contact WHERE contact_email LIKE '%unionwebservices%');
UPDATE red_member SET member_parent_id = 319 WHERE member_id IN (SELECT DISTINCT(member_id) FROM red_hosting_order WHERE hosting_order_host = 'jones.mayfirst.org');
UPDATE red_member SET member_parent_id = 469 WHERE member_id IN (SELECT DISTINCT(member_id) FROM red_hosting_order WHERE hosting_order_host = 'roe.mayfirst.org');
UPDATE red_member SET member_parent_id = 5 WHERE member_id IN (SELECT DISTINCT(member_id) FROM red_hosting_order WHERE hosting_order_host = 'menchu.mayfirst.org');

INSERT INTO red_map_user_server SET member_id = 5, login = 'downing', server = 'menchu.mayfirst.org';
INSERT INTO red_map_user_server SET member_id = 319, login = 'uws', server = 'jones.mayfirst.org';
INSERT INTO red_map_user_server SET member_id = 319, login = 'uwsclient', server = 'jones.mayfirst.org';
INSERT INTO red_map_user_server SET member_id = 469, login = 'nnaf', server = 'roe.mayfirst.org';
INSERT INTO red_map_user_server SET member_id = 469, login = 'mmurain', server = 'roe.mayfirst.org';

