CREATE TABLE `red_phone` (
  `phone_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` mediumint(11) unsigned NOT NULL DEFAULT '0',
  `phone_country_code` smallint NOT NULL DEFAULT 1,
  `phone_number` bigint NOT NULL,
  `phone_status` enum('active','deleted'),
  `phone_modified` timestamp DEFAULT CURRENT_TIMESTAMP(),
  `phone_type` varchar(32),
  PRIMARY KEY (`phone_id`),
  KEY `member_id` (`member_id`)
);

INSERT INTO red_service SET service_table = 'red_phone', service_area = 'member', service_name = 'Phone', service_description = 'Member Phone Number', service_delete_order = 240, service_order_by = 'phone_type DESC', service_status = 'active';

