CREATE TABLE `red_item_nextcloud` (
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `nextcloud_login` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  CONSTRAINT `red_item_nextcloud_item_id` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
);

INSERT INTO red_service SET service_table = 'red_item_nextcloud', service_area = 'hosting_order', service_name = 'Nextcloud', service_description = 'Nextcloud is the May First cloud storage, calendar and contact syncing service', service_delete_order = 50, service_order_by = 'nextcloud_login', service_status = 'active', service_item = 1;
