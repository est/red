ALTER TABLE red_item_mysql_db DROP COLUMN IF EXISTS mysql_db_host;

UPDATE red_service SET service_help_link = 'https://help.mayfirst.org/en/guide/how-to-create-a-mysql-user-and-database' WHERE service_id IN (20, 21);
UPDATE red_service SET service_help_link = 'https://help.mayfirst.org/en/guide/how-to-create-a-psql-user-and-database' WHERE service_id = 39;
UPDATE red_service SET service_help_link = 'https://help.mayfirst.org/en/guide/how-to-create-and-edit-dns-records-in-control-panel' WHERE service_id = 9;
UPDATE red_service SET service_help_link = 'https://help.mayfirst.org/en/reference/about-nextcloud' WHERE service_id = 37;
UPDATE red_service SET service_help_link = 'https://help.mayfirst.org/en/guide/how-to-create-a-new-email-address' WHERE service_id IN (2, 38);
