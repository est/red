CREATE TABLE `red_file_sync` (
  `file_sync_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_sync_key` varchar(128) UNIQUE,
  `file_sync_hash` varchar(32),
  PRIMARY KEY (`file_sync_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
