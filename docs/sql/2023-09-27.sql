DELETE FROM red_service WHERE service_name IN ('Invoices', 'Planned Payments', 'Phone', 'Payments', 'Postal Address', 'Tags', 'Notes', 'Voting Token');
DROP TABLE red_planned_payment;
DROP TABLE red_phone;
DROP TABLE red_tag;
DROP TABLE red_note;
DROP TABLE red_voting_token;
DROP TABLE account;
DROP TABLE bank;
DROP TABLE expenditure;
DROP TABLE list_subscribe_log; 
DROP TABLE mj_lists;
DROP TABLE mj_menus;
DROP TABLE mj_queries;
DROP TABLE mj_relationships;
DROP TABLE mj_sql_logs;
DROP TABLE mj_users;
DROP TABLE purchase;

