ALTER TABLE red_item ADD COLUMN item_quota_notify_exceeded_date datetime DEFAULT NULL; 
ALTER TABLE red_item CHANGE COLUMN item_quota_notify_date item_quota_notify_warning_date datetime DEFAULT NULL; 
ALTER TABLE red_item CHANGE COLUMN item_quota_notify_percent item_quota_notify_warning_percent tinyint unsigned NOT NULL DEFAULT 85;
UPDATE red_service SET service_description = 'Web configuration controls how your web site will be displayed. The web site quota is set by the owner setting.' WHERE service_id = 7;
