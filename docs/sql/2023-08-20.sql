ALTER TABLE red_hosting_order ADD COLUMN hosting_order_disabled_by_admin tinyint(1) DEFAULT 0;
ALTER TABLE red_item ADD COLUMN item_disabled_by_hosting_order tinyint(1) DEFAULT 0;
