ALTER TABLE red_item MODIFY COLUMN `item_status` enum('active','pending-delete','pending-update','pending-insert','pending-restore','deleted','hard-error','soft-error','transfer-limbo') default NULL;
ALTER TABLE red_hosting_order MODIFY COLUMN `hosting_order_status` enum('active','inactive','suspended','transfer-limbo') default NULL;
