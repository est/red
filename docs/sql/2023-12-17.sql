CREATE TABLE `red_queue` (
  `queue_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `queue_status` enum('running', 'pending', 'error', 'completed') DEFAULT 'pending',
  `queue_initial_item_status` enum('pending-insert', 'pending-update', 'pending-restore', 'pending-delete', 'pending-disable'),
  `queue_message` text DEFAULT NULL,
  `queue_last_updated` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
  PRIMARY KEY (`queue_id`),
  KEY `item_id` (`item_id`),
  KEY `queue_status` (`queue_status`),
  CONSTRAINT `red_queue_item_id` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `red_queue_task` (
  `queue_task_id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `queue_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `queue_task_status` enum('running', 'pending', 'error', 'completed') DEFAULT 'pending',
  `queue_task_function` varchar(255) DEFAULT NULL,
  `queue_task_args` text NOT NULL DEFAULT '',
  `queue_task_order` smallint(6) NOT NULL DEFAULT 0,
  PRIMARY KEY (`queue_task_id`),
  KEY `queue_id` (`queue_id`),
  KEY `queue_task_status` (`queue_task_status`),
  CONSTRAINT `red_queue_id` FOREIGN KEY (`queue_id`) REFERENCES `red_queue` (`queue_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

