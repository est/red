CREATE TABLE `red_item_mailbox` (
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
  `mailbox_login` varchar(128) DEFAULT NULL,
  `mailbox_auto_response` text DEFAULT NULL,
  `mailbox_auto_response_reply_from` varchar(128) DEFAULT NULL,
  `mailbox_auto_response_action` varchar(24) DEFAULT NULL,
  `mailbox_abandoned` tinyint(1) DEFAULT 0,
  `mailbox_mountpoint` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `mailbox_abandoned` (`mailbox_abandoned`),
  CONSTRAINT `red_item_mailbox_item_id` FOREIGN KEY (`item_id`) REFERENCES `red_item` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--- Create the new mailbox service record.
INSERT INTO red_service SET service_table = 'red_item_mailbox', service_area = 'hosting_order', service_name = 'Mailbox', service_description = 'You can configure one of your user accounts to receive email by adding a record here. Note: Set the password in the user account section, set the email addresses that delivers to this mailbox in the Email Addresses section.', service_delete_order = 50, service_order_by = 'mailbox_login', service_status = 'active', service_item = 1, service_default_host = 'mailstore001.mayfirst.org', service_display_order = 1;

--- Drop mountpoint from email address.
ALTER TABLE `red_item_email_address` DROP COLUMN `email_address_mountpoint`;

-- Change display order of service items
UPDATE red_service SET service_display_order = 5 WHERE service_name = 'Server Access';
UPDATE red_service SET service_display_order = 3 WHERE service_name = 'Nextcloud';
UPDATE red_service SET service_display_order = 2 WHERE service_name = 'Email Address';

-- Update explanations
UPDATE red_service SET service_description = "A user account is the basic building block for giving your users access to other features. You must create a user account before you can create a mailbox, Nextcloud account or give someone the ability to modify your web site." WHERE service_name = 'User Account';

UPDATE red_service SET service_description = "You can create email addresses that deliver to other email addresses or to the mailboxes you created in the mailbox section (by appending @mail.mayfirst.org to the mailbox login)." WHERE service_name = 'Email Address';

