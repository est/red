ALTER TABLE red_item_email_address ADD COLUMN email_address_mountpoint varchar(8) DEFAULT NULL;
ALTER TABLE red_server ADD COLUMN server_dedicated_mail tinyint(4) DEFAULT 0;
