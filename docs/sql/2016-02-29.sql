-- Changes for knot dns server backend
ALTER TABLE red_item_dns DROP COLUMN dns_timestamp;
ALTER TABLE red_item_dns ADD COLUMN dns_port smallint UNSIGNED DEFAULT 0;
ALTER TABLE red_item_dns ADD COLUMN dns_weight smallint UNSIGNED DEFAULT 0;
ALTER TABLE red_item_dns ADD COLUMN dns_zone varchar(255) AFTER item_id;
CREATE INDEX `dns_zone` ON red_item_dns (`dns_zone`); 
UPDATE red_item SET item_host = 'ranciere.mayfirst.org' WHERE item_status = 'active' AND service_id = 9;
UPDATE red_service SET service_default_host = 'ranciere.mayfirst.org' WHERE service_id = 9;
