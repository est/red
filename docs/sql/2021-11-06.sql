ALTER TABLE red_vps ADD COLUMN vps_type varchar(32);
ALTER TABLE red_vps ADD COLUMN vps_location varchar(32);
ALTER TABLE red_vps ADD COLUMN vps_managed char(1) DEFAULT 'n';
