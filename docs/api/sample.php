<?php

$api_user = 'testuser';
$api_password = 'secret';
$url = 'https://members.mayfirst.org/cp';
$hosting_order_id = 430;

$user_name = 'julio';
$user_pass = 'secret';
$email_address = 'julio@example.com';

function red_fetch($url, $data) {
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  $out = curl_exec($ch);
  if(!$out) {
    echo curl_error($ch);
  }
  curl_close($ch);
  return json_decode($out, $assoc = true);
}

$data = array(
  'action' => 'insert',
  'object' => 'item',
  'user_name' => $api_user,
  'user_pass' => $api_password,
  'set:service_id' => 1,
  'set:hosting_order_id' => $hosting_order_id,
  'set:user_account_login' => $user_name,
  'set:user_account_password' => $user_pass,
);

$ret = red_fetch($url, $data);

if($ret['is_error'] == 1) {
  // handle errors
} 

$data = array(
  'user_name' => $api_user,
  'user_pass' => $api_password,
  'action' => 'insert',
  'object' => 'item',
  'set:service_id' => 2,
  'set:hosting_order_id' => $hosting_order_id,
  'set:email_address' => $email_address,
  'set:email_recipient' => $user_name,
);

$ret = red_fetch($url, $data);

if($ret['is_error'] == 1) {
  // handle errors
}

?>
