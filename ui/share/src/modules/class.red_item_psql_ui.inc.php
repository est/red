<?php

if(!class_exists('red_item_psql_ui')) {
  class red_item_psql_ui extends red_item_psql {
    // provides password generator
    var $_javascript_includes = array('scripts/password_generator.js','scripts/item_psql.js');
    var $is_admin = false;
    var $_max_allowed_user_set_connections = 25;

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the PostgreSQL user and database " .
         $this->_html_generator->get_tag('span', $this->get_psql_name(),$attr) . "?";
    }
    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }
    function get_edit_psql_password() {
      // display confirm box
      $value = $this->get_psql_password();
      $pass_input = $this->_html_generator->get_input(
        'sf_psql_password',
        '',
        'password',
        ['size' => 30,'id' => 'psql_password']
      ) . '<br />';
      // create link for user to generate random password
      $link_attr = [
        'href' => '#',
        'onClick' => "pass=generateRandomPassword(10);parsePassword(pass);return false;"
      ];
      $password_link = $this->_html_generator->get_tag(
        'a',
        'Create Random Pass',
        $link_attr
      );
      // Create tag to display the password when the users clicks it
      $display_attr = array('id' => 'red_display_password');
      $password_display = $this->_html_generator->get_tag('span', '', $display_attr) . '<br />';

      // password confirm box
      $pass_confirm_input = $this->_html_generator->get_input(
        'sf_psql_password_confirm',
        '',
        'password',
        ['size' => 30,'id' => 'psql_password_confirm']
      ) . '<br />';

      // Directions for password confirm box
      $explanation_attr = array('class' => 'red_explanation');
      $message = $this->_html_generator->get_tag(
        'span',
        red_t("Please confirm the database user's password (or leave blank if not changing)"),
        $explanation_attr
      ) . '<br />';
      return $pass_input . $password_link . $password_display .
        $pass_confirm_input . $message;
    }

    function set_user_input($post) {
      $this->set_item_id($post['sf_item_id']);
      $this->reset_to_db_values();
      foreach($post as $k => $v) {
        if(preg_match('/sf_(.*)$/',$k,$matches)) {
          $field = $matches[1];
          // This field should not be set by the class - it's a
          // helper field
          if($field == 'psql_password_confirm') continue;

          // We need to auto create/manipulate the password field
          if($field == 'psql_password') {
            // this is hacky - validation errors should happen
            // when validation... but then the post variable is not
            // available there and by then the password will be
            // encrypted ...
            $login = '';
            if (array_key_exists('sf_psql_name', $post)) {
              // it's a new db
              $login = $post['sf_psql_name'];
            } else {
              $login = $this->get_psql_name();
            }
            if($v == '') {
              // Continue - if this record is being updated, then the
              // value from the db will be used. If it is a new record
              // then the empty password will be caught by the validate
              // script
              continue;
            }
            elseif($v != $post['sf_psql_password_confirm']) {
              $this->set_error(red_t("The database user's passwords do not match."), 'validation');
            }
            elseif(!red_is_good_password($v)) {
              $this->set_error(red_t("Please use a password that has at least 8 unique characters and has both letters and non-letters."),'validation');
            }
            elseif($v == $login) {
              $this->set_error(red_t("Please don't set the database user's password to the same value as the account's name. That's one of the first guesses of any EvilGenius."),'validation');
            }
            else {
              // Hash the password for postgres.
              $v = red_item_psql::hash_password($v);
            }
          }
          if ($this->accept_user_input($field)) {
            $this->set_field_value($field, $v);
          }
        }
      }
    }

    function additional_validation() {
      parent::additional_validation();
      if ($this->get_psql_max_connections() > $this->_max_allowed_user_set_connections && !$this->is_admin) {
          // This is only allowed if the value is the same as the value currently in the database
        // (otherwise, regular users could not edit a record that was increased by an admin).
        if ($this->exists_in_db()) {
          // Get the value of max_connections currently saved in the db.
          $item_id = intval($this->get_item_id());
          $sql = "SELECT psql_max_connections FROM red_item_psql WHERE item_id = #item_id";
          $result = red_sql_query($sql, ['#item_id' => $item_id]);
          $row = red_sql_fetch_row($result);
          // If they are different, then this user is trying to change this value. We don't
          // allow that.
          if($row[0] != $this->get_psql_max_connections()) {
            $err = red_t(
              'Only administrators are allowed to set Max Connections greater than @max_connections.',
              ['@max_connections' => $this->_max_allowed_user_set_connections]
            );
            $this->set_error($err, 'validation');
          }
        }
      }
    }

    function get_read_psql_name() {
      $name = $this->get_psql_name();
      $host = str_replace('.org', '.cx', $this->get_item_host());
      return $name . ' ' . $this->_html_generator->get_tag('em', "(psql://{$name}:[password]@{$host}/{$name})");


    }
    // We run on post_commit because we need the item_id.
    function _post_commit_to_db() {
      // We do not notify the host - we handle everything via the queue
      $this->_notify_host = FALSE;
      $active_states = ['pending-insert', 'pending-update', 'pending-restore'];
      $status = $this->get_item_status();
      if (in_array($status, $active_states)) {
        $action = "enable";
      }
      elseif ($status == 'pending-delete') {
        $action = "delete";
      }
      elseif ($status == 'pending-disable') {
        $action = "disable";
      }
      $item_id = $this->get_item_id();
      $this->queue->add_task('red_item_psql::apply', [$item_id, $action], 10);
      // Check the disk usage and update.
      $service = 'psql';
      $server = NULL;
      $this->queue->add_task('red_item::get_disk_usage_for_service', [$service, $server, $item_id], 20, 'red_item_psql::save_disk_usage');

      return parent::_post_commit_to_db();
    }
  }
}

?>
