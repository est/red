<?php

if(!class_exists('red_item_mailbox_ui')) {

  class red_item_mailbox_ui extends red_item_mailbox {
    var $is_admin = false;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);
    }

    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the mailbox for " . $this->_html_generator->get_tag('span',$this->get_mailbox_login(),$attr) . "? ALL email in this mailbox will be permanently deleted.";
    }


    function get_edit_mailbox_abandoned() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('mailbox_abandoned');
      }
      else {
        if ($this->get_mailbox_abandoned() == 0) {
          return red_t("Not abandoned");
        }
        else {
          return red_t("Yes, over 6 months without a login");
        }
      }
    }

    function get_edit_mailbox_mountpoint() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('mailbox_mountpoint');
      }
      else {
        return red_t("Only available to administrators");
      }
    }
    
    function additional_validation() {
      parent::additional_validation();
      // Only admins are allowed to change the mountpoint parameter.
      if (!$this->is_admin) {
        $item_id = $this->get_item_id();
        $mountpoint = $this->get_mailbox_mountpoint();
        if (!empty($mountpoint)) {
          // We are going to throw an error if the submitted value is not
          // the same as the value in the database.
          $throw_error = FALSE;
          if ($this->exists_in_db()) {
            $throw_error = TRUE;
            $sql = "SELECT mailbox_mountpoint FROM red_item_mailbox WHERE item_id = #item_id";
            $result = red_sql_query($sql, ['#item_id' => $item_id]);
            $row = red_sql_fetch_row($result);
            // If they are the same, then it's no problem.
            if($row[0] == $mountpoint) {
              // You are saved, no error.
              $throw_error = FALSE;
            }
          }
          if ($throw_error) {
            $this->set_error(red_t('Only administrators are allowed to set mountpoint.'), 'validation');
          }
        }
        $abandoned_mailbox = $this->get_mailbox_abandoned();
        // We are going to throw an error if the abandoned mailbox is different
        // then what is in the database..
        if ($this->exists_in_db()) {
          $throw_error = TRUE;
          $sql = "SELECT mailbox_abandoned FROM red_item_mailbox WHERE item_id = #item_id";
          $result = red_sql_query($sql, ['#item_id' => $item_id]);
          $row = red_sql_fetch_row($result);
          // If they are the same, then it's no problem.
          if(intval($row[0]) == intval($abandoned_mailbox)) {
            // You are saved, no error.
            $throw_error = FALSE;
          }
          if ($throw_error) {
            $this->set_error(red_t('Only administrators are allowed to set abandoned mailbox setting.'), 'validation');
          }
        }
      }
    }
    function _pre_commit_to_db() {
      if (!parent::_pre_commit_to_db()) return FALSE;

      // Add user to dovecot proxy server so they can login.
      $login = $this->get_mailbox_login();
      $active_states = ['pending-insert', 'pending-update', 'pending-restore'];
      if (in_array($this->get_item_status(), $active_states)) {
        $action = "add";
      }
      else {
        $action = "remove";
      }
      $host = $this->get_item_host();
      $this->queue->add_task('red_modify_dovecot_proxy_user', [$action, $login, $host], 10);

      // Rebuild databases so they can receive email at their @mail.mayfirst.org account.
      $this->queue->add_task('red_rebuild_email_databases', [], 20); 
      return TRUE;
    }
  }
}



?>
