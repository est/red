<?php

if (!class_exists('red_item_dns_ui')) {
  class red_item_dns_ui extends red_item_dns {
    var $_javascript_includes = array('scripts/item_dns.js');
    var $is_admin = FALSE;

    // constructor
    function __construct($construction_options) {
      // Call our various elders' construction options
      parent::__construct($construction_options);

    }
  
    function get_delete_confirmation_message() {
      $attr = array('class' => 'red-message-variable');
      return "Are you sure you want to delete the " . 
        $this->_html_generator->get_tag('span',$this->get_dns_type().  
          "/" . $this->get_dns_fqdn(),$attr) . 
        " dns entry?";
    }

    function get_edit_dns_type() {
      // You cannot change a DNS type once it is created - on the server we update records
      // based on fqdn + type so editing a type will have unexpected consequences.
      $attr = [];
      $multiple = FALSE;
      $edits_allowed = [ 'a', 'alias' ];
      $options = $this->get_dns_type_options();
      $type = $this->get_dns_type();
      if ($this->exists_in_db()) {
        if (!in_array($type, $edits_allowed)) {
          // Most records you cannot edit.
          $attr['disabled'] = 'disabled';
        }
        else {
          // But you can change an A to an ALIAS and vice versa. That ensures you can make
          // the change without editing the A record first.
          $options = [ 'a' => 'a', 'alias' => 'alias'];
        }
      }
      return $this->_html_generator->get_select('sf_dns_type', $options, $type, $multiple, $attr);
    }    
    function get_edit_dns_ttl() {
      return $this->get_auto_constructed_edit_field('dns_ttl') . ' seconds';
    }
    function get_read_dns_fqdn() {
      $non_active = '';
      // add a warning if it's an mx record, there's not dns server set
      // (it could be a mx record pointing lists
      if(!$this->name_service_active()) {
        $non_active = $this->_html_generator->get_tag('span','*',
                      array('class' => 'red-non-local-mx'));
      }
      return $this->get_auto_constructed_read_field('dns_fqdn') . $non_active;
    }

    // deterine whether name service is active for the given domain
    function name_service_active() {
      if ($this->is_local($this->get_dns_fqdn())) {
        return TRUE;
      }
      // we can't use a class variable because a new instance of this class
      // is created with every record
      static $dns_lookup_cache = array();

      // if we're offline, provide a warning and fail closed 
      if(!red_single_ping_result('1.1.1.1')) {
        return false;
      }

      $hostname = $this->get_dns_zone(); 
      // check cache
      if(array_key_exists($hostname,$dns_lookup_cache)) return $dns_lookup_cache[$hostname];

      $assigned = $this->get_nameservers($hostname);
      $records = @dns_get_record($hostname,DNS_NS);
      if($records) {
        foreach($records as $k => $record) {
          if(in_array($record['target'],$assigned)) {
            // store for faster lookups
            $dns_lookup_cache[$hostname] = true;
            return true;
          }
        }
      }
      // store for faster lookups
      $dns_lookup_cache[$hostname] = false;
      return false;  
    }
    function get_edit_dns_mx_verified() {
      // Only display for admins
      if ($this->is_admin) {
        return $this->get_auto_constructed_edit_field('dns_mx_verified');
      }
      else {
        return red_t("Only available to administrators");
      }
    }

    function _post_commit_to_db() {
      // If this is an MX domain and another mx domain matching this domain is verified
      // then this one should be considered verified as well.
      if ($this->get_dns_type() == 'mx' && $this->get_dns_mx_verified() == 0) {
        if (red_domain_is_verified($this->get_dns_fqdn())) {
          $this->set_dns_mx_verified(1);
        }
        else {
          // Every time a MX record that is unverified is changed, we need to trigger
          // an email to verify. 
          //
          // But first, we have to add this domain name to the list of domains on our
          // MX server so the test will be accepted.
          $this->queue->add_task('red_rebuild_unverified_domains_database', [], 10); 

          if (in_array($this->get_item_status(), ['pending-delete', 'pending-disable'])) {
            // Clean up the red_mx_verify table. No need to re-verify.
            $item_id = $this->get_item_id();
            $sql = "DELETE FROM red_mx_verify WHERE item_id = #item_id";
            red_sql_query($sql, ['#item_id' => $item_id]);
          }
          else {
            // Send verification.
            $this->queue->add_task('red_send_mx_verification_after_delay', [$this->get_item_id()], 15);
          }
        }
      
      }
      return parent::_post_commit_to_db();
    }
  }
}

?>
