<?php

/**
 * Class for reseting or requesting a new password.
 */
class red_password {

  // Santitized user input.
  private $user_input = [];
  private $template = NULL;
  // The message to display back to the user.
  private $message = NULL;

  public function set_user_input($input) {
    $this->user_input = $input;
  }

  public function set_template($template) {
    $this->template = $template;
  }

  public function reset_pass() {
    $this->template->set_file('reset_password_file', 'reset_password.ihtml');
    // Determine the action to take.
    $action = NULL;

    $action = $this->user_input['password_action'] ?? NULL;
    $h = $this->user_input['h'] ?? NULL;

    // If following a reset URL, no action is specified
    // but h is in the get array.
    if (!$action && $h) {
      $action = 'display_password_reset_form';
    }

    // If still not action, display new form to request
    // password reset
    if (!$action) {
      $action = 'new';
    }

    switch ($action) {
      case 'submit_new_password':
        // A brand new password is being submitted
        // We still check to ensure the hash is correct.
        if(!$this->is_valid_hash($h)) {
          red_set_message($this->message, 'error');
          break;
        }
        else {
          if ($this->submit_new_password()) {
            red_set_message($this->message, 'success');
            break;
          }
          else {
            red_set_message($this->message, 'error');
            // fall through
          }
        }

      case 'display_password_reset_form':
        // The user is following a reset password link. Check if hash is valid.
        if ($this->is_valid_hash($h)) {
          // Display change password form
          $this->display_password_reset_form();
        }
        else {
          red_set_message($this->message, 'error');
        }
        break;

      case 'submit_username':
        // The user is submitting the username of the user whose
        // password they want to reset - generate an email
        // to the billing contact
        //
        if (true === $this->submit_username()) {
          red_set_message($this->message, 'success');
          break;
        }
        else {
          red_set_message($this->message, 'error');
          // fall through
        }
      case 'new':
        // The user is requesting the form to submit a password reset request
        $this->template->set_file('enter_username_file','enter_username.ihtml');
        $this->template->parse('password_reset_block','enter_username_file');
    }
    $this->template->parse('body_block', 'reset_password_file');
  }

  function get_reset_emails($username) {
    $sql = "SELECT red_hosting_order.member_id 
      FROM 
        red_hosting_order JOIN
        red_item USING(hosting_order_id) JOIN
        red_item_user_account USING(item_id) 
      WHERE
        user_account_login = @username AND 
        item_status = 'active'";
    $result = red_sql_query($sql, ['@username' => $username]);
    if (!$result) {
      $this->message = "There was an error getting your membership info.";
      return false;
    }
    $row = red_sql_fetch_row($result);
    $member_id = $row[0];
    if (empty($member_id)) return array();
    $sql = "SELECT contact_email FROM red_contact
      WHERE 
        member_id = #member_id AND
        contact_email != '' AND 
        contact_status = 'active'";
    $result = red_sql_query($sql, ['#member_id' => $member_id]);
    if (!$result) {
      $this->message .= "There was an error getting your membership contacts.";
      return false;
    }
    // Make the array easier to manage
    $ret = array();
    while($v = red_sql_fetch_row($result)) {
      if (!empty($v[0])) {
        $ret[] = $v[0];
      }
    }
    // Now check for a recovery email.
    $sql = "SELECT 
        user_account_recovery_email 
      FROM 
        red_item_user_account JOIN 
        red_item USING(item_id) 
      WHERE 
        user_account_login = @username AND 
        item_status = 'active'";
    $result = red_sql_query($sql, ['@username' => $username]);
    if (!$result) {
      $this->message = "There was an error getting your recovery email. ";
      return false;
    }
    $row = red_sql_fetch_row($result);
    if ($row) {
      if (!empty($row[0])) {
        $ret[] = $row[0];
      }
    }
    return $ret;
  }

  function get_username_for_hash($hash) {
    $sql = "SELECT pass_reset_login FROM red_item_pass_reset WHERE pass_reset_hash = @hash";
    $result = red_sql_query($sql, ['@hash' => $hash]);
    $row = red_sql_fetch_row($result);
    return $row[0];
  }

  function is_valid_hash($hash) {
    $sql = "SELECT pass_reset_expires,pass_reset FROM red_item_pass_reset 
      JOIN red_item USING(item_id) WHERE pass_reset_hash = @hash 
      AND item_status = 'active'";
    $result = red_sql_query($sql, ['@hash' => $hash]);
    if (red_sql_num_rows($result) == 0) {
      $this->message = red_t("That is an invalid hash.");
      return FALSE;
    }
    $row = red_sql_fetch_row($result);
    $expires = strtotime($row[0]);
    if ($expires < time()) {
      $this->message = red_t('That hash has expired.');
      return FALSE;
    }
    if ($row[1] != '0000-00-00 00:00:00') {
      $this->message = red_t('That hash has already been used.');
      return FALSE;
    }
    return TRUE;
  }

  function is_current_hash($hash) {
    $now = date("Y-m-h H:i:s");
    $sql = "SELECT password_reset_id FROM password_reset JOIN red_item ".
      "USING(item_id) WHERE hash = @hash ".
      "AND expires > @now AND reset = '0000-00-00 00:00:00'";
    $result = red_sql_query($sql, [
      '@hash' => $hash,
      '@now' => $now,
    ]);
    if (red_sql_num_rows($result) == 0) {
      return false;
    }
    return true;
  }
  function submit_new_password() {
    $hash = $this->user_input['h'];
    $username = $this->get_username_for_hash($hash);
    $password = $this->user_input['password'];
    $password_confirm = $this->user_input['password_confirm'];
    if($password == '') {
      $this->message = 'You must enter your new password.';
      return FALSE;
    }
    elseif ($password != $password_confirm) {
      $this->message = "Your passwords don't match.";
      return FALSE;
    }
    elseif (!red_is_good_password($password)) {
      $this->message = "Sorry, but that password was too simple. ".
        "Please make it at least 8 unique characters and ensure that it ".
        "has both letters and non-letters. Thanks.";
      return FALSE;
    }
    $sql = "SELECT * FROM red_item JOIN red_item_user_account ".
      "USING(item_id) WHERE user_account_login = @username AND ".
      "item_status = 'active'";
    $result = red_sql_query($sql, ['@username' => $username]);
    if(red_sql_num_rows($result) == 0) {
      $this->message = 'That username no longer exists, is disabled or is currently being updated.';
      return FALSE;
    }
    $co = [];
    $co['mode'] = 'ui';
    $co['rs'] = red_sql_fetch_assoc($result);
    $red = red_item::get_red_object($co);
    $red->set_user_account_password($password);
    if (!$red->validate()) {
      $this->message = "There was an error. Sorry, your password did not ".
        "properly validate.";
      $errors = $red->get_errors();
      foreach($errors as $v) {
        red_log($v['error']);
      }
      return FALSE;
    }
    if (!$red->commit_to_db()) {
      $this->message = "There was an error updating the database.";
      $errors = $red->get_errors();
      foreach($errors as $v) {
        red_log($v['error']);
      }
      return FALSE;
    }
    $hosting_order_id = $red->get_hosting_order_id();
    // update the reset time in the db
    $co = [];
    $co['mode'] = 'ui';
    $co['service_id'] = red_get_service_id_for_service_table('red_item_pass_reset');
    $co['hosting_order_id'] = $hosting_order_id;

    $pass_reset = new red_item_pass_reset_ui($co, $hash);
    // record time it was reset
    $reset = date("Y-m-d H:i:s", time());
    $pass_reset->set_pass_reset($reset);
    if (!$pass_reset->commit_to_db()) {
      red_log("The password was updated, however, the hash was not." . 
        $pass_reset->get_errors_as_string());
    }
    $this->message = "The password has been updated.";
    $ip = $_SERVER['REMOTE_ADDR'];
    red_log("Password for '{$username}' has been updated, IP: {$ip}");
    return TRUE;
  }

  function submit_username() {
    $username = $this->user_input['user_name'] ?? NULL;
    if (!$username) {
      $this->message = "No username was submitted.";
      return FALSE;
    }
    // create hash
    $hosting_order_id = $this->get_hosting_order_id($username);
    if (!$hosting_order_id) {
      $this->message = "That username is not valid.";
      return false;
    }
    $co = [];
    $co['mode'] = 'ui';
    $co['hosting_order_id'] = $hosting_order_id;
    $co['service_id'] = red_get_service_id_for_service_table('red_item_pass_reset');
    $hash = NULL;
    $pass_reset = new red_item_pass_reset_ui($co, $hash, $username);
    $emails = $this->get_reset_emails($username);
    if (false === $emails) {
      return false;
    }
    $ip = $_SERVER['REMOTE_ADDR'];
    if (count($emails) == 0) {
      red_log("Password reset for '{$username}' returns no records, IP: {$ip}");
      $return_message = "We were unable to locate any email addresses to send your password reset link. Please email support @ mayfirst.org for help.";
      return FALSE;
    }
    if (!$pass_reset->validate()) {
      $return_message = "There was an error. Sorry, your password reset link did not ".
        "properly validate.<br>";
      $errors = $pass_reset->get_errors();
      foreach($errors as $v) {
        $this->message .= $v['error'] . "<br />";
      }
      return false;
    }
    if (!$pass_reset->commit_to_db()) {
      $return_message = "There was an error updating the database.";
      $errors = $pass_reset->get_errors();
      foreach ($errors as $v) {
        $return_message .= $v['error'] . "<br />";
      }
      return false;
    }
    if (!$pass_reset->send_hash($emails)) {
      $return_message = "Failed to send email. " .
        $pass_reset->get_errors_as_string();
      return false;
    }
    $return_message = '';
    $print_emails = array();
    reset($emails);
    foreach($emails as $email) {
      $print_emails[] = $this->obscure_email($email);
    }
    $this->message .= "An email was sent to ".  implode(' ', $print_emails) . " ".
      "with a link. Please check your email and click on the link to ".
      "reset your password.";
    $this->message .= "The link will expire on " .
      $pass_reset->convert_expires_to_friendly_date();

    red_log("Password reset for '{$username}' successfully sent, IP: {$ip}");
    return true;
  }

  // Take an email and partially obscure it for privacy.
  // e.g. joe@example.com => j**@ex****.com
  function obscure_email($email) {
    if (empty($email)) {
      return NULL;
    }

    $parts = explode('@', $email);
    $user = array_key_exists(0, $parts) ? $parts[0] : NULL;
    $domain = array_key_exists(1, $parts) ? $parts[1] : NULL;

    if (empty($user) || empty($domain)) {
      return NULL;
    }

    $user_len = strlen($user);
    $domain_len = strlen($domain);

    $pos = 2;

    $start = $end = '';
    for($i = 0; $i <= ($user_len/$pos) - 1; $i++) {
      $start .= 'x';
    }
    for($i = 0; $i <= ($domain_len/$pos) - 1; $i++)
    {
      $end .= 'x';
    }

    return substr_replace($user, $start, 2, ceil($user_len/$pos)) .
      '@' . substr_replace($domain, $end, 2, ceil($domain_len/$pos));
  }

  function display_password_reset_form() {
    $hash = $this->user_input['h'];
    $username = $this->get_username_for_hash($hash);
    $this->template->set_file('enter_password_file','enter_password.ihtml');
    $this->template->set_var('username', $username);
    $this->template->set_var('hash', $hash);
    $this->template->parse('password_reset_block','enter_password_file');
  }

  function get_hosting_order_id($login) {
    $sql = "SELECT hosting_order_id FROM red_item JOIN ".
      "red_item_user_account USING(item_id) WHERE user_account_login = ".
      "@login AND item_status = 'active'";
    $result = red_sql_query($sql, ['@login' => $login]);
    $row = red_sql_fetch_row($result);
    return $row[0] ?? NULL;
  }
}
