<?php
  
class red_area_top extends red_area {
  var $default_service_id = 14; // member 
  // there are no units in this area since we're at the top
  // of the hierarchy
  var $unit_friendly_name = null;
  var $unit_key_field = null;
  var $unit_friendly_field = null;
  var $unit_table = null;
  var $service_key_field = 'member_id';
  var $member_parent_id_options = null;
  var $server_options = null;
  var $search_keywords = null;
  var $search_tokens = array();

  function __construct($area, $service_id) {
    parent::__construct($area, $service_id);
    if($this->service_id == 15) {
      $this->service_key_field = 'map_user_server_id';
      $this->order_by_fields[] = 'login';
    } else if($this->service_id == 22) {
      $this->service_key_field = 'search_id';
    } else {
      $this->order_by_fields[] = 'member_friendly_name';
    }
  }
    
  function get_single_object($co) {
    global $globals;
    $member_parent_id_options = $this->get_member_parent_id_options();
    if ($this->service_id == 15) {
      $ret = new red_map_user_server($co);
      // set server options
      $ret->server_options = $this->get_server_options();
      // change default option
      $member_parent_id_options[0] = "--Choose One--";
    } 
    elseif($this->service_id == 22) {
      $ret = new red_search($co);
    } 
    else {
      $ret = new red_member($co);
      // set the available member_parent_ids
      $ret->member_parent_id_options = $member_parent_id_options;    
    }
    
    return $ret;
  }

  function get_area_navigation_items($top,$member,$hosting_order) {
    $ret = array();
      
    $items = array();
    if($top) {
      $items['top'] = array(
        'friendly' => 'Top',
      );
    }
    if($member) {
      $items['member'] = array(
        'friendly' => 'Members', 
      );
    }
    if($hosting_order) {
      $items['hosting_order'] = array(
        'friendly' => 'Hosting Orders',
      );
    }

    return $items;
  }
  function get_server_options() {
    if(!is_null($this->server_options))
      return $this->server_options;

    $ret = array();
    $sql_select = "SELECT server ";
    $sql_from = "FROM red_server ";
    $sql_where = '';
    $params = [];
    if(!$this->is_admin) {
      $params['@user_name'] = $this->user_name;
      $sql_from .= " JOIN red_map_user_server USING (server) ";
      $sql_where .= "WHERE login = @user_name";

    }
    $sql = $sql_select . $sql_from . $sql_where . ' ORDER BY server';
    $result = red_sql_query($sql, $params);
    while($row = red_sql_fetch_row($result)) {
      $server = $row[0];
      $ret[$server] = $server;
    }
    $this->server_options = $ret;
    return $ret;
  }

  function get_member_parent_id_options() {
    if(!is_null($this->member_parent_id_options))
      return $this->member_parent_id_options;

    $ret = array();
    $params = [];
    $ret[0] = '--Do not set parent--';
    $sql_select = "SELECT member_id,member_friendly_name ";
    $sql_from = "FROM red_member ";
    $sql_where = "WHERE member_status = 'active' ";
    if(!$this->is_admin) {
      $sql_from .= " JOIN red_map_user_server USING(member_id) ";
      $sql_where .= "AND login = @user_name";
      $params['@user_name'] = $this->user_name;
    }

    $sql = $sql_select . $sql_from . $sql_where . ' ORDER BY member_friendly_name';
    $result = red_sql_query($sql, $params);
    while($row = red_sql_fetch_row($result)) {
      $id = $row[0];
      $ret[$id] = red_truncate_from_middle($row[1],60);
    }
    $this->member_parent_id_options = $ret;
    return $ret;
  }

  function get_list_links($key_field_name,$id, $status = Null) {
    $ret = $this->get_list_edit_link($key_field_name,$id) .
      '&nbsp;' . $this->get_list_delete_link($key_field_name,$id);  
    if($key_field_name == 'member_id')
      $ret .= '&nbsp;' . $this->get_list_view_link('member',$key_field_name,$id);
    return $ret;
  }

  function list_children($start = 0,$limit = 50) {
    $this->prepare_list();
    if($this->is_admin && $this->service_id == 22) {
      $this->template->set_file('search_file','search.ihtml');
      $this->template->set_var('search_keywords',$this->search_keywords);
      $this->template->set_var('lang_search',red_t("Search"));
      $this->template->parse('search_block','search_file');
    }
    $this->template->set_file('child_items_file','child_items.ihtml');
    $this->template->set_var('list_block',$this->get_list_block($start,$limit));
    $this->template->parse('children','children_file');
    $this->template->parse('body_block','child_items_file');
  }

  function get_list_sql($start = 0, $limit = 50) {
    $start = intval($start);
    $limit = intval($limit);
    $member_id = intval($this->unit_id);
    $order_by = '';
    // We can't properly set user name as a token, so addslashes here.
    $user_name = addslashes($this->user_name);
    if ($this->order_by_fields) {
      foreach ($this->order_by_fields as $field) {
        // This will trigger an error if any illegal characters are present.
        red_escape_field_or_table_name($field, 'order by red area top');
      }
      $order_by = "ORDER BY " . implode(',', $this->order_by_fields);
    }
   
    if ($this->service_key_field == 'member_id') {
      $sql_start = "SELECT red_member.* FROM red_member ";
      $sql_where = "WHERE member_status = 'active' ";
      if(!$this->is_admin) {
        $sql_start .= "JOIN red_map_user_server ON red_member.member_parent_id = ".
          "red_map_user_server.member_id ";
        $sql_where .=  "AND login = '$user_name' AND ".
          "red_map_user_server.status = 'active' ";
      }
      $sql = $sql_start . $sql_where . $order_by . " LIMIT $start, $limit";

    } 
    elseif ($this->service_key_field == 'search_id') {
      $sql = $this->get_search_sql($start, $limit);
    } 
    else {

      $sql = "SELECT * FROM red_map_user_server WHERE red_map_user_server.status = 'active' ";
      if(!$this->is_admin) {
        $sql .= "AND member_id IN (" . 
          implode(',',$this->get_authorized_parent_member_ids()) .
          ")";
      }
      $sql .= $order_by . " LIMIT $start, $limit";
    }
    return $sql;
  }

  function parse_search_keywords($keywords) {
    $this->search_keywords = $keywords;
    $this->tokenize_search_keywords();
  }

  function tokenize_search_keywords() {
    $this->search_tokens = array();
    $search_tokens = explode(' ',$this->search_keywords);
    foreach($search_tokens as $token) {
      // check if the token should be limited
      if(preg_match('/:/',$token)) {
        $parts = explode(':',$token);
        $index = $parts[0];
        $string = $parts[1];
        $this->search_tokens[] = array(
          'index' => $index,
          'value' => addslashes($string)
        );
      } else {
        $this->search_tokens[] = array(
          'index' => '*',
          'value' => addslashes($token)
        );
      }
    }
  }

  function get_search_where($fields,$table) {
    reset($this->search_tokens);
    $ret = array();
    foreach($this->search_tokens as $token_array) {
      $index = $token_array['index'];
      $token = $token_array['value'];
      if($index != '*' && $index != $table) continue;
      reset($fields);
      $field_ret = array();
      foreach($fields as $field) {
        if(preg_match('/.*_id$/',$field)) {
          if(is_numeric($token)) {
            $id = intval($token);
            $field_ret[] = "$field = $id";
          }
        } else {  
          $field_ret[] = "$field LIKE '%$token%'";
        }
      }
      $ret[] = '(' . implode(' OR ',$field_ret) . ')';
    }
    return implode(' AND ',$ret);
  }

  function is_global_search() {
    $indices = $this->get_search_indices();
    if(in_array('*',$indices)) return true;
    return false;
  }

  function get_search_indices() {
    $ret = array();
    reset($this->search_tokens);
    foreach($this->search_tokens as $token_array) {
      $ret[] = $token_array['index'];
    }
    return array_unique($ret);
  }

  function get_search_sql($start, $limit) {
    if(empty($this->search_keywords)) return '';
    // create a giant union sql statement
    $sqls = array();
    $global_search = $this->is_global_search();
    $search_indices = $this->get_search_indices();
    # members
    $index = 'member';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('member_id','member_friendly_name','unique_unix_group_name');
      $sqls[] = "(SELECT DISTINCT 1 AS search_id,'Member' AS type, ".
        "member_friendly_name AS display, ".
        "CONCAT('area=member&amp;member_id=',member_id) AS view_link, ".
        "CONCAT('area=top&amp;service_id=14&amp;action=edit&amp;member_id=',member_id) AS edit_link, ".
        "CONCAT('area=top&amp;service_id=14&amp;action=delete&amp;member_id=',member_id) AS delete_link ".
        "FROM red_member JOIN red_unique_unix_group USING(unique_unix_group_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND member_status = 'active' ORDER BY display)";
    }
    // hosting orders
    $index = 'hosting_order';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('hosting_order_id','hosting_order_identifier','unique_unix_group_name');
      $sqls[] = "(SELECT 1 AS search_id,'Hosting Order' AS type, ".
        "hosting_order_identifier AS display, ".
        "CONCAT('area=hosting_order&amp;hosting_order_id=',hosting_order_id) AS view_link, ".
        "CONCAT('area=member&amp;service_id=12&amp;action=edit&amp;hosting_order_id=',hosting_order_id) AS edit_link, ".
        "CONCAT('area=member&amp;service_id=12&amp;action=delete&amp;hosting_order_id=',hosting_order_id) AS delete_link ".
        "FROM red_hosting_order JOIN red_unique_unix_group USING(unique_unix_group_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND (hosting_order_status = 'active' OR hosting_order_status = 'disabled') ORDER BY display)";
    }
    //user accounts 
    $index = 'user_account';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','user_account_login');
      $sqls[] = "(SELECT 1 AS search_id,'User Account' AS type, ".
        "user_account_login AS display, ".
        "CONCAT('area=hosting_order&amp;service_id=1&amp;hosting_order_id=',hosting_order_id) AS view_link, ".
        "'' AS edit_link, '' AS delete_link ".
        "FROM red_item JOIN red_item_user_account USING(item_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND (item_status = 'active' OR item_status = 'disabled') ORDER BY display)";
    }
    // web conf 
    $index = 'web_conf';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','web_conf_domain_names', 'web_conf_writer_uid');
      $sqls[] = "(SELECT 1 AS search_id,'Web Conf' AS type, ".
        "red_item.item_id AS display, ".
        "CONCAT('area=hosting_order&amp;service_id=7&amp;hosting_order_id=',hosting_order_id) AS view_link, ".
        "'' AS edit_link, '' AS delete_link ".
        "FROM red_item JOIN red_item_web_conf USING(item_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND (item_status = 'active' OR item_status = 'disabled') ORDER BY display)";
    }

    //  nextcloud 
    $index = 'nextcloud';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','nextcloud_login');
      $sqls[] = "(SELECT 1 AS search_id,'Nextcloud' AS type, ".
        "nextcloud_login AS display, ".
        "CONCAT('area=hosting_order&amp;service_id=37&amp;hosting_order_id=',hosting_order_id) AS view_link, ".
        "'' AS edit_link, '' AS delete_link ".
        "FROM red_item JOIN red_item_nextcloud USING(item_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND (item_status = 'active' OR item_status = 'disabled') ORDER BY display)";
    }
    // mailbox 
    $index = 'mailbox';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','mailbox_login');
      $sqls[] = "(SELECT 1 AS search_id,'Mailbox' AS type, ".
        "mailbox_login AS display, ".
        "CONCAT('area=hosting_order&amp;service_id=38&amp;hosting_order_id=',hosting_order_id) AS view_link, ".
        "'' AS edit_link, '' AS delete_link ".
        "FROM red_item JOIN red_item_mailbox USING(item_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND (item_status = 'active' OR item_status = 'disabled') ORDER BY display)";
    }

    //mysql databases 
    $index = 'mysql_database';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','mysql_db_name');
      $sqls[] = "(select 1 as search_id,'mysql database' as type, ".
        "mysql_db_name as display, ".
        "concat('area=hosting_order&amp;service_id=20&amp;hosting_order_id=',hosting_order_id) as view_link, ".
        "'' as edit_link, '' as delete_link ".
        "from red_item join red_item_mysql_db using(item_id) ".
        "where " . $this->get_search_where($search_fields,$index) . ' ' .
        "and (item_status = 'active' OR item_status = 'disabled') order by display)";
    }

    //mysql users 
    $index = 'mysql_user';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','mysql_user_name');
      $sqls[] = "(select 1 as search_id,'mysql user' as type, ".
        "mysql_user_name as display, ".
        "concat('area=hosting_order&amp;service_id=21&amp;hosting_order_id=',hosting_order_id) as view_link, ".
        "'' as edit_link, '' as delete_link ".
        "from red_item join red_item_mysql_user using(item_id) ".
        "where " . $this->get_search_where($search_fields,$index) . ' ' .
        "and (item_status = 'active' OR item_status = 'disabled') order by display)";
    }

    // contacts
    $index = 'contact';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('contact_id','contact_first_name','contact_last_name');
      $sqls[] = "(SELECT 1 AS search_id,'Contact' AS type, ".
        "CONCAT(contact_first_name,' ', contact_last_name, ' (',member_friendly_name, ') ') AS display, ".
        "CONCAT('area=member&amp;service_id=16&amp;member_id=',member_id) AS view_link, ".
        "'' AS edit_link, '' AS delete_link ".
        "FROM red_contact JOIN red_member USING(member_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND contact_status = 'active' ORDER BY display)";
    }

    // dns 
    $index = 'dns';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','dns_fqdn');
      $sqls[] = "(SELECT 1 AS search_id,'DNS' AS type, ".
        "dns_fqdn AS display, ".
        "CONCAT('area=hosting_order&amp;service_id=9&amp;hosting_order_id=',hosting_order_id) AS view_link, ".
        "'' AS edit_link, '' AS delete_link ".
        "FROM red_item JOIN red_item_dns USING(item_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND (item_status = 'active' OR item_status = 'disabled') ORDER BY display)";
    }

    //  email addresses 
    $index = 'email_address';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','email_address','email_address_recipient');
      $sqls[] = "(SELECT 1 AS search_id,'Email address' AS type, ".
        "CONCAT(email_address, ' -> ',email_address_recipient) AS display, ".
        "CONCAT('area=hosting_order&amp;service_id=2&amp;hosting_order_id=',hosting_order_id) AS view_link, ".
        "'' AS edit_link, '' AS delete_link ".
        "FROM red_item JOIN red_item_email_address USING(item_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND (item_status = 'active' OR item_status = 'disabled') ORDER BY display)";
    }

    //  mailman 
    $index = 'mailman';
    if($global_search || in_array($index,$search_indices)) {    
      $search_fields = array('item_id','list_name','list_domain');
      $sqls[] = "(SELECT 1 AS search_id,'Email List' AS type, ".
        "CONCAT(list_name, '@',list_domain) AS display, ".
        "CONCAT('area=hosting_order&amp;service_id=',service_id,'&amp;hosting_order_id=',hosting_order_id) AS view_link, ".
        "'' AS edit_link, '' AS delete_link ".
        "FROM red_item JOIN red_item_list USING(item_id) ".
        "WHERE " . $this->get_search_where($search_fields,$index) . ' ' .
        "AND (item_status = 'active' OR item_status = 'disabled') ORDER BY display)";
    }

    // red_set_message(implode(' UNION ',$sqls) . " LIMIT $start, $limit");
    return implode(' UNION ',$sqls) . " LIMIT $start, $limit";
  }

  // return parent ids the current user has access to
  function get_authorized_parent_member_ids() {
    $sql = "SELECT member_id FROM red_map_user_server ".
      "WHERE login = @user_name AND status = 'active'";
    $result = red_sql_query($sql, ['@user_name' => $this->user_name]);
    $ret = array();
    while($row = red_sql_fetch_row($result)) {
      $ret[] = $row[0];
    }
    return $ret;
  }

  function check_access($id = null, $action = null) {
    // admins have access to everything (this includes search - only admins
    // should be able to search)
    if($this->is_admin) return true;
    if($this->service_key_field == 'search_id') {
      return false;
    }

    if(is_null($id)) $id = $this->id;

    if(empty($id)) {
      // make sure they have access to at least one member
      $ids =  $this->get_authorized_parent_member_ids();
      if(count($ids) == 0) return false;
      return true;
    }

    if($this->service_key_field == 'member_id') {
      // make sure this member really can be accessed by this user. Their login must have explicit
      // access to the item's member_parent_id as defined in the red_map_user_server table.
      $sql = "SELECT member_parent_id FROM red_member WHERE member_id = #id ".
        "AND member_status = 'active'";
      $result = red_sql_query($sql, ['#id' => $id]);
      $row = red_sql_fetch_row($result);
      $member_parent_id = intval($row[0]);
      $user_name = addslashes($this->user_name);
      $sql = "SELECT map_user_server_id FROM red_map_user_server WHERE member_id = #member_parent_id ".
        "AND login = @user_name";
      $result = red_sql_query($sql, ['#member_parent_id' => $member_parent_id, '@user_name' => $user_name]);
      if(red_sql_num_rows($result) == 0) {
        $this->errors[2] = "There is an error. You don't seem to have access to member id: $id"; 
        return false;
      }
    } else {
      // make sure the map_user_server record they are trying to manipulate is coded to a 
      // member_id that the current user has access to.
      $sql = "SELECT member_id FROM red_map_user_member WHERE map_user_member_id = #id";
      $result = red_sql_query($sql, ['#id' => $id]);
      $row = red_sql_fetch_row($result);
      $member_parent_id = $row[0];

      $sql = "SELECT map_user_server_id FROM red_map_user_server WHERE member_id = #member_parent_id ".
        "AND login = @user_name";

      $result = red_sql_query($sql, ['#member_parent_id' => $member_parent_id, '@user_name' => $user_name]);
      $row = red_sql_fetch_row($result);
      if(red_sql_num_rows($result) == 0) {
        $this->errors[2] = "There is an error. You don't seem to have access to map user server id: $id"; 
        return false;
      }
    }
    return true;
  }
  function get_table_row_for_single_object(&$object,$row_number) {
    if($this->service_id != 22) return parent::get_table_row_for_single_object($object,$row_number);

    $table_row = array();

    $object->set_html_generator($this->html_generator);
    if($row_number == 0)  {
      $row = $object->get_enumerate_header_block().
        $this->html_generator->get_table_cells("&nbsp;");
      $table_row[] = $this->html_generator->get_table_row($row);
    }
    $row = $object->get_enumerate_data_block();
    $attr = array('class' => 'red-row-even');
    if(!is_int($row_number/2)) $attr = array('class' => 'red-row-odd');
    $table_row[] = $this->html_generator->get_table_row($row,$attr);
    return $table_row;
  }  
}


?>
