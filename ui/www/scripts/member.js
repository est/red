$(document).ready(function () {

  // FIXME - this must stay in sync with config.inc.php!
  var prices = {
    'USD:individual:dues': 25, 
    'USD:organization:dues': 50, 
    'USD:organization:benefits': 150, 
    'USD:individual:benefits': 75, 
    'MXN:individual:dues': 205, 
    'MXN:organization:dues': 410, 
    'MXN:organization:benefits': 1230, 
    'MXN:individual:benefits': 615, 
  };

  // Update the membership dues and benefits based on membership type
  // selected.
  $("#member_benefits_level").change(function() {
    red_update_membership_prices(prices);
  });
  $("#member_currency").change(function() {
    red_update_membership_prices(prices);
  });
  $("#member_type").change(function() {
    red_update_membership_prices(prices);
  });
});

function red_update_membership_prices(prices) {
  var cur = $("#member_currency").val();
  var level = $("#member_benefits_level").val();
  var type = $("#member_type").val();
  var dues = 0;
  var benefits = 0;
  if (cur != "" && level != "" && type != "") {
    // Dues are the same price regardless of our level.
    $("#member_price").val(prices[cur + ':' + type + ':dues']);

    if (level == 'standard') {
      $("#member_benefits_price").val(prices[cur + ':' + type + ':benefits']);
    }
    else if (level == 'extra') {
      $("#member_benefits_price").val('?');
    }
    else {
      $("#member_benefits_price").val(0);
   }
 }
}
