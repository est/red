function toggleVisibilityId(id) {
  var el = document.getElementById(id);
  toggleElementVisibility(el);
}

function toggleVisibilityElement(el) {
  if (el.style.display == 'block') {
    el.style.display = 'none';
  } else {
    el.style.display = 'block';
  }
}

function toggleVisibilityFieldset(fieldsetId, contentId) {
  fieldset = document.getElementById(fieldsetId);
  content = document.getElementById(contentId);
  toggleVisibilityElement(content);
  toggleBorders(fieldset);
}

function toggleBorders(el) {
  if (el.style.borderLeft == "") {
    el.style.borderLeft = "solid 1px #bbb";
    el.style.borderRight = "solid 1px #bbb";
    el.style.borderBottom = "solid 1px #bbb";
  } else {
    el.style.borderLeft = "";
    el.style.borderRight = "";
    el.style.borderBottom = "";
  }

}
