$(document).ready(function() {
  split('#web_conf_domain_names');

  // Check if we are on new infrastructure and hide elements as
  // necessary (until we completely move to new infrastructure).
  var on_new_infrastructure = $('#red-edit-table').data("red-on-new-infrastructure");

  function toggle_advanced_settings() {
    var advanced = [];
    if (!on_new_infrastructure) {
      advanced = [ "web-conf-tls-cert", "web-conf-tls-key", "web-conf-logging", "web-conf-tls-redirect", "web-conf-cgi", "web-conf-max-memory", "web-conf-max-processes", "web-conf-cache-settings", "web-conf-settings", "web-conf-document-root", "web-conf-php-version", "web-conf-mountpoint", "web-conf-cache-type" ];
    }
    else {
      // On new infrastructure, we omit the tls keys.
      advanced = [ "web-conf-logging", "web-conf-tls-redirect", "web-conf-cgi", "web-conf-max-memory", "web-conf-max-processes", "web-conf-cache-settings", "web-conf-settings", "web-conf-document-root", "web-conf-php-version", "web-conf-mountpoint", "web-conf-cache-type" ];
    }
    var action = "hide";
    if ($("#red-web-conf-toggle-advanced").data("visibility-state") == "hidden") {
      action = "show"
    }
    for (var i = 0, size = advanced.length; i < size ; i++) {
      if (action == 'hide') {
        $("#red-" + advanced[i] + "-table-row").hide();
      }
      else {
        $("#red-" + advanced[i] + "-table-row").show();
      }
    }
    if (action == 'hide') {
      $("#red-web-conf-toggle-advanced").val("Show Advanced Settings");
      $("#red-web-conf-toggle-advanced").data("visibility-state", "hidden");
    }
    else {
      $("#red-web-conf-toggle-advanced").val("Hide Advanced Settings");
      $("#red-web-conf-toggle-advanced").data("visibility-state", "visible");
    }
    // We may need to re-hide this one.
    toggle_cache_settings();
  }

  function toggle_cache_settings() {
    var action = 'show';
    if ($("#red-web-conf-toggle-advanced").data("visibility-state") == "hidden") {
      action = 'hide';
    }
    if ($("#web_conf_cache_type").val() == 'custom' && action == 'show') {
      $('#red-web-conf-cache-settings-table-row').show();
    }
    else {
      console.log("Hiding...");
      $('#red-web-conf-cache-settings-table-row').hide();
    }
  }

  // Create a button that will toggle between showing and hiding the advanced options.
 	$("#item_id").parent().append('<input id="red-web-conf-toggle-advanced" class="btn btn-warning" type="button" value="See Advanced Settings" title="Toggle advanced display">');

  // Store a data attribute on it, set to hidden.
  $("#red-web-conf-toggle-advanced").data("visibility-state", "visible");

  // Now toggle it to hidden.
  toggle_advanced_settings();

  // Add a click function to toggle it again.
  $("#red-web-conf-toggle-advanced").click(function() {
    toggle_advanced_settings();
  });

  // Add a click function to toggle display of cache settings.
  $("#web_conf_cache_type").change(function() {
    toggle_cache_settings();
  });
  toggle_cache_settings();

  if (on_new_infrastructure) {
    $('#red-web-conf-login-table-row').hide();
    $('#red-web-conf-execute-as-user-table-row').hide();
    $("#red-web-conf-tls-key-table-row").hide(); 
    $("#red-web-conf-tls-cert-table-row").hide(); 
  }
  else {
    $("#red-item-quota-table-row").hide(); 
    $("#red-item-quota-notify-warning-percent-table-row").hide();
    $("#red-item-quota-notify-warning-date-table-row").hide();
  }
});
