function parsePassword(pass)
{
	// set passwords to blank
	document.getElementById('mysql_user_password').value = '';
	document.getElementById('mysql_user_password_confirm').value = '';

	// display new password
	document.getElementById('red_display_password').innerHTML = ': '+pass;

	// wait half a second
	var now = new Date();
	var exitTime = now.getTime() + 100;
  while(true)
  {
		now = new Date();
		if(now.getTime() > exitTime) break;
	}

	// Now write in forms - this gives users the sense that the password
	// is changed in the form. Otherwise, as they click Generate New Password
	// it doesn't seem like the passwords in the form is changing.
	document.getElementById('mysql_user_password').value = pass;
	document.getElementById('mysql_user_password_confirm').value = pass;
}

