#!/usr/bin/php
<?php

/*
 * add write and reader UIDs for existing web confs.. 
 *
 */

$config_paths = [ '../etc/config.inc.php', '/usr/local/share/red/ui/etc/config.inc.php' ];
foreach($config_paths as $path) {
  if (is_file($path)) {
    require($path);
    break;
  }
}

$globals['config'] = $config;
global $globals;

require($config['common_src_path'] . '/red.lang.utils.inc.php');
require($config['common_src_path'] . '/red.utils.inc.php');
require($config['common_src_path'] . '/class.red_db.inc.php');
require($config['common_src_path'] . '/class.red_ado.inc.php');
require($config['common_src_path'] . '/class.red_item.inc.php');

if(!$sql_resource = red_db::init_db($config)) {
  echo "Failed to initialize db\n";
  exit(2);
}

$construction_options = array(
  'mode' => 'ui',
  'notify_host' => FALSE,
  'sql_resource' => $sql_resource,
  'src_path' => $config['src_path'],
  'common_src_path' => $config['common_src_path'],
  'site_dir_template' => $config['site_dir_template'],
);

$details = [];
$table = 'red_item_web_conf';

$object_path = $config['common_src_path'] . '/modules/class.' . $table . '.inc.php';
if (file_exists($object_path)) {
  require_once($object_path);
}
$object_path = $config['src_path'] . '/modules/class.' . $table . '_ui.inc.php';
require_once($object_path);

$service_id = 7;
$sql = "SELECT item_id FROM red_item JOIN red_item_web_conf USING (item_id) WHERE item_status in ('active', 'disabled') AND web_conf_writer_uid IS NULL";
$result = red_sql_query($sql, $sql_resource);
$count = 0;
$num_results = red_sql_num_rows($result);
$results_per_dot = ceil($num_results / 100);
$dot_count = 0;
while ($row = red_sql_fetch_row($result)) {
  $dot_count++;
  if ($dot_count == $results_per_dot) {
    echo ".";
    $dot_count = 0;
  }
  $count++;
  $item_id = $row[0];
  $co = $construction_options;
  $co['id'] = $item_id;
  $co['child_table'] = $table;
  $class = "{$table}_ui";
  $item = new $class($co);
  $web_conf_writer_uid = $item->get_next_uid();
  $web_conf_reader_uid = $item->get_next_uid();
  $sql = "UPDATE red_item_web_conf SET web_conf_writer_uid = $web_conf_writer_uid, web_conf_reader_uid = $web_conf_reader_uid WHERE item_id = $item_id";
  red_sql_query($sql, $sql_resource);
}

echo "\nUpdated $count records\n";




?>
