#!/usr/bin/php
<?php

/*
 * Mis-matched TTL. 
 *
 */

$config_paths = [ '../etc/config.inc.php', '/usr/local/share/red/ui/etc/config.inc.php' ];
foreach($config_paths as $path) {
  if (is_file($path)) {
    require($path);
    break;
  }
}

$globals['config'] = $config;
global $globals;

require($config['common_src_path'] . '/red.lang.utils.inc.php');
require($config['common_src_path'] . '/red.utils.inc.php');
require($config['common_src_path'] . '/class.red_db.inc.php');
require($config['common_src_path'] . '/class.red_ado.inc.php');
require($config['common_src_path'] . '/class.red_item.inc.php');

if(!$sql_resource = red_db::init_db($config)) {
  echo "Failed to initialize db\n";
  exit(2);
}

$construction_options = array(
  'mode' => 'ui',
  'notify_host' => FALSE,
  'sql_resource' => $sql_resource,
  'src_path' => $config['src_path'],
  'common_src_path' => $config['common_src_path'],
  'site_dir_template' => $config['site_dir_template'],
);

$details = [];
$table = 'red_item_dns';

$object_path = $config['common_src_path'] . '/modules/class.' . $table . '.inc.php';
if (file_exists($object_path)) {
  require_once($object_path);
}
$object_path = $config['src_path'] . '/modules/class.' . $table . '_ui.inc.php';
require_once($object_path);

$service_id = 9;
$sql = "SELECT item_id, dns_fqdn, dns_ttl, item_status FROM red_item JOIN red_item_dns USING (item_id) WHERE item_status in ('active', 'disabled') AND service_id = $service_id";
$result = red_sql_query($sql, $sql_resource);
$count = 0;
$invalid = 0;
$num_results = red_sql_num_rows($result);
$results_per_dot = ceil($num_results / 100);
$dot_count = 0;
while ($row = red_sql_fetch_row($result)) {
  $item_id = $row[0];
  $fqdn = $row[1];
  $ttl = $row[2];
  $status = $row[3];
  $co = $construction_options;
  $co['id'] = $item_id;
  $co['child_table'] = $table;
  $class = "{$table}_ui";
  $item = new $class($co);
  if ($table == 'red_item_dns') {
    $item->quick = TRUE;
  }
  if (!$item->validate()) {
    $invalid++;
    $hosting_order_id = $item->get_hosting_order_id();
    $link = "https://members.mayfirst.org/cp/index.php?area=hosting_order&hosting_order_id={$hosting_order_id}&service_id={$service_id}";
    $error = $item->get_errors_as_string() . " " . $link;
    $regexp = 'All records with the same domain name and type must have the same time to live value\. Try using ([0-9]+) as the time to live instead\.';
    if (preg_match("/$regexp/", $error, $matches)) {
      $new_ttl = intval($matches[1]);
      $item_id = intval($item_id);
      if ($status == 'disabled') {
        $sql = "UPDATE red_item_dns SET dns_ttl = $new_ttl WHERE item_id = $item_id";
        echo "$sql\n";
        red_sql_query($sql, $sql_resource);
      }
      else {
        echo "update $fqdn ($item_id, status $status) and change ttl from $ttl to $new_ttl \n";
      }
    }
  }
}





?>
