#!/usr/bin/php
<?php

/*
 * Server access with more than two entries. 
 *
 */

$config_paths = [ '../etc/config.inc.php', '/usr/local/share/red/ui/etc/config.inc.php' ];
foreach($config_paths as $path) {
  if (is_file($path)) {
    require($path);
    break;
  }
}

$globals['config'] = $config;
global $globals;

require($config['common_src_path'] . '/red.lang.utils.inc.php');
require($config['common_src_path'] . '/red.utils.inc.php');
require($config['common_src_path'] . '/class.red_db.inc.php');
require($config['common_src_path'] . '/class.red_ado.inc.php');
require($config['common_src_path'] . '/class.red_item.inc.php');

if(!$sql_resource = red_db::init_db($config)) {
  echo "Failed to initialize db\n";
  exit(2);
}

$construction_options = array(
  'mode' => 'ui',
  'notify_host' => FALSE,
  'sql_resource' => $sql_resource,
  'src_path' => $config['src_path'],
  'common_src_path' => $config['common_src_path'],
  'site_dir_template' => $config['site_dir_template'],
);

$details = [];
$table = 'red_item_server_access';

$object_path = $config['common_src_path'] . '/modules/class.' . $table . '.inc.php';
if (file_exists($object_path)) {
  require_once($object_path);
}
$object_path = $config['src_path'] . '/modules/class.' . $table . '_ui.inc.php';
require_once($object_path);

$service_id = 3;
$sql = "SELECT item_id, server_access_login, server_access_public_key, item_status FROM red_item JOIN red_item_server_access USING (item_id) WHERE item_status in ('active', 'disabled') AND service_id = $service_id";
$result = red_sql_query($sql, $sql_resource);
$num_results = red_sql_num_rows($result);
$fix = [];
while ($row = red_sql_fetch_row($result)) {
  $item_id = $row[0];
  $login = $row[1];
  $public_key = $row[2];
  $status = $row[3];
  $co = $construction_options;
  $co['id'] = $item_id;
  $co['child_table'] = $table;
  $class = "{$table}_ui";
  $item = new $class($co);
  if (!$item->validate()) {
    $hosting_order_id = $item->get_hosting_order_id();
    $link = "https://members.mayfirst.org/cp/index.php?area=hosting_order&hosting_order_id={$hosting_order_id}&service_id={$service_id}";
    $error = $item->get_errors_as_string();
    if ($error == 'The login has already been assigned a server access record. You can edit that one to add additional keys.') {
      if (!in_array($login, $fix)) {
        $fix[] = $login;
      }
    }
  }
}
foreach ($fix as $user) {
  $user = addslashes($user);
  $sql = "SELECT item_id, item_status, server_access_public_key 
    FROM red_item_server_access JOIN red_item USING (item_id)
    WHERE item_status IN ('active', 'disabled') AND server_access_login = '$user'";
  $result = red_sql_query($sql, $sql_resource);
  $last_item_id = NULL;
  $item_ids = [];
  $keys = [];
  echo "User: $user\n";
  while ($row = red_sql_fetch_row($result)) {
    $item_id = $row[0];
    $status = $row[1];
    $public_key = $row[2];
    $item_ids[$item_id] = '';
    if ($public_key) {
      $keys[] = $public_key;
    }
    if (is_null($last_item_id)) {
      $last_item_id = $item_id;
    }
    elseif ($status == 'active') {
      $last_item_id = $item_id;
    }
  }
  // Update the last item with all the keys.
  $keys_str = addslashes(implode("\n", $keys));
  $sql = "UPDATE red_item_server_access SET server_access_public_key = '$keys_str'
    WHERE item_id = $last_item_id";
  echo "$sql\n";
  red_sql_query($sql, $sql_resource);
  // Delete the rest.
  unset($item_ids[$last_item_id]);
  $item_ids_query = implode(',', array_keys($item_ids));
  $sql = "UPDATE red_item SET item_status = 'deleted' WHERE item_id IN ($item_ids_query)";
  red_sql_query($sql, $sql_resource);
  echo "$sql\n";

}





?>
