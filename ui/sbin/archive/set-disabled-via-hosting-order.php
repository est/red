#!/usr/bin/php
<?php

/*
 * One time script to update existing disabled items so that
 * the item_disabled_by_hosting_order flag is set if it seems
 * that the item was disabled by hosting order.
 */

$config_paths = [ '../etc/config.inc.php', '/usr/local/share/red/ui/etc/config.inc.php' ];
foreach($config_paths as $path) {
  if (is_file($path)) {
    require($path);
    break;
  }
}

$globals['config'] = $config;
global $globals;

require($config['common_src_path'] . '/red.lang.utils.inc.php');
require($config['common_src_path'] . '/red.utils.inc.php');
require($config['common_src_path'] . '/class.red_db.inc.php');

if(!$sql_resource = red_db::init_db($config)) {
  echo "Failed to initialize db\n";
  exit(2);
}


// Get all hosting orders that are currently disabled and have not been updated.
$sql = "SELECT DISTINCT member_id, hosting_order_id FROM red_hosting_order WHERE hosting_order_status = 'disabled' AND 
  hosting_order_id NOT IN (SELECT hosting_order_id FROM red_item WHERE item_disabled_by_hosting_order)";

$result = red_sql_query($sql, $sql_resource);
while ($row = red_sql_fetch_row($result)) {
  $member_id = intval($row[0]);
  $hosting_order_id = intval($row[1]);
  // Pick the date of the most recent disabled item and only flag other disabled items
  // if they were disabled within a one hour time range of the most recent.
  $sql = "SELECT MAX(item_modified) FROM red_item WHERE item_status = 'disabled' AND 
    hosting_order_id = $hosting_order_id";
  $max_result = red_sql_query($sql, $sql_resource);
  $max_row = red_sql_fetch_row($max_result);
  $date_modified = $max_row[0] ?? NULL;
  if (!$date_modified) {
    echo "Failed to find any items that are disabled for member id/hosting order $member_id / $hosting_order_id.\n";
    exit(1);
  }
  $interval = DateInterval::createFromDateString('1 day');
  $date_search = DateTime::createFromFormat("Y-m-d H:i:s", $date_modified)->sub($interval)->format("Y-m-d H:i:s");

  // Get total of all disabled items for reporting purposes.
  $sql = "SELECT COUNT(*) FROM red_item WHERE hosting_order_id = $hosting_order_id AND item_status = 'disabled'";
  $count_result = red_sql_query($sql, $sql_resource);
  $count_row = red_sql_fetch_row($count_result);
  $total = $count_row[0];

  // Get total of all disabled items in last hour for reporting purposes.
  $sql = "SELECT COUNT(*) FROM red_item WHERE hosting_order_id = $hosting_order_id AND item_status = 'disabled' AND item_modified > '$date_search'";
  $count_result = red_sql_query($sql, $sql_resource);
  $count_row = red_sql_fetch_row($count_result);
  $total_in_range = $count_row[0];

  // Update disabled_by_hosting_order for recently disabled items only.
  $sql = "UPDATE red_item SET item_disabled_by_hosting_order = 1 WHERE hosting_order_id = $hosting_order_id AND item_status = 'disabled' AND item_modified > '$date_search'";
  $item_result = red_sql_query($sql, $sql_resource);
  echo "$total_in_range out of $total updated for member id / hosting order $member_id / $hosting_order_id\n";

}

